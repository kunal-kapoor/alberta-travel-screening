/**
 * Condition for running tests based on environment variables
 * 
 */
export const envCond = (val: string) => process.env.APP_OUTPUT === val || process.env.APP_OUTPUT === 'ALL'  || !process.env.APP_OUTPUT? it : it.skip;