import { Test, TestingModule, TestingModuleBuilder } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ArrivalFormModule } from '../../arrival-form/arrival-form.module';
import { ArrivalFormSeedService, SeedBuilder } from '../../arrival-form/arrival-form-seed.service';
import { ArrivalFormRepository } from '../../arrival-form/repositories/arrival-form.repository';
import { ServiceAlbertaRepository } from '../../service-alberta/service-alberta.repository';
import { ClearDB } from '../../db/db2migrations';
import { configureServer } from '../../serverconfig';

/**
 * Creates a nest app that you can use for integration testing purposes.
 *
 * Note:
 * - remember to close() after your tests
 */
export async function createTestingApp(opts?: {entities: any, providers: any, imports: Array<any>}, {disableAuth}={disableAuth: true}, configure?: (module: TestingModuleBuilder) => TestingModuleBuilder ): Promise<any> {
  let app: INestApplication;
  let module: TestingModule;
  let arrivalFormRepository: ArrivalFormRepository;

  return {
    // Starts a nest server, optionally with the db populated with seeding data.
    start: async(shouldSeed: false) => {
      module = await testingModule(opts, configure);

      if(shouldSeed) {
        seed(module);
      }

      app = module.createNestApplication();
      
      configureServer(app, {disableAuth});

      return await app.init().then(context => {
        arrivalFormRepository = context.get(ArrivalFormRepository);
        if(shouldSeed) {
          context.get(ArrivalFormSeedService).seedTestData();
        }

        return context;
      });
    },
    // Close the app
    close: async() => {
      await ClearDB();
      await app.close();
    },
    // Access to the underlying express server
    server: () => app && app.getHttpServer(),
    seed: () => seed(module),
    seedBuilder: numElements => seedBuilder(module, numElements),
    seedEntities: numElements => seedEntities(module, numElements)
  }
}



interface ABTestingModule {
  module: TestingModule,
  seed: any,
  seedBuilder: (numElements: number) => Promise<SeedBuilder>,
  close: () => void
}

/**
 * Creates a testing module with the given params
 * @param entities: defines what repositories should be accessible
 * @param providers: what custom providers should be accessible
 */
export async function createModule(opts={entities: null, providers: null, imports: []}, configure = null): Promise<ABTestingModule>{
  const module = await testingModule(opts, configure);

  return {
    module,
    seed: async () => await seed(module),
    seedBuilder: numElements => seedBuilder(module, numElements),
    close: async () => {
      await ClearDB();
      return await module.close();
    }
  }
}

async function testingModule({entities, providers, imports}={entities: null, providers: null, imports: []}, configure?: (module: TestingModuleBuilder) => TestingModuleBuilder ): Promise<TestingModule> {
  let module = Test.createTestingModule({
    imports: [
      ArrivalFormModule,
      ...(imports || [])
    ],
    providers: providers || [],
  });

  if(configure) {
    module = configure(module);
  }

  return await module.compile();
}

async function seed(module) {
  const seedService = module.get(ArrivalFormSeedService);
  await seedService.seedTestData();
}

async function seedBuilder(module, numElements): Promise<SeedBuilder> {
  const seedService = module.get(ArrivalFormSeedService);
  return await seedService.seedBuilder(numElements);
}

async function seedEntities(module, numElements): Promise<SeedBuilder> {
  const seedService = module.get(ArrivalFormSeedService);
  return await seedService.seedEntities(numElements);
}
