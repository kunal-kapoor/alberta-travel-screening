import { createTestingApp } from "../util/db-test-utils";
import { envCond } from "../util/test-utils";
import request from "supertest";
import { ArrivalFormService } from "../../arrival-form/arrival-form.service";
import { ArrivalFormAdminController } from "../../arrival-form/arrival-form-admin.controller";

describe("Reporting tests", () => {
    let app;
    let appContext;
    let arrivalFormService: ArrivalFormService;
    let unwillingTravellerCtrl: ArrivalFormAdminController;
    const reportEndpoint = '/api/v1/admin/service-alberta/reports';
    const NUM_TRAVELLERS = '# of travellers';
    const EXEMPT_TRAVELLERS = '# of exempt travellers';
    const UNWILLING_TRAVELLERS = '# of travellers unwilling to fill out form';

    afterEach(async () => {
      await app.close();
    });
    
    beforeEach(async () => {
        app = await createTestingApp();
        appContext = await app.start();
        arrivalFormService = appContext.get(ArrivalFormService);
        unwillingTravellerCtrl = appContext.get(ArrivalFormAdminController);
    });
    
    const performCheckpointReport = async () => {
        return (await request.agent(app.server())
            .post(`${reportEndpoint}/checkpoint`)
            .send({
                fromDate: "2015-04-01T07:00:00.000Z",
                toDate: "2040-07-28T06:59:59.999Z"
            })).body[0];
    };

    const submitDetermination = async (form, exempt=false) => {
        await arrivalFormService.submitDetermination(form, {
            determination: 'Support',
            notes: 'Abc123',
            exempt
        })
    }

    describe('# of exempt travellers', () => {
        envCond('ADMIN')('should return 0 when no forms submitted', async () => {
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(0)
        });

        envCond('ADMIN')('should return 0 when form submitted but not determined', async () => {
            await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'No';
                    el0.additionalTravellers = null;
    
                    return [el0];
                })
                .create();
    
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(0)
        });
        envCond('ADMIN')('should return 1 when user is exempt and determination is made', async () => {
            const forms = await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'No';
                    el0.additionalTravellers = null;
    
                    return [el0];
                })
                .create();

            for(const form of forms) {
                await submitDetermination(form, true);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(1)
        });
        envCond('ADMIN')('should return 0 when user is not exempt and determination is made', async () => {
            const forms = await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'No';
                    el0.additionalTravellers = null;
    
                    return [el0];
                })
                .create();

            for(const form of forms) {
                await submitDetermination(form, false);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(0)
        });

        envCond('ADMIN')('should count additional travellers', async () => {
            const forms = await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'Yes';
                    el0.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                    ];
    
                    return [el0];
                })
                .create();

            for(const form of forms) {
                await submitDetermination(form, true);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(3)
        });
        envCond('ADMIN')('should count additional travellers and original traveller when multiple forms exists', async () => {
            const forms = await (await app.seedBuilder(3))
                .transform(([el0, el1, el2]) => {
                    el0.confirmationNumber = 'f1';
                    el0.hasAdditionalTravellers = 'Yes';
                    el0.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                    ];
                    el1.hasAdditionalTravellers = 'No';
                    el1.additionalTravellers = null;
                    el1.confirmationNumber = 'f2';

                    el2.confirmationNumber = 'f3';
                    el2.hasAdditionalTravellers = 'Yes';
                    el2.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                    ];
    
                    return [el0, el1, el2];
                })
                .create();
            
            await submitDetermination(forms.find(e => e.confirmationNumber === 'f1'), false);
            await submitDetermination(forms.find(e => e.confirmationNumber === 'f2'), true);
            await submitDetermination(forms.find(e => e.confirmationNumber === 'f3'), true);
    
            const res = await performCheckpointReport();
    
            expect(res[EXEMPT_TRAVELLERS]).toBe(5)
        });
    });

    const recordUnwillingTraveller = async (numTravellers, note, agent?) => {
        await unwillingTravellerCtrl.recordUnwillingTraveller({user: agent || {sub: 'abc123', name: 'Test', email: 'test@user.com'}}, {
            numTravellers,
            note
        });
    }

    describe('# of travellers unwilling to fill out form', () => {
        envCond('ADMIN')('should return 0 when no records submitted', async () => {
            const res = await performCheckpointReport();
    
            expect(res[UNWILLING_TRAVELLERS]).toBe(0)
        });

        envCond('ADMIN')('should return 1 when 1 unwilling traveller is submitted', async () => {
            await recordUnwillingTraveller(1, 'abc');

            const res = await performCheckpointReport();
    
            expect(res[UNWILLING_TRAVELLERS]).toBe(1)
        });

        envCond('ADMIN')('should return 3 when 3 unwilling travellers are submitted', async () => {
            await recordUnwillingTraveller(3, 'abcd');

            const res = await performCheckpointReport();
    
            expect(res[UNWILLING_TRAVELLERS]).toBe(3)
        });

        envCond('ADMIN')('should return correct number when multiple unwilling travellers are submitted', async () => {
            await recordUnwillingTraveller(3, 'abcd');
            await recordUnwillingTraveller(1, 'defgj', {sub: 'otheragent', name: 'Test2Agent', email: 'test2@user.com'});
            await recordUnwillingTraveller(10, 'abcd');

            const res = await performCheckpointReport();
    
            expect(res[UNWILLING_TRAVELLERS]).toBe(14);
        });

        
    })

    describe('# of travellers', () => {
        envCond('ADMIN')('should return 0 when no forms submitted', async () => {
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(0)
        });

        envCond('ADMIN')('should return 0 when form submitted but not determined', async () => {
            await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'No';
                    el0.additionalTravellers = null;
    
                    return [el0];
                })
                .create();
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(0)
        });
    
        envCond('ADMIN')('should return 1 when 1 form submitted and determined', async () => {
            const forms = await (await app.seedBuilder(1))
            .transform(([el0]) => {
                el0.hasAdditionalTravellers = 'No';
                el0.additionalTravellers = null;
        
                return [el0];
            })
            .create();
            
            for(const form of forms) {
                await submitDetermination(form);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(1)
        });
        envCond('ADMIN')('should not count determined reports', async () => {
            const forms = await (await app.seedBuilder(2))
                .transform(([el0, el1]) => {
                    el0.confirmationNumber = 'f1';
                    el0.hasAdditionalTravellers = 'No';
                    el0.additionalTravellers = null;
    
                    el1.hasAdditionalTravellers = 'No';
                    el1.additionalTravellers = null;
            
                    return [el0, el1];
                })
                .create();
        
            await submitDetermination(forms.find(f => f.confirmationNumber === 'f1'));
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(1)
        });
    
        envCond('ADMIN')('should return 2 when 2 form submitted and determined', async () => {
            const forms = await (await app.seedBuilder(2))
            .transform(([el0, el1]) => {
                el0.hasAdditionalTravellers = 'No';
                el0.additionalTravellers = null;
    
                el1.hasAdditionalTravellers = 'No';
                el1.additionalTravellers = null;
        
                return [el0, el1];
            })
            .create();
        
            for(const form of forms) {
                await submitDetermination(form);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(2)
        });
    
        envCond('ADMIN')('should account for additional travellers', async () => {
            const forms = await (await app.seedBuilder(1))
                .transform(([el0]) => {
                    el0.hasAdditionalTravellers = 'Yes';
                    el0.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        }
                    ];
            
                    return [el0];
                })
                .create();
            
            for(const form of forms) {
                await submitDetermination(form);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(4)
        });
        envCond('ADMIN')('should account for additional travellers for multiple submissions', async () => {
            const forms = await (await app.seedBuilder(3))
                .transform(([el0, el1, el2]) => {
                    el0.hasAdditionalTravellers = 'Yes';
                    el0.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        },
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        }
                    ];
    
                    el1.hasAdditionalTravellers = 'No';
                    el1.additionalTravellers = [];
    
                    el2.hasAdditionalTravellers = 'Yes';
                    el2.additionalTravellers = [
                        {
                            "firstName": "Mischa",
                            "lastName": "Farrar",
                            "dateOfBirth": "1954/09/03"
                        }
                    ];
    
                    return [el0, el1, el2];
                })
                .create();
            
            for(const form of forms) {
                await submitDetermination(form);
            }
    
            const res = await performCheckpointReport();
    
            expect(res[NUM_TRAVELLERS]).toBe(7);
        });    
    });
});