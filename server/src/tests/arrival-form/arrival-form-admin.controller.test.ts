import { createModule } from "../util/db-test-utils";
import { ArrivalFormAdminController } from "../../arrival-form/arrival-form-admin.controller";
import { ArrivalForm } from "../../arrival-form/entities/arrival-form.entity";
import { HttpException } from "@nestjs/common";
import { ArrivalFormRepository } from "../../arrival-form/repositories/arrival-form.repository";
import { ArrivalFormService } from "../../arrival-form/arrival-form.service";
import { ArrivalFormValidationNumberGenerator } from "../../arrival-form/validation-number.service";
import { IsolationPlanReportRepository } from "../../arrival-form/repositories/isolation-plan.repository";
import { envCond } from "../util/test-utils";
import { UnwillingTravellerService } from "../../arrival-form/unwilling-traveller.service";
import { UnwillingTravellerRepository } from "../../arrival-form/repositories/unwilling-traveller.repository";
import { ServiceAlbertaRepository } from "../../service-alberta/service-alberta.repository";

describe('admin controller tests', () => {
  let controller: ArrivalFormAdminController;
  let mockRepository;
  let module;
  
  beforeEach(async () => {

    module = await createModule({ entities: [ArrivalForm], providers: [
      ArrivalFormAdminController,
      ArrivalFormRepository,
      ArrivalFormService,
      ArrivalFormValidationNumberGenerator,
      UnwillingTravellerService,
      UnwillingTravellerRepository,
      IsolationPlanReportRepository,
      ServiceAlbertaRepository,
    ], imports: []}, module => {
      return module.overrideProvider(ArrivalFormRepository).useValue({
        findByLastName: jest.fn(res => res)
      });
    });
    controller = module.module.get(ArrivalFormAdminController);
    mockRepository = module.module.get(ArrivalFormRepository);
  });

  afterEach(async () => {
    await module.close();
  });

  describe('findByLastName', () => {

    const successfulResponse = () => ({travellers: [{id: 1, lastName: 'TestName'}]});
    const noMatchesResponse = () => ({travellers: []});

    
    describe('match is found', () => {
      let res;

      beforeEach(async () => {
        mockRepository.findByLastName.mockReturnValueOnce(successfulResponse());
        res = await controller.findByLastName(null, {lname: 'Test'});
      })

      envCond('ADMIN')('calls findByLastName', async () => {      
        expect(mockRepository.findByLastName).toHaveBeenCalledWith('Test');
      });

      envCond('ADMIN')('returns result', async () => {
        expect(res.travellers[0].id).toBe(1);
      });
    });

    describe('no matches are found', () => {
      beforeEach(async () => {
        mockRepository.findByLastName.mockReturnValueOnce(noMatchesResponse());
      })

      envCond('ADMIN')('throws 404', async () => {
        try{
          await controller.findByLastName(null, {lname: 'Test'});
          fail('Expected findByLastName to throw a HttpException')
        } catch(e) {
          expect(e).toBeInstanceOf(HttpException);
          expect(e.status).toBe(404);
        }
      });
    });

    describe('no matches are found', () => {
      beforeEach(async () => {
        mockRepository.findByLastName.mockReturnValueOnce(noMatchesResponse());
      })

      envCond('ADMIN')('throws 404', async () => {
        try{
          await controller.findByLastName(null, {lname: 'Test'});
          fail('Expected findByLastName to throw a HttpException')
        } catch(e) {
          expect(e).toBeInstanceOf(HttpException);
          expect(e.status).toBe(404);
        }
      });
    });

    describe('lastName is empty', () => {
      beforeEach(async () => {
        mockRepository.findByLastName.mockReturnValueOnce(noMatchesResponse());
      })

      envCond('ADMIN')('throws 404', async () => {
        try{
          await controller.findByLastName(null, {lname: ''});
          fail('Expected findByLastName to throw a HttpException')
        } catch(e) {
          expect(e).toBeInstanceOf(HttpException);
          expect(e.status).toBe(404);
        }
      });
    });
  });
});
