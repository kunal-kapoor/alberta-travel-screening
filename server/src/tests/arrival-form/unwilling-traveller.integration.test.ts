import { createTestingApp } from "../util/db-test-utils";
import { envCond } from "../util/test-utils";
import { ArrivalFormAdminController } from "../../arrival-form/arrival-form-admin.controller";
import { UnwillingTravellerRepository } from "../../arrival-form/repositories/unwilling-traveller.repository";
import { UnwillingTravellerRO } from "../../arrival-form/ro/unwilling-traveller.ro";


let app;
let appContext;
let unwillingTravellerCtl: ArrivalFormAdminController;
let repository: UnwillingTravellerRepository;

afterEach(async () => {
  await app.close();
});

beforeEach(async () => {
    app = await createTestingApp();
    appContext = await app.start();
    unwillingTravellerCtl = appContext.get(ArrivalFormAdminController);
    repository = appContext.get(UnwillingTravellerRepository);
});

describe('unwilling traveller endpoint', () => {
    envCond('ADMIN')('adds record to db', async () => {
        const res = await unwillingTravellerCtl.recordUnwillingTraveller({user: {sub: 'abc123', name: 'Test', email: 'test@user.com'}}, {
            numTravellers: 2,
            note: 'abc'
        });

        expect(res.id).toBeDefined();
    });

    envCond('ADMIN')('adds record to db with data', async () => {
        const res = await unwillingTravellerCtl.recordUnwillingTraveller({user: {sub: 'abc123', name: 'Test', email: 'test@user.com'}}, {
            numTravellers: 2,
            note: 'abc'
        });

        const record: UnwillingTravellerRO = await repository.getUnwillingTraveller(res.id);
        expect(record).toEqual({
            id: 1,
            agentId: 'abc123',
            note: 'abc',
            numTravellers: 2,
            created: expect.any(String)
        })
    });
});
