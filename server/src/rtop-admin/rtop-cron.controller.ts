import {
    Controller,
    UseInterceptors,
    ClassSerializerInterceptor,
    Patch,
    UseGuards,
  } from '@nestjs/common';
  import { LoggerService } from '../logs/logger';
  import { PublicRoute } from '../decorators';
  import { DailyQuestionnaireService } from './daily-reminder.service';
  import { DailyCheckService } from './daily-check.service';
  import { AuthGuard } from '@nestjs/passport';
    
  /**
   * This controller contains endpoints that will be used by an internal
   * cron job to trigger various jobs.
   */
  @Controller('/api/v2/rtop-admin/cron')
  export class RTOPCronController {
  
    constructor(
      private readonly questionnaireService: DailyQuestionnaireService,
      private readonly dailyCheckService: DailyCheckService,
    ){}
  
    /**
     * Send daily reminders
     */
    // Disable global guards
    @PublicRoute()
    // Enable jwt auth guard
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch('/send-daily-reminder')
    async runJob(): Promise<void> {
      try {
        return this.questionnaireService.sendDailyReminders();
      } catch(e) {
        LoggerService.error('Failed to edit form', e);
  
        throw e;
      }
    }

    /**
     * Send daily reminders for call in travellers
     */
    // Disable global guards
    @PublicRoute()
    // Enable jwt auth guard
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch('/send-daily-call-in-reminder')
    async runDailyJob(): Promise<void> {
      try {
        return this.questionnaireService.sendCallInDailyReminders();
      } catch(e) {
        LoggerService.error('Failed to edit form', e);
  
        throw e;
      }
    }
  
    /**
     * Update statuses of households/travellers based on if they
     * have completed their daily check in, if it's > 9 days since
     * they completed a covid test etc.
     * 
     * Scheduler runs 11.59 PM MT daily - which is the next day in UTC.
     * No issue will occur running the scheduler on the next day UTC time.
     * 
     */
    // Disable global guards
    @PublicRoute()
    // Enable jwt auth guard
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch('/daily-check')
    async runDailyCheck(): Promise<void> {
      try {
        return this.dailyCheckService.checkTokens();
      } catch(e) {
        LoggerService.error('Failed to run daily check', e);
        throw e;
      }
    }
  }
  