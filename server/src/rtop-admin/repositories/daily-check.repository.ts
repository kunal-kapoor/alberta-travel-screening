import { Injectable } from "@nestjs/common";
import moment from "moment";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";
import { DATE_FORMAT } from "../../rtop/enrollment-form.constants";
import { EnrollmentFormRepository } from "../../rtop/repositories/enrollment-form.repository";
import { DAILY_STATUS, CARD_STATUS_REASON } from "../../rtop/constants";


export class DailyCheckRO {
    travellerId: number;
    travellerConfirmationNumber: string;

    constructor(data: any) {
        this.travellerId = data["TRAVELLER_ID"];
        this.travellerConfirmationNumber = data["CONFIRMATION_NUMBER"];
    }
}

@Injectable()
export class DailyCheckRepository {

    constructor(
        private readonly enrollmentFormRepository: EnrollmentFormRepository
    ) { }


    async updateExpiredStatus(): Promise<void> {
        const yesterdayDate = moment().subtract('1', 'day').format(DATE_FORMAT);
        /**
         * fetches all the travellers who have not submitted their daily survey
         */
        const expiredTravellers: DailyCheckRO[] = (await RTOPDbService.getExpiredTravellerStatus(EnrollmentStatus.ENROLLED, yesterdayDate)).map((ele) => {
            return new DailyCheckRO(ele);
        });

        const travellerIds: number[] = expiredTravellers.map((ele) => ele.travellerId);
        const travellerConfirmationNumbers: string[] = expiredTravellers.map((ele) => ele.travellerConfirmationNumber);

        /**
         * If there are any traveller who hasn't submitted their daily survey,
         * then update their household and their status
         */
        if (travellerIds.length > 0) {
            const status: string = DAILY_STATUS.YELLOW;
            await this.enrollmentFormRepository.updateHouseholdCardStatus(travellerConfirmationNumbers, status, CARD_STATUS_REASON.DAILY_TRACKING_FLAG, null);
            await this.enrollmentFormRepository.updateTravellerCardStatus(travellerIds, status);
        }
    }
}
