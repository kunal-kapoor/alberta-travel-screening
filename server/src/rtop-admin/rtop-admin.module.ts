import { Module } from "@nestjs/common";
import { RTOPAdminController } from "./rtop-admin.controller";
import { EnrollmentFormRepository } from "../rtop/repositories/enrollment-form.repository";
import { EnrollmentFormService } from "../rtop/enrollment-form.service";
import { ServiceAlbertaRepository } from '../service-alberta/service-alberta.repository';
import { ServiceAlbertaService } from "../service-alberta/service-alberta.service";
import { EmailService } from "../email/email.service";
import { TextService } from "../text.service";
import { EnrollmentFormModule } from "../rtop/enrollment-form.module";
import { EnrollmentFormValidationNumberGenerator } from "../rtop/validation-number.service";
import { DailyQuestionnaireService } from "./daily-reminder.service";
import { DailyCheckService } from "./daily-check.service";
import { DailyReminderRepository } from "./repositories/daily-reminder.repository.";
import { AuthModule } from "../auth/auth.module";
import { AHSApiController } from "./ahs-api.controller";
import { ContactMethodVerificationService } from "../rtop/contact-method-verification.service";
import { ContactMethodVerificationRepository } from "../rtop/repositories/contact-method-verification.repository";
import { RTopMonitoringRepository } from "../rtop/repositories/rtop-monitoring.repository";
import { RTOPCronController } from "./rtop-cron.controller";
import { DailyCheckRepository } from "./repositories/daily-check.repository";

@Module({
    providers: [
        ContactMethodVerificationService,
        ServiceAlbertaRepository,
        ServiceAlbertaService,
        EnrollmentFormService,
        RTopMonitoringRepository,
        EnrollmentFormRepository,
        EnrollmentFormValidationNumberGenerator,
        ContactMethodVerificationRepository,
        DailyQuestionnaireService,
        DailyCheckService,
        DailyReminderRepository,
        EmailService,
        TextService,
        DailyCheckRepository
    ],
    imports: [EnrollmentFormModule, AuthModule],
    controllers: [RTOPAdminController, AHSApiController, RTOPCronController]
  })
  export class RTOPAdminModule {}
  