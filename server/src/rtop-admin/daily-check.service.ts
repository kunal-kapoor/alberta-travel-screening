import { Injectable } from "@nestjs/common";
import { LoggerService } from '../logs/logger';
import { RTOPDbService } from "../db/rtop-dbservice";
import { DailyCheckRepository } from "./repositories/daily-check.repository";


@Injectable()
export class DailyCheckService {
    private static readonly JOB_NAME = 'daily-check';

    constructor(
        private readonly repo: DailyCheckRepository,
      ) {
    }

    /**
     * Run though all daily reminders and find which ones do not have submissions.
     * Change status to yellow for those submissions.
     */
    async checkTokens(): Promise<void> {
        LoggerService.info(`Job '${DailyCheckService.JOB_NAME}' is set to start`);  
        return await this.repo.updateExpiredStatus();
    }
}
