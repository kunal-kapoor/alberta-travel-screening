import {
  Controller,
  Param,
  UseInterceptors,
  Get,
  Req,
  HttpException,
  HttpStatus,
  ClassSerializerInterceptor,
  Query,
  Patch,
  UseGuards,
  Post,
  Body,
} from '@nestjs/common';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { EnrollmentFormRepository } from '../rtop/repositories/enrollment-form.repository';
import { EnrollmentFormRO } from '../rtop/ro/enrollment-form.ro';
import { IsString, MinLength } from 'class-validator';
import { Roles, PublicRoute } from '../decorators';
import { AgentDTO } from '../service-alberta/service-alberta.repository';
import { DailyQuestionnaireService } from './daily-reminder.service';
import { DailyMonitoringFormQuery } from './dto/form-query.dto';
import { DailyMonitoringFromQueryResultRO } from './ro/daily-monitoring-query.ro';
import { EnrollmentStatus } from '../rtop/entities/household.entity';
import { DailyCheckService } from './daily-check.service';
import { HouseholdMonitoringRO } from '../rtop/ro/household-monitoring.ro';
import { RTopMonitoringRepository } from '../rtop/repositories/rtop-monitoring.repository';
import { ActivityDTO } from '../service-alberta/dto/activity.dto';
import { AgentRO } from '../service-alberta/ro/agent.ro';
import { PerformedTestsRecordsRO, PerformedTestRecordRO } from './ro/performed-tests-record.ro';
import { PerformedTestsRecordsDTO } from './dto/performed-tests-record.dto';
import { ContactMethodVerificationDTO } from '../rtop/dto/contact-method-verification.dto';
import { DAILY_STATUS, CARD_STATUS_REASON } from '../rtop/constants';
import { AuthGuard } from '@nestjs/passport';

const throw404 = (message = 'No entries found') => {
  throw new HttpException({
    status: HttpStatus.NOT_FOUND,
    error: message,
  }, HttpStatus.NOT_FOUND);
}

@Controller('/api/v2/rtop-admin')
@UseInterceptors(ClassSerializerInterceptor)
export class RTOPAdminController {

  constructor(
    private readonly enrollmentFormRepository: EnrollmentFormRepository,
    private readonly questionnaireService: DailyQuestionnaireService,
    private readonly monitoringPortalRepository: RTopMonitoringRepository,
  ){}


  /**
   * Search for household records for use in the monitoring portal
   */
  @Roles('GOA')
  @Get('/form')
  async search(@Req() req, @Query() query:DailyMonitoringFormQuery): Promise<DailyMonitoringFromQueryResultRO> {
    const user = await this.monitoringPortalRepository.getOrCreateAgent(req.user);
    const [queryString, results, numberOfResults] = await this.enrollmentFormRepository.getFilteredFormList(user, query);
    const filtredResults = results.map((result)=>{
      return this.questionnaireService.mapToQueryRO(result,user.id)
    });

    LoggerService.logData(req, ActionName.VIEW, queryString, [filtredResults.map(result=>result.id)]);
    
    return new DailyMonitoringFromQueryResultRO(numberOfResults, filtredResults);
  }

  /**
   * Look up a form by confirmation number
   */
  @Roles('GOA')
  @Get('form/:confirmationNumber')
  async findOneByConfirmationNumber(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<HouseholdMonitoringRO> {
    const household: HouseholdMonitoringRO = await this.monitoringPortalRepository.findByConfirmationNumber(confirmationNumber, req);
    if (!household) throw404('Form with matching confirmation number not found.')

    LoggerService.logData(req, ActionName.VIEW, confirmationNumber, [household.id]);

    return household;
  }

   /**
    * Look up the reason for why a household card status was changed
    * based on the household confirmation number
    */
   @Roles('GOA')
   @Get('form/:confirmationNumber/reason')
   async getStatusReason(@Req() req, @Param() {confirmationNumber}): Promise<string> {
       if(!confirmationNumber) {
           LoggerService.logData(req, ActionName.SEARCH, confirmationNumber, null);
           throw404();
       }

       const res: string = await this.enrollmentFormRepository.getStatusReason(confirmationNumber);
       LoggerService.logData(req, ActionName.SEARCH, confirmationNumber, [res]);
       return res;
   }

  /**
   * Updates the card status of a household
   */
  @Roles('GOA')
  @Patch('form/:confirmationNumber/override-status/:cardStatus')
  async overrideHouseholdCardStatus(@Req() req, @Param('confirmationNumber') confirmationNumber: string, @Param('cardStatus') cardStatus: string ): Promise<any> {
    const arrivalForm: EnrollmentFormRO = await this.enrollmentFormRepository.findByConfirmationNumber(confirmationNumber);
    if (!arrivalForm) throw404('Form with matching confirmation number not found.')
    LoggerService.logData(req, ActionName.UPDATE, confirmationNumber, [arrivalForm.id]);

    await this.enrollmentFormRepository.overrideHouseholdCardStatus(confirmationNumber, cardStatus, null);
    return 'Updated';
  }

  /**
   * Assign a household by id to myself for follow up
   */
  @Roles('GOA')
  @Post('/form/:id/assign')
  async assign(@Req() req, @Param('id') formId): Promise<any> {
    const household: EnrollmentFormRO = await this.enrollmentFormRepository.findOne(formId);

    if(!household){
      throw404('Form not found');
    }

    LoggerService.logData(req, ActionName.UPDATE, formId, [household.id]);

    await this.monitoringPortalRepository.assignTo(household.id, req.user);
    return 'Ok';
  }

  /**
   * Update the contact info for the given household.
   * Does not require email/sms verification
   */
  @Roles('GOA')
  @Patch('/form/:id/contact-info')
  async editContactInfo(@Req() req, @Param('id') householdId: number, @Body() contactInfo: ContactMethodVerificationDTO): Promise<any> {
    const household: EnrollmentFormRO = await this.enrollmentFormRepository.findOne(householdId);

    if(!household){
      throw404('Form not found');
    }

    LoggerService.logData(req, ActionName.UPDATE, `${householdId}`, [household.id]);

    await this.monitoringPortalRepository.editContactInfo(householdId, contactInfo);
    return 'Ok';
  }

  /**
   * Undo assignment of household
   */
  @Roles('GOA')
  @Post('/form/:id/assign/undo')
  async undo(@Req() req, @Param('id') formId): Promise<any> {
    const household: EnrollmentFormRO = await this.enrollmentFormRepository.findOne(formId);
    if(!household){
      throw new HttpException({
        status: HttpStatus.NOT_FOUND,
        error: 'Form not found',
      }, HttpStatus.NOT_FOUND);
    }

    LoggerService.logData(req, ActionName.UPDATE, formId, [household.id]);

    await this.monitoringPortalRepository.unassignFrom(household.id, req.user);
    return 'Ok';
  }

  /**
   * Create activity associated with the given household
   */
  @Roles('GOA')
  @Post('/form/:id/activity')
  async createActivity(@Req() req, @Param('id') id: number, @Body() activity: ActivityDTO): Promise<any> {
    LoggerService.logData(req,ActionName.UPDATE,JSON.stringify(activity),[id]);
      const user = await this.monitoringPortalRepository.getOrCreateAgent(req.user);
      if(!await this.enrollmentFormRepository.updateStatus(activity.status,id)){
        throw new HttpException({message:"invalid request"},HttpStatus.BAD_REQUEST);
      }
      
      await this.enrollmentFormRepository.addActivity(activity,user.id,id);
      
    return "Successful";
  }

  /**
   * List agents that have at any time assigned a record to themselves
   */
  @Roles('GOA')
  @Get('/agents')
  async listActiveAgents(@Req() req): Promise<AgentRO[]> {

    const agents: AgentDTO[] = await this.monitoringPortalRepository.listActiveAgents();

    LoggerService.logData(req, ActionName.LIST_AGENTS, null, agents.map(a => a.id));

    return agents.map(r => new AgentRO(r));
  }

  /**
   * Enroll a household into the program
   * @param confirmationNumber confirmation number of the household to enroll
   */
  @Roles('GOA')
  @Patch('form/:confirmationNumber/enroll')
  async enrollTravellerStatus(@Req() req, @Param('confirmationNumber') confirmationNumber: string): Promise<any> {
    const arrivalForm: HouseholdMonitoringRO = await this.monitoringPortalRepository.findByConfirmationNumber(confirmationNumber, req);

    if (!arrivalForm) throw404('Form with matching confirmation number not found.')
    LoggerService.logData(req, ActionName.UPDATE, confirmationNumber, [arrivalForm.id]);

    await this.enrollmentFormRepository.updateEnrollmentStatus(EnrollmentStatus.ENROLLED, arrivalForm.id, arrivalForm.enrollmentStatus);
    return 'Ok';
  }

  @Roles('GOA')
  @Patch('form/:confirmationNumber/withdraw')
  async withdrawTravellerStatus(@Req() req, @Param('confirmationNumber') confirmationNumber: string ): Promise<any> {
    const arrivalForm: HouseholdMonitoringRO = await this.monitoringPortalRepository.findByConfirmationNumber(confirmationNumber, req);

    if (!arrivalForm) throw404('Form with matching confirmation number not found.')
    LoggerService.logData(req, ActionName.UPDATE, confirmationNumber, [arrivalForm.id]);

    await this.enrollmentFormRepository.updateEnrollmentStatus(EnrollmentStatus.WITHDRAWN, arrivalForm.id, arrivalForm.enrollmentStatus);
    return 'Ok';
  }

  @Roles('GOA')
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('test-results')
  async updatePerformedTestRecords(@Req() req, @Body() testRecords: PerformedTestsRecordsDTO): Promise<PerformedTestsRecordsRO> {
    const errors = await this.monitoringPortalRepository.updatePerformedTestRecords(testRecords);

    LoggerService.logData(req, ActionName.CREATE, `TEST_RESULTS`, []);

    return new PerformedTestsRecordsRO(errors);
  }

  /**
   * Re-send a specific reminder to a user
   */
  @Roles('GOA')
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/form/:id/send-reminder')
  async sendDailyReminder(@Req() req, @Param('id') householdId): Promise<string> {
    let household: EnrollmentFormRO;
    try {
      household = await this.enrollmentFormRepository.findOne(householdId);
    } catch(e) {
      // Silently catch error. Error is already logged in the findOne method
    }

    if(!household) {
      throw404('Household not found');
    }

    try {
      LoggerService.logData(req, ActionName.SEND_DAILY_REMINDER, `${householdId}`, []);

      return this.questionnaireService.sendReminder(householdId);
    } catch(e) {
      LoggerService.error('Failed to edit form', e);

      throw e;
    }
  }

  @Roles('GOA')
  @Patch('form/:confirmationNumber/isolation-status')
  async updateTravellerIsolationStatus(@Req() req, @Param('confirmationNumber') confirmationNumber: string, @Body('isolationStatus') isolationStatus: boolean): Promise<any> {
    LoggerService.logData(req, ActionName.UPDATE, confirmationNumber, [String(isolationStatus)]);

    if( await this.monitoringPortalRepository.updateTravellerIsolationStatus(confirmationNumber, isolationStatus))
    {
      return 'Ok';
    }
    else{
      throw404('Cannot update isolation status of traveller.')
    }
  }

}
