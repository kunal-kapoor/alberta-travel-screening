import { ValidationPipe } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { APIAuthGuard } from "./auth/api.guard";
import { RolesGuard } from "./auth/roles.guard";

export const configureServer = (server, {disableAuth} = {disableAuth: false}): void => {
    server.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true , disableErrorMessages:process.env.NODE_ENV==='production' }));
    server.disable('x-powered-by');

    if(disableAuth && process.env.NODE_ENV !== 'test') {
        throw 'Cannot start app without auth for non-test purposes';
    } else if(!disableAuth) {
        // Use APIAuthGuard for ALL endpoints by default
        // For public endpoints: Annotate endpoint with @PublicRoute
        const reflector = server.get( Reflector );
        server.useGlobalGuards(new APIAuthGuard(reflector), new RolesGuard(reflector));
    }
}
