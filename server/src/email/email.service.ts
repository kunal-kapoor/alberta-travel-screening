import { Logger, Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import node_handlebars from 'nodemailer-express-handlebars';
import Mail from 'nodemailer/lib/mailer';
import { join } from 'path';
import { LoggerService } from '../logs/logger';

export abstract class EmailSendable {
    // Recipient address
    to: string | string[];
    // Sender email address. Must match a configured email service sender
    from: string;
    subject: string;

    // Name of handlebar template to send.
    // Must correspond to a .hbs template in the templates/ folder
    readonly template: string;
  
    // Injectable values for handlebars template
    readonly context?: any;    
}

@Injectable()
export class EmailService {

    transporter: Mail;

    constructor() {
        this.transporter = createTransport({
            host: process.env.MAIL_HOST as string,
            port: parseInt(process.env.MAIL_PORT, 10),
            secure: process.env.SECURE_MAIL === 'true',
            auth: process.env.MAIL_USER? {
                user: process.env.MAIL_USER as string,
                pass: process.env.MAIL_PASSWORD as string,
            }: null,
        });
      
        this.transporter.use(
            'compile',
            node_handlebars({
                viewEngine: {
                    extname: '.hbs',
                    layoutsDir: join(__dirname, 'templates/emails/'),
                    partialsDir: join(__dirname, 'templates/partials/'),
                },
                viewPath: join(__dirname, 'templates/emails/'),
                extName: '.hbs',
            }),
        );
      
    }
    
    /**
     * Sends an email to the given email address
     */
    async sendEmail(email: EmailSendable): Promise<void> {
        try {

            return new Promise((resolve, reject) => {
                this.transporter.sendMail({ ...email }, (error: Error) => {
                    error ? reject(error) : resolve();
                });    
            })
        } catch(e) {
            LoggerService.logger.error('Failed to send email', e);

            throw e;
        }
    }
}