  /**
   * Describes the actions performed by the user during an access 
   * 
   * @remarks
   * The EHR and connected systems or applications must capture unmasking or break-the-glass activity while ensuring health information is not disclosed.
   */

export enum ActionName {
  SEARCH = "SEARCH",
  CREATE = "CREATE",
  READ = "READ",
  UPDATE = "UPDATE",
  EDIT = "EDIT",
  DELETE = "DELETE",
  VIEW = "VIEW",
  LOGIN = "LOGIN",
  LOGOUT = "LOGOUT",
  LIST_AGENTS = "LIST_AGENTS",
  OUTFLOW = "OUTFLOW",
  COPY = "COPY",
  PRINT = "PRINT",
  VALIDATE_TOKEN = "VALIDATE_TOKEN",
  RECORD_UNWILLING_TRAVELLER = "RECORD_UNWILLING_TRAVELLER",
  GRANT_REPORT_ACCESS = "GRANT_REPORT_ACCESS",
  LIST_REPORT_ACCESS = "LIST_REPORT_ACCESS",
  UPDATE_REPORT_ACCESS = "UPDATE_REPORT_ACCESS",

  SEND_DAILY_REMINDER = "SEND_DAILY_REMINDER"
}