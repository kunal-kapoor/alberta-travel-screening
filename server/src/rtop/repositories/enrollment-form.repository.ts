import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logs/logger';
import { SubmissionDeterminationDTO } from '../dto/submission-determination.dto';
import { ActivityDTO } from '../../service-alberta/dto/activity.dto';
import { RTOPDbService } from '../../db/rtop-dbservice';
import { EnrollmentSearchQueryBuilder } from './enrollment-search-query-builder';
import { EnrollmentFormRO } from '../ro/enrollment-form.ro';
import { EditFormDTO } from '../dto/edit-form.dto';
import { EnrollmentFormSearchResultRO } from '../ro/enrollment-form-search.ro';
import { RtopTravellerSearchRO } from '../ro/rtop-traveller.ro';
import { Household, EnrollmentStatus } from '../entities/household.entity';
import { RTOPTraveller } from '../entities/rtop-traveller.entity';
import { DailySubmission } from '../entities/daily-submission.entity';
import { ReminderIDRO } from '../ro/reminder-id.ro';
import { HouseholdTravellersQueryStatusRO } from '../ro/household-submission-status.ro';
import { DailyMonitoringFormQuery } from '../../rtop-admin/dto/form-query.dto';
import { DAILY_STATUS, DAILY_QUESTIONS } from '../constants';
import { DailyReminderRO } from '../../rtop-admin/repositories/daily-reminder.repository.';
import { DailyQuestionnaireService } from '../../rtop-admin/daily-reminder.service';
import { DailyQuestionAnswerService } from '../daily-question-answer.service';

@Injectable()
export class EnrollmentFormRepository {

    /**
     * Create traveller records and assign them to the given household
     * @param household household to assign travellers to
     * @param records travellers to create
     */
    async createTravellerRecords(household: Household, records: RTOPTraveller[]) {
        return await RTOPDbService.createTravellerRecords(household, records);
    }

    /**
     *
     * Counts the number of records with the corresponding confirmation number
     * This function is used to verify that a record exists
     *
     * @param code - confirmation number
     *
     * @returns The number of records with a matching confirmation number
     */
    async countByConfirmationNumber(code: string): Promise<number> {
        return await RTOPDbService.countByNum(code);
    }

    /**
     * Counts the number of households in the database
     *
     * @returns - The number of matching records
     */
    async count(): Promise<number> {
        return await RTOPDbService.getCount('household');
    }

    /**
     * Updates the quarantineLocation of a form with the given values
     *
     * @param agentId - ID of the agent making the change
     * @param arrivalFormId - The enrollment form id to update
     * @param updatedFields - The new quarantine location values
     */
    async editForm(agentId: string, arrivalForm: EnrollmentFormRO, updatedFields: EditFormDTO): Promise<void> {
        try {
            const updateRequest = JSON.stringify(
                {"$set":
                    {
                        hasPlaceToStayForQuarantine: updatedFields.hasPlaceToStayForQuarantine,
                        quarantineLocation: updatedFields.hasPlaceToStayForQuarantine? updatedFields.quarantineLocation: undefined
                    }
                }
            );

            // Actually update form with new values
            await RTOPDbService.editForm(agentId, arrivalForm.id, updateRequest);
        } catch(e) {
            LoggerService.error(`Failed to edit form with id ${arrivalForm.id}`, e);
            throw e;
        }
    }

    /**
     *
     * Find an enrollment form by id
     *
     * @param id - Id of the record
     *
     * @returns Enrollment form
     */
    async findOne(id: number): Promise<EnrollmentFormRO> {
        try {
            const res = await (await RTOPDbService.getForm(id)).single();
            return res;
        } catch (e){
            LoggerService.error('Failed to find form', e);

            throw e;
        }
    }

    /**
     * Find a record by confirmation number
     *
     * @param confirmationNumber
     *
     * @returns - enrollment form with the matching confirmation number
     */
    async findByConfirmationNumber(confirmationNumber: string): Promise<EnrollmentFormRO> {
        const res = await(await RTOPDbService.getFormByNumber(confirmationNumber))
            .single();
        return res;
    }

    /**
     * Find a record by a travellers confirmation number
     *
     * @param confirmationNumber confirmation number
     *
     * @returns - enrollment form that has a traveller with the matching confirmation number
     */
    async findByTravellerConfirmationNumber(confirmationNumber: string): Promise<EnrollmentFormRO> {
        const res = await(await RTOPDbService.getFormByTravellerConfirmationNumber(confirmationNumber))
            .single();
        return res;
    }

    /**
     * Find all enrollment forms that have a traveller with the corresponding last name.
     *
     * @param lname - Last name of the record
     *
     * @returns - An array of forms.
     */
    async findByLastName(lname: string): Promise<EnrollmentFormSearchResultRO> {
        if (!lname) { return { travellers: [], date: '' } }

        const query = lname.length === 2? RTOPDbService.getFormByLastNameExact: RTOPDbService.getFormByLastName;

        const travellers = await (await query(lname))
            .multipleTraveller();

        return new EnrollmentFormSearchResultRO(travellers[0]?.date, travellers);
    }

    /**
     * Find all enrollment forms that have a traveller with the corresponding last name.
     *
     * @param id - travellers id
     * @param birthdate - travellers date of birth
     *
     * @returns - traveller
     */
    async verifyByBirthDate(confirmationNumber: string, reminderToken: string, birthDate: string): Promise<RtopTravellerSearchRO> {
        if (!confirmationNumber || !reminderToken || !birthDate) { return null }

        const res = await(await RTOPDbService.verifyByBirthDate(confirmationNumber, reminderToken, birthDate));
        return res && new RtopTravellerSearchRO(res);
    }

    /**
     * Find all enrollment forms that have a traveller with the corresponding token.
     *
     * @param token - daily reminder id
     *
     * @returns - An array of travellers of a household.
     */
    async findByToken(token: string): Promise<EnrollmentFormSearchResultRO> {
        if (!token) { return { travellers: [], date: '' } }

        const travellers = await (await RTOPDbService.getFormByToken(token))
            .multipleTraveller();

        return new EnrollmentFormSearchResultRO(travellers[0]?.date, travellers, true);
    }

    /**
     * Get a reason for status change
     *
     * @param confirmationNumber - travellers confirmation number
     *
     * @returns - string value for reason
     */
    async getStatusReason(confirmationNumber: string): Promise<string> {
        if (!confirmationNumber) { return null }
        return await RTOPDbService.getStatusReason(confirmationNumber);
    }

    /**
     * Submit daily questionnaire for traveller
     *
     * @param submission - submitted answers
     * @param travellerId - confirmation number
     * @param reminderId - reminder id
     */
    async createDailySubmission(submission: DailySubmission, reminder: ReminderIDRO): Promise<any> {
        const status = DailyQuestionAnswerService.submissionCardStatus(submission);

        const encodedAnswers = DailyQuestionAnswerService.encodeAnswers(submission, reminder);
        const jsonSubmission = JSON.stringify(Object.assign({}, submission, {answers: encodedAnswers}));
        return await RTOPDbService.createDailySubmission(status, jsonSubmission, reminder.travellerId, reminder.id);
    }

    /**
     * Returns reminder id
     *
     * @param travellerId - confirmation number
     */
    async getReminderId(confirmationNumber: string, token: string): Promise<ReminderIDRO> {
        const res = await(await RTOPDbService.getReminderId(confirmationNumber, token));
        return res && new ReminderIDRO(res.value()[0], token);
    }

    /**
     * Saves an enrollment form
     *
     * @param form - form to save
     *
     * @returns - The enrollment form
     */
    async save(form: Household): Promise<Household> {
        const id:number = await(await RTOPDbService.saveForm(form)).idOnly();

        form.id = id;

        return form;
    }

    /**
     * Search households based on the given search filters.
     * 
     * This powers the list view of the monitoring portal
     * @param user user performing the search
     * @param query query to filter records
     */
    async getFilteredFormList(user:any, query: DailyMonitoringFormQuery):Promise<any>{
        return new EnrollmentSearchQueryBuilder(user)
            .query(query.query)
            .filter(query.filter)
            .agent(query.agent)
            .fromDate(query.fromDate)
            .filterCardStatus(query.cardStatus)
            .enrollmentStatus(query.enrollmentStatus)
            .toDate(query.toDate)
            .orderBy(query.orderBy, query.order)
            .paginate(query.page, 20)
            .execute();
    }

    /**
     * Add a new activity to the given household 
     * @param activity activity to add 
     * @param userId user that added the activity
     * @param householdId id of household to add activity to
     */
    async addActivity(activity:ActivityDTO, userId:string, householdId:number):Promise<void>{
        await RTOPDbService.addActivity(activity, userId, householdId);
    }

    /**
     * Update the status of a household (1st call made etc)
     * @param status status enum to update to
     * @param householdId id of household to update status of
     */
    async updateStatus(status:string, householdId:number):Promise<boolean>{
        try{
            await RTOPDbService.updateStatus(status, householdId);
            return true;
        }catch(err){
            return false;
        }
    }

    /**
     * Update the status of a household (red / green / yellow)
     * @param confirmationNumber confirmation number of traveller to update
     * @param dailyStatus new status
     * @param reason reason for updating the status
     */
    async updateHouseholdCardStatus(confirmationNumber: string[], dailyStatus: string, reason: string, dailySubmission: number):Promise<any> {
        await RTOPDbService.createHouseholdStatusHistory(confirmationNumber, dailyStatus, reason, dailySubmission);
        return await RTOPDbService.updateHouseholdCardStatus(confirmationNumber, dailyStatus, reason);
        
    }

    /**
     * Override the status of a household from (red / yellow) to green by agent
     * @param confirmationNumber confirmation number of household to update
     * @param dailyStatus new status
     * @param reason reason for updating the status
     */
    async overrideHouseholdCardStatus(confirmationNumber: string, dailyStatus: string, reason: string):Promise<any> {
        return await RTOPDbService.overrideHouseholdCardStatus(confirmationNumber, dailyStatus, reason);
    }

    /**
     * Retrieve status of traveller based on their confirmation
     * @param confirmationNumber confirmation number of traveller to retrieve
     */
    async getTravellerStatusByConfirmationNumber(confirmationNumber: string): Promise<HouseholdTravellersQueryStatusRO> {
        const res = await RTOPDbService.getTravellerStatusForHousehold(confirmationNumber);
        return res;
    }

    /**
     * Update the enrollment status of a household
     * @param enrollmentStatus new enrollment status
     * @param householdId id of household to update
     * @param currentEnrollmentStatus the current enrollment status of household
     */
    async updateEnrollmentStatus(enrollmentStatus: EnrollmentStatus, householdId: number, currentEnrollmentStatus: EnrollmentStatus):Promise<any> {
        if(currentEnrollmentStatus === EnrollmentStatus.APPLIED) {
            return await RTOPDbService.updateEnrollmentStatusWithDate(enrollmentStatus, householdId );
        } else {
            return await RTOPDbService.updateEnrollmentStatus(enrollmentStatus, householdId );
        }
    }

    /**
     * Update the status of a traveller (red / green / yellow)
     * @param travellerId Id of the traveller
     * @param dailyStatus new status
     *
     */
    async updateTravellerCardStatus(travellerId: number[], dailyStatus: string):Promise<any> {
        return await RTOPDbService.updateTravellerCardStatus(travellerId, dailyStatus);
    }

    async updateDOBVerificationAttempts(confirmationNumber: string, isVerified: boolean): Promise<any> {
        if (isVerified) {
            // reset DOB verification attempt status
            return await RTOPDbService.resetDOBVerificationAttempts(confirmationNumber);
        }
        else {
            return await RTOPDbService.updateDOBVerificationAttempts(confirmationNumber);
        }
    }
}
