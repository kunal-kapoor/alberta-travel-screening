import { RTOPTraveller } from "../entities/rtop-traveller.entity";
import { booleanToString } from "../enrollment-form.constants";
import { Household, EnrollmentStatus } from "../entities/household.entity";
import { HouseholdTravellersSubmissionStatusRO } from "./household-submission-status.ro";
import { QuarantineLocationRO } from "./quarantine-location.entity";

export class EnrollmentFormRO extends RTOPTraveller {
    id: number;
    viableIsolationPlan: boolean;
    phoneNumber: string;
    email: string;
    hasAdditionalTravellers: string;
    additionalTravellers: Array<RTOPTraveller>
    hasTravellersOutsideHousehold: string;
    travellersOutsideHousehold: string;
    nameOfAirportOrBorderCrossing: string;
    arrivalDate: string;
    arrivalCityOrTown: string;
    arrivalCountry: string;
    address: string;
    cityOrTown: string;
    provinceTerritory: string;
    postalCode: string;
    quarantineLocationPhoneNumber: string;
    quarantineLocation: QuarantineLocationRO;
    typeOfPlace: string;
    howToGetToPlace: string;
    enrollmentStatus: EnrollmentStatus;
    householdCardStatus: string;
    travellerStatus: HouseholdTravellersSubmissionStatusRO[];
    dailyReminderLink: string;

    constructor(data: Household) {
        super();

        Object.assign(this, data);

        this.hasAdditionalTravellers = booleanToString(data.hasAdditionalTravellers);
        this.hasTravellersOutsideHousehold = booleanToString(data.hasTravellersOutsideHousehold);
        this.quarantineLocation = new QuarantineLocationRO();
        this.quarantineLocation.address = data.address;
        this.quarantineLocation.phoneNumber = data.quarantineLocationPhoneNumber;
        this.quarantineLocation.cityOrTown = data.cityOrTown;
        this.quarantineLocation.provinceTerritory = data.provinceTerritory;
        this.quarantineLocation.postalCode = data.postalCode;
        this.quarantineLocation.typeOfPlace = data.typeOfPlace;
        this.quarantineLocation.howToGetToPlace = data.howToGetToPlace;
        this.quarantineLocation.doesVulnerablePersonLiveThere = '';
        this.quarantineLocation.otherPeopleResiding = '';
    }
}
