import { Exclude, Expose } from "class-transformer";

@Exclude()
export class EnrollmentFormTravellerRO {
    @Expose()
    firstName: string;
    @Expose()
    lastName: string;
    @Expose()
    confirmationNumber: string;

    constructor(data) {
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        this.confirmationNumber = data.confirmationNumber;
    }
}

@Exclude()
export class EnrollmentFormSubmissionRO {
    id: number;

    @Expose()
    message: string;
    @Expose()
    viableIsolationPlan: boolean;
    @Expose()
    travellers: EnrollmentFormTravellerRO[];
    
    constructor(data) {
        this.travellers = data.travellers.map(r => new EnrollmentFormTravellerRO(r));
        delete data.travellers;
        Object.assign(this, data);
    }
}
