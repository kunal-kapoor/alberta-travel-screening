export class IsolationPlanReportRO {
    id?: number;
    date:string;
    failed:number;
    passed:number;
    failedThenPassed:number;
    passedThenFailed:number;
    constructor(res) {
        // Convert uppercase properties (from db2 query) to properties
        this.id = res.ID;
        this.date = res.DATE;
        this.passed = res.PASSED;
        this.failed = res.FAILED;
        this.failedThenPassed = res.FAILEDTHENPASSED;
        this.passedThenFailed = res.PASSEDTHENFAILED;
    }
}
