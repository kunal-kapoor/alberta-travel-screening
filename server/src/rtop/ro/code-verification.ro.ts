import { Expose, Exclude } from "class-transformer";

@Exclude()
export class CodeVerificationRO {

    @Expose()
    token: string;

    constructor(data) {
        const {TOKEN} = data;

        this.token = TOKEN;
    }
}