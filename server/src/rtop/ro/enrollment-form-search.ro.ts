import { Household } from "../entities/household.entity";
import { IsArray, ValidateNested } from "class-validator";
import { Exclude, Expose } from "class-transformer";

@Exclude()
export class EnrollmentFormSearchRO {
    @Expose()
    id: number;

    @Expose()
    confirmationNumber: string;

    @Expose()
    status: string;

    @Expose()
    submissionStatus: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    arrivalCityOrTown: string;

    @Expose()
    arrivalCountry: string;

    @Expose()
    arrivalDate: Date;

    @Expose()
    dobVerificationAttempt: number;

    @Expose()
    dobAttemptTime: string;

    constructor(data: any) {
        Object.assign(this, data);
    }
}

export class EnrollmentFormSearchResultRO {
    @IsArray()
    @ValidateNested({each: true})
    travellers: EnrollmentFormSearchRO[];

    @Exclude()
    date: string;

    constructor(date: string, trls: any[], maskNames = false) {
        this.travellers = trls.map(t => new EnrollmentFormSearchRO(t));
        this.date = date;

        if (maskNames) {
            this.travellers.forEach(traveller => {
                traveller.firstName = StringMask.modigyString(traveller.firstName);
                traveller.lastName = StringMask.modigyString(traveller.lastName);
            });
        }
    }
}

@Exclude()
class StringMask {
    /**
     * Masks the given string by replacing all but the first character of the string with *
     * TODO: This needs to be updated once actual business requirements are known
     */
    public static modigyString(str: string) {
        const maskSymbol = "*";
        str.substring(0, 1);
        return str.substring(0, 1) + maskSymbol.repeat(str.length);
    }
}
