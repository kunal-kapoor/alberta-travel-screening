import { randomBytes } from "crypto";
import format from "biguint-format";

export class VerificationTokenService {   
    /**
     * Creates a random number with numDigits decimals
     */
    static createNumericCode(numDigits: number) {
        const bts = randomBytes(6); // Generate random bytes
        const code = format(bts, 'dec', {padstr:'0', size: numDigits}); // Format number as decimal
        
        return code.substring(code.length - numDigits);
    }

    /**
     * Creates a random string of length numBytes
     */
    static createToken(numBytes: number) {
        return randomBytes(numBytes).toString('hex');
    }
}