import { DAILY_QUESTIONS, DAILY_STATUS } from "./constants";
import { DailySubmissionAnswer } from "./enrollment-form.constants";
import { DailySubmission } from "./entities/daily-submission.entity";
import crypto from "crypto";
import { ReminderIDRO } from "./ro/reminder-id.ro";

export class DailyQuestionAnswerService {

    /**
     * Returns red if any questions in submission were answered yes,
     * otherwise green
     */
    public static submissionCardStatus(submission: DailySubmission): DAILY_STATUS {
      return submission.answers?.find(a => a.answer.toLowerCase() === 'yes')? DAILY_STATUS.RED: DAILY_STATUS.GREEN;
    }

    public static hashAnswer(answerKey: string, reminder: ReminderIDRO) {

        if(!answerKey || !reminder.token || !reminder.travellerId) {
            // Fail hard if something is missing when hashing answer
            const reasons = []

            if(!answerKey) {reasons.push('Answer Key')}
            if(!reminder.token) {reasons.push('Token')}
            if(!reminder.travellerId) {reasons.push('Traveller ID')}

            throw new Error(`Missing required param(s) for answer hashing: ${reasons.join(', ')}`);
        }

        return crypto.createHash('sha256')
            .update(`${reminder.token}${answerKey}${reminder.travellerId}`)
            .digest('hex');
    }


     /**
     * Encode answers based on the given submission, encode with keys found in DAILY_QUESTIONS
     *  [{question: 'Are you following the rules?', answer: 'Yes'}] => ['abnwejkfrenw', 'sdjnfjnrwbnv']
     */
    public static encodeAnswers(submission: DailySubmission, reminder: ReminderIDRO): string[] {
        return submission.answers.map(item => {
            const answer =  item.answer.toLowerCase();
            const answerKey = answer === 'yes'? DAILY_QUESTIONS[item.question].yes: DAILY_QUESTIONS[item.question].no;

            return this.hashAnswer(answerKey, reminder);
        })
    }

    /**
     * Find actual questions / answers based on decoded answers
     * ['abnwejkfrenw', 'sdjnfjnrwbnv'] => [{question: 'Are you following the rules?', answer: 'Yes'}]
     */
    public static decodeAnswers(answers: string[], reminder: ReminderIDRO): DailySubmissionAnswer[] {
        const decodedAnswers: any = Object.entries(DAILY_QUESTIONS).map(([question, ans]) =>  {
            return Object.entries(ans).map(([answer, key]) => {
                key = this.hashAnswer(key, reminder);
 
                if(answers.includes(key)) {
                    return {question, answer};
                }

                return null;
            });
        });

        return decodedAnswers.flat().filter(a => !!a);
    }
}