import { IsString, Length, IsOptional, Matches, IsIn } from "class-validator";
import { CONTACT_METHOD } from "../../rtop-admin/repositories/daily-reminder.repository.";

export class ContactMethodVerificationDTO {
    @IsOptional()
    @IsString()
    @Length(0, 35)
    phoneNumber: string;
  
    @IsOptional()
    @IsString()
    @Matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$|$^/)// eslint-disable-line no-useless-escape
    email: string;

    @IsString()
    @IsIn(Object.values(CONTACT_METHOD))
    contactMethod: CONTACT_METHOD
}

export class CodeVerificationDTO {
    @IsString()
    @Length(1, 35)
    code: string;

    @IsString()
    @Length(1, 255)
    verificationId: string;
}