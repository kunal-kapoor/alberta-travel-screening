import {
    IsArray,
    ValidateNested,
    IsOptional,
    IsString,
    Length,
  } from 'class-validator';
  
  import { Exclude, Type } from 'class-transformer';
  import { DailySubmissionAnswer } from '../enrollment-form.constants';
  
  export class DailySubmissionDTO {
  
    constructor(data: DailySubmissionDTO) {
      Object.assign(this, data);
    }
  
    @Exclude()
    id?: number;
    
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => DailySubmissionAnswer)
    answers: Array<DailySubmissionAnswer>;

    @IsOptional()
    @IsString()
    @Length(1, 255)
    cardStatus: string;
  }
  