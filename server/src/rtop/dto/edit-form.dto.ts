import { IsString, Length, IsOptional, ValidateNested, IsBoolean } from "class-validator";
import { Type } from "class-transformer";
import { QuarantineLocationDTO } from "./quarantine-location.dto";
import { TransformBooleanString } from "../../decorators";

export class EditFormDTO {
    
    @IsBoolean()
    @TransformBooleanString()
    hasPlaceToStayForQuarantine: boolean;
  
    @IsOptional()
    @ValidateNested()
    @Type(() => QuarantineLocationDTO)
    quarantineLocation: QuarantineLocationDTO;  
}