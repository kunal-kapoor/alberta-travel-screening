import moment from 'moment';
import { Injectable } from "@nestjs/common";
import { EnrollmentFormRepository } from './repositories/enrollment-form.repository';
import { Household, EnrollmentStatus } from './entities/household.entity';
import { RTOPTraveller } from './entities/rtop-traveller.entity';
import { EnrollmentFormValidationNumberGenerator } from './validation-number.service';
import { EnrollmentFormDTO } from './dto/enrollment-form.dto';
import { EnrollmentFormSubmissionRO } from './ro/form-submission.ro';
import { AdditionalTravellers } from './enrollment-form.constants';
import { EnrollmentFormRO } from './ro/enrollment-form.ro';
import { RTOPDbService } from '../db/rtop-dbservice';
import { ContactMethodVerification } from './entities/contact-method-verification';
import { DailySubmission } from './entities/daily-submission.entity';
import { ReminderIDRO } from './ro/reminder-id.ro';
import { CARD_STATUS_REASON, DAILY_STATUS } from './constants';
import { DailyQuestionAnswerService } from './daily-question-answer.service';

export enum DeterminationDecision {
    ACCEPTED = 'Accepted'
}

const isViableIsolationPlan = (dto: EnrollmentFormDTO): boolean => {
    return dto.address? true: false;
};

@Injectable()
export class EnrollmentFormService {
    constructor(
        private readonly enrollmentFormRepository: EnrollmentFormRepository,
        private readonly confirmationNumberGenerator:EnrollmentFormValidationNumberGenerator
      ) {}

    /**
     * Creates a new ArrivalForm
     * - Generate a unique confirmation number
     * - Create entity
     * - Update daily report based on isolation plan status
     *
     * @param body data of the arrival form to create.
     */
    async saveForm(body: EnrollmentFormDTO, verification: ContactMethodVerification): Promise<EnrollmentFormSubmissionRO> {
        const confirmationNumber = await this.confirmationNumberGenerator.generateValidationNumber();
        const form: Household = this.mapArrivalFormDtoToEntity(body, confirmationNumber, verification);
        await this.enrollmentFormRepository.save(form);

        // Create separate traveller records with unique confirmation numbers
        // for use in the daily questionnaire flow
        // Primary traveller gets confirmation number ${confirmationNumber}-0
        // additional travellers gets ${confirmationNumber}-1, ${confirmationNumber}-2 etc.
        const travellers = [
            this.mapToAdditionalTraveller(`${confirmationNumber}-0`, form),
            ...(form.additionalTravellers || []).map(t => this.mapToAdditionalTraveller(t.confirmationNumber, t))
        ];
        await this.enrollmentFormRepository.createTravellerRecords(form, travellers);

        return new EnrollmentFormSubmissionRO(
            {
                id: form.id,
                message: 'Form submission successful',
                confirmationNumber,
                viableIsolationPlan: form.viableIsolationPlan,
                travellers
            }
        );
    }

    /**
     * Mapping from EnrollmentFormDTO + Confirmation number -> ArrivalForm
     * @param dto
     * @param confirmationNumber
     */
    mapArrivalFormDtoToEntity(dto: EnrollmentFormDTO, confirmationNumber: string, verification: ContactMethodVerification): Household {
        return <Household> {
            ...dto,
            id: null,
            contactMethod: verification.contactMethod,
            email: dto.email,
            phoneNumber: dto.phoneNumber,
            contactEmail: verification.email,
            contactPhoneNumber: verification.phoneNumber,
            confirmationNumber,
            enrollmentStatus: EnrollmentStatus.APPLIED, 
            viableIsolationPlan: isViableIsolationPlan(dto),
            determination: undefined,
            created: new Date(),
            dateOfBirth: moment(dto.dateOfBirth,'YYYY/MM/DD').toDate(),
            arrivalDate: moment(dto.arrivalDate,'YYYY/MM/DD').toDate(),
            additionalTravellers: dto.additionalTravellers && dto.additionalTravellers.map((t, idx) => this.mapToAdditionalTraveller(`${confirmationNumber}-${idx+1}`, t)),
        }
    }

    mapToAdditionalTraveller(confirmationNumber, additionalTraveller: AdditionalTravellers): RTOPTraveller {
        return {
            firstName: additionalTraveller.firstName,
            confirmationNumber,
            lastName: additionalTraveller.lastName,
            dateOfBirth: moment(additionalTraveller.dateOfBirth,'YYYY/MM/DD').toDate(),
            gender: additionalTraveller.gender,
            citizenshipStatus: additionalTraveller.citizenshipStatus,
            exemptionType: additionalTraveller.exemptionType,
            exemptOccupation: additionalTraveller.exemptOccupation,
            exemptOccupationDetails: additionalTraveller.exemptOccupationDetails,
            dateLeavingCanada: additionalTraveller.dateLeavingCanada,
            seatNumber: additionalTraveller.seatNumber,
            reasonForTravel: additionalTraveller.reasonForTravel,
            durationOfStay: additionalTraveller.durationOfStay,
        }
    }

    async enrollHouseholdForTraveller(confirmationNumber: string) {
        return await RTOPDbService.enrolHouseholdForTraveller(confirmationNumber);
    }

    /**
     * Look up a traveller record based on a travellers confirmation number.
     * 
     * The record returned contains the entire household form submission
     * with the primary travellers contact info replaced with the requested travellers contact info,
     * and additional travellers removed.
     * 
     * This transformation is required as AHS needs to be able to look up data for individual travellers,
     * not by household as this app has the data structured.
     * 
     * @param confirmationNumber 
     */
    async findByTravellerConfirmationNumber(confirmationNumber: string) {
        // If not matching test data, actually perform the lookup
        let household: EnrollmentFormRO = await this.enrollmentFormRepository.findByTravellerConfirmationNumber(confirmationNumber);

        // Find traveller record
        const travellerRecord = (household.additionalTravellers || []).find(t => t.confirmationNumber.toLowerCase() === confirmationNumber.toLowerCase());

        if(travellerRecord) {
            // If traveller record is found, override the households primary traveller info
            // with the info for traveller requested

            // TODO: This is currently naively done. Once new form fields have been added,
            // this might need to change to make sure the record doesn't contain any primary traveller info.
            household = Object.assign({}, household, travellerRecord);
        }

        // Make sure we return the travellers confirmation number
        // intead of household confirmation number for primary traveller
        household.confirmationNumber = confirmationNumber;

        // Remove additional travellers from the returned JSON
        // This is not needed by AHS
        delete household.additionalTravellers;

        return household;
    }

    async createDailySubmission(confirmationNumber: string, submission: DailySubmission, travellerId: number, reminder: ReminderIDRO): Promise<number> {
        const submissionId: number = await(await this.enrollmentFormRepository.createDailySubmission(submission, reminder)).idOnly();

        const status = DailyQuestionAnswerService.submissionCardStatus(submission);

        const reason: string = status === DAILY_STATUS.RED? CARD_STATUS_REASON.DAILY_SYMPTOM_FLAG : null;
        await this.enrollmentFormRepository.updateHouseholdCardStatus([confirmationNumber], status, reason, submissionId);
        await this.enrollmentFormRepository.updateTravellerCardStatus([reminder.travellerId], status);

        return submissionId;
    }

}
