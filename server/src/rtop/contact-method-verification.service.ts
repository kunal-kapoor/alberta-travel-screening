import { CONTACT_METHOD } from "../rtop-admin/repositories/daily-reminder.repository.";
import { EmailSendable, EmailService } from "../email/email.service";
import { TextSendable, TextService } from "../text.service";
import { Injectable, Logger } from "@nestjs/common";
import { RTOPDbService } from "../db/rtop-dbservice";
import { ContactMethodVerificationRepository } from "./repositories/contact-method-verification.repository";
import { EnrollmentFormSubmissionRO } from "./ro/form-submission.ro";
import { ContactMethodVerificationDTO } from "./dto/contact-method-verification.dto";
import { ContactMethodVerification } from "./entities/contact-method-verification";
import { VerificationTokenService } from "./verification-token.service";
import { CodeVerificationRO } from "./ro/code-verification.ro";
import { LoggerService } from "../logs/logger";

const fallbackText = `
Please use the following code to complete the Alberta COVID-19 Border Testing Pilot Program application: %VERIFICATION_CODE%
`;

class EmailVerificationEmail implements EmailSendable {
    from: string;
    to: any;
    subject: string;
    template = 'email-verification';
    context: any;

    constructor(to: string, data: any) {
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
        this.subject = process.env.EMAIL_VERIFICATION_EMAIL_SUBJECT || 'Alberta COVID-19 Border Testing Pilot Program Application';
    }
}

class SMSVerificationText implements TextSendable {
    to: string;
    message: string;

    constructor(phoneNumber: string, data: any) {
        this.to = phoneNumber;
        this.message = (process.env.SMS_VERIFICATION_TEXT || fallbackText)
            .replace('%VERIFICATION_CODE%', data.verificationCode)
            .replace('%DATE%', data.date);
    }
}

@Injectable()
export class ContactMethodVerificationService {
    constructor(
        private readonly emailService: EmailService,
        private readonly txtService: TextService,
        private readonly contactMethodVerificationRepository: ContactMethodVerificationRepository,
    ) {}
    
    /**
     * Find household by contact method token
     * @param contactMethodToken 
     */
    async validateContactMethod(contactMethodToken: string): Promise<ContactMethodVerification> {
        return await this.contactMethodVerificationRepository.findVerification(contactMethodToken);
    }

    /**
     * Update status of verification to verified + add household record reference
     * @param verification verification to update
     * @param household household to associate to 
     */
    async markAsVerified(verification: ContactMethodVerification, household: EnrollmentFormSubmissionRO) {
        return await this.contactMethodVerificationRepository.markAsVerified(verification, household);
    }
  
  
    /**
     * Sends verifications to verify the chosen contact method.
     * 
     * Phone In:
     * - No verification is required, but we're still creating a record for the event
     * - Return verificationToken to client so it immediately can be used to submit the enrollment form
     * 
     * Email
     * - Send Verification Token to email address as a magic link the user can click on to submit the enrollment form
     *
     * SMS
     * - Send a numeric verification code to the users phone
     * - Return verificationId to client
     * - Verification code (that user receives) and verification ID can be used to retrieve the verification token required to submit the
     *   enrolment form by calling `api/v2/rtop/traveller/verification/verify/code` 
     */
    async sendVerification(verification: ContactMethodVerificationDTO): Promise<CodeVerificationRO> {

        // Token that is needed to submit the enrolment form
        const verificationToken = VerificationTokenService.createToken(48); // Random token of 48 characters

        // Numeric verification code sent to user when SMS is selected
        const verificationCode = VerificationTokenService.createNumericCode(6); // Random numeric code of 6 characters

        // Verification id used together with the numeric verification code to
        // verify a phone number
        const verificationId = VerificationTokenService.createToken(48); // Random token of 48 characters

        await RTOPDbService.createContactMethodVerification(verificationToken, verification.contactMethod, verification.email, verification.phoneNumber, `${verificationCode}`, verificationId);
        const questionnaireLink = `${process.env.APP_URL}/${process.env.ENROL_SUBMISSION_CONFIRMATION_ROUTE || 'enroll'}/${verificationToken}`;

        switch (verification.contactMethod) {
            case CONTACT_METHOD.EMAIL: {
                await this.sendEmailVerification(verification.email, questionnaireLink);
                break;
            }

            case CONTACT_METHOD.SMS: {
                await this.sendSMSVerification(verification.phoneNumber, verificationCode);
                return new CodeVerificationRO({TOKEN: verificationId});
            }

            case CONTACT_METHOD.CALLIN:
                return new CodeVerificationRO({TOKEN: verificationToken});
        }

        return null;
    }

    /**
     * 
     * @param verification 
     */
    public async verifyCode(verificationId: string, code: string): Promise<CodeVerificationRO> {
        return await this.contactMethodVerificationRepository.verifyCode(verificationId, code);
    }

    /**
     * Sends verification text to verify a households phone number
     */
    private async sendSMSVerification(phoneNumber: string, verificationCode: number) {
        try {
            const msg = new SMSVerificationText(phoneNumber, {
                verificationCode
            });
            await this.txtService.sendText(msg);
        }
        catch (e) {
            LoggerService.logger.error('Failed to send sms verification', e);
            throw e;
        }
    }

    /**
     * Sends verification email to verify a households email address
     */
    private async sendEmailVerification(email: string, applicationLink: string) {
        try {
            const msg = new EmailVerificationEmail(email, {
                applicationLink
            });
            await this.emailService.sendEmail(msg);
        }
        catch (e) {
            LoggerService.logger.error('Failed to send email verification', e);
            throw e;
        }
    }

  
}