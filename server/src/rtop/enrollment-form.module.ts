import { Module } from '@nestjs/common';
import { EnrollmentFormService } from './enrollment-form.service';
import { EnrollmentFormValidationNumberGenerator } from './validation-number.service';
import { ServiceAlbertaRepository} from '../service-alberta/service-alberta.repository';
import { EnrollmentFormController } from './enrollment-form.controller';
import { EnrollmentFormRepository } from './repositories/enrollment-form.repository';
import { ServiceAlbertaService } from '../service-alberta/service-alberta.service';
import { EmailService } from '../email/email.service';
import { TextService } from '../text.service';
import { ContactMethodVerificationService } from './contact-method-verification.service';
import { ContactMethodVerificationRepository } from './repositories/contact-method-verification.repository';

const routes = {
  'controllers': [EnrollmentFormController],
  'imports': [],
};

@Module({
  providers: [
    ContactMethodVerificationService,
    EnrollmentFormService,
    EnrollmentFormRepository,
    EnrollmentFormValidationNumberGenerator,
    ContactMethodVerificationRepository,
    ServiceAlbertaRepository,
    ServiceAlbertaService,
    EmailService,
    TextService,

  ],
  imports: routes.imports || [],
  controllers: routes.controllers || [],
})
export class EnrollmentFormModule {}
