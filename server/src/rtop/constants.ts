import fs from 'fs';
import { Logger } from '@nestjs/common';

export enum DAILY_STATUS {
    RED = 'red',
    GREEN = 'green',
    YELLOW = 'yellow'
}

export enum CARD_STATUS_REASON {
    DAILY_SYMPTOM_FLAG = 'Daily Symptom Flag: Traveller has one of COVID symptoms',
    DAILY_TRACKING_FLAG = 'Daily Tracking not Completed: Traveller has not completed daily checkin by midnight',
    NOT_TESTED = 'Not tested after 9 days: Traveller has not been tested for 9 days'
}

class DailyQuestionAnswer {
    yes: string;
    no: string;
}

/**
 * Parse questions from daily questionnaire from either a file
 * or from env variable directly.
 * 
 * process.env.DAILY_QUESTIONS_FILE: JSON file with an object following the format below
 * process.env.DAILY_QUESTIONS_FILE: env variable containing an object following the format below
 * 
 * {
 *     <question_identifier>: {
 *         "<answer>": <answer token>
 *         ...
 *     }
 *     ...
 * }
 * 
 */
export const DAILY_QUESTIONS: Record<string, DailyQuestionAnswer> = (() => {
    if(process.env.DAILY_QUESTIONS_FILE) {
        try {
            const questions = fs.readFileSync(process.env.DAILY_QUESTIONS_FILE, 'utf-8');
            return Object.freeze(JSON.parse(questions));
        } catch(e) {
            Logger.error('Failed to parse questions file', e);
            throw e
        }
    } else if(process.env.DAILY_QUESTIONS) {
        return Object.freeze(JSON.parse(process.env.DAILY_QUESTIONS));
    } else {
        Logger.error('No questions file or questions env variable specified');

        throw new Error(`Either DAIL_QUESTIONS_FILE or DAILY_QUESTIONS env variable must be defined`);
    }
})();
