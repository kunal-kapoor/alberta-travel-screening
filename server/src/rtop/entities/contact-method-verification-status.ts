export enum ContactVerificationStatus {
    CREATED = 'created',
    VERIFIED = 'verified',
}