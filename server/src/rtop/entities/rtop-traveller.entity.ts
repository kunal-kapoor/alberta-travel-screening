
export class RTOPTraveller {
  firstName: string;
  lastName: string;
  confirmationNumber: string;

  dateOfBirth: Date;

  gender: string;
  citizenshipStatus: string;
  exemptionType: string;
  exemptOccupation: string;
  exemptOccupationDetails: string;
  dateLeavingCanada: string;
  seatNumber: string;
  reasonForTravel: string;
  durationOfStay: string;
}
