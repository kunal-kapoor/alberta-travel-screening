import { CONTACT_METHOD } from "../../rtop-admin/repositories/daily-reminder.repository."
import moment from "moment";

export class ContactMethodVerification {
    id: number;
    status: string;
    created: string;
    token: string;
    contactMethod: CONTACT_METHOD;
    email: string;
    phoneNumber: string;
    householdId: number;
    
    constructor(data) {
        const {ID, STATUS, CREATED, TOKEN, CONTACT_METHOD, EMAIL, PHONE_NUMBER, HOUSEHOLD_ID} = data;
        this.id = ID;
        this.status = STATUS;
        this.created = CREATED;
        this.token = TOKEN;
        this.contactMethod = CONTACT_METHOD;
        this.email = EMAIL;
        this.phoneNumber = PHONE_NUMBER;
        this.householdId = HOUSEHOLD_ID;
    }

    /**
     * Check if the token associated with this verification has expired
     */
    expired() {
        if(process.env.VERIFICATION_EXPIRY_SECONDS) {
            const timeSinceCreation = moment.duration(moment().diff(moment(this.created))).asSeconds();

            // Check that the token is still valid
            return timeSinceCreation > parseInt(process.env.VERIFICATION_EXPIRY_SECONDS);
        } else {
            return false;
        }
    }

    /**
     * Check if this contact method has been verified
     */
    verified() {
        return this.status === 'verified';
    }

    /**
     * Check if this contact can be verified and assigned to a household
     * - It cannot already be assigned
     * - The token must not be expired
     * - It cannot already be verified
     */
    available() {
        return this.status === 'created'
            && !this.expired()
            && !this.householdId;
    }
}
