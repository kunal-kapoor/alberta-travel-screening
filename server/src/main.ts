import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './logs/logger';
import { configureServer } from './serverconfig';
import { NestExpressApplication } from '@nestjs/platform-express';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';

const port = process.env.APP_PORT || 8080;

/** @type {http.Server|undefined} */
let server;

// shut down server
async function shutdown() {
  if (server) {
    server.close((err) => {
      if (err) {
        LoggerService.error('Failed to close error', err);
        process.exitCode = 1;
      }
      process.exit();
    });
  }
}

// quit on ctrl-c when running docker in terminal
process.on('SIGINT', () => {
  LoggerService.info('Got SIGINT (aka ctrl-c in docker). Graceful shutdown');
  shutdown();
});

// quit properly on docker stop
process.on('SIGTERM', () => {
  LoggerService.info('Got SIGTERM (docker container stop). Graceful shutdown');
  shutdown();
});


// Start server
(async () => {
  try {
    server = await NestFactory.create<NestExpressApplication>(AppModule);
    server.useLogger(server.get(WINSTON_MODULE_NEST_PROVIDER));
    await configureServer(server);

    await server.listen(port);
    LoggerService.info(`Listening on port ${port}`);
  } catch (err) {
    LoggerService.error('Failed to start server', err);
    shutdown();
  }
})();
//
// need above in docker container to properly exit
//

module.exports = server;
