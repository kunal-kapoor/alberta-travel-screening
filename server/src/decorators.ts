import { Transform, TransformationType } from "class-transformer";
import { SetMetadata, CustomDecorator } from "@nestjs/common";
import { ValidatorConstraintInterface, ValidationArguments, ValidatorConstraint } from "class-validator";
import moment from "moment";

/**
 * Decorator that wraps `Transform` to convert the given value between Yes/No and
 * and internal boolean representation.
 * 
 * Usage example:
 * @TransformBooleanString()
 * hasAdditionalTravellers: boolean;
 */

export const TransformBooleanString = ():any => Transform((value, obj, transformationType: TransformationType) => {
    if(transformationType === TransformationType.CLASS_TO_PLAIN) {
        // Don't convert value if already converted
        if(typeof value === 'string' || value instanceof String) {return value;}

        return value === true? 'Yes': 'No';
    } else if (transformationType === TransformationType.PLAIN_TO_CLASS){
        return value === 'Yes';
    } else {
        return value;
    }
});

/**
 * Decorator that indicates to the APIAuthGuard that the given endpoint is public.
 * - Sets isPublicRoute to true on the current execution context
 * - The guard will allow the request if this is set on the current execution context
 */
export const PublicRoute = (): CustomDecorator => SetMetadata( "isPublicRoute", true );
  
/**
 * Validator that validates that the annotated Array property
 * has a length < 10
 */
@ValidatorConstraint({ name: "maxArrayLength", async: false })
export class MaxArrayLength implements ValidatorConstraintInterface {
    /**
     * Validate that array is < specified length
     * @param prop The array to validate
     * @param args validation arguments. Max size is at index 0 of args
     */
    validate(prop: Array<any>, args: ValidationArguments) {
        return prop.length <= args.constraints[0];
    }
}

/**
 * Validator that checks if the given date string is <= today.
 */
@ValidatorConstraint({ name: "isNotFutureDate", async: false })
export class IsNotFutureDate implements ValidatorConstraintInterface {

    /**
     * Returns true if dateStr <= today
     * @param dateStr date string (YYYY/MM/DD)
     * @param args validation args
     */
    validate(dateStr: string, args: ValidationArguments) {
        const date = moment(dateStr, 'YYYY/MM/DD');

        return date.isSameOrBefore(new Date());
    }

    defaultMessage(args: ValidationArguments) {
      return `"${args.property}" must not be in the future"`;
    }
}

/**
 * Validates that the given array is empty when the associated property is falsy.
 * 
 * @Validate(IsEmptyIfNotToggled, 'hasAdditionalTravellers')
 * additionalTravellers:Array<AdditionalTravellers>
 */
@ValidatorConstraint({ name: "emptyIfNotToggled", async: false })
export class IsEmptyIfNotToggled implements ValidatorConstraintInterface {
    validate(prop: Array<any>, args: ValidationArguments) {
        return args.object[args.constraints[0]]? true: prop.length === 0;
    }

    defaultMessage?(args?: ValidationArguments): string {
        return `${args.property} must not be set when ${args.constraints[0]} is set to No`;
    }
}


export const Roles = (...roles: any[]) => {
    return SetMetadata('roles', roles);
}