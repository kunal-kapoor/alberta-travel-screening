import { IsString, IsOptional, IsBoolean, Length } from 'class-validator';

export class SubmissionDeterminationDTO {

  @IsString()
  @Length(1,128)
  determination: string;

  @IsString()
  @Length(1,2048)
  notes: string;

  @IsOptional()
  @IsBoolean()
  exempt: boolean
}
