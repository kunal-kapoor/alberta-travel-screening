import { IsBoolean, IsString, Length, IsOptional } from 'class-validator';
import { TransformBooleanString } from '../../decorators';

export class QuarantineLocationDTO {
  @IsString()
  @Length(1, 1024)
  address: string;

  @IsString()
  @Length(1, 255)
  cityOrTown: string;

  @IsString()
  @Length(1, 255)
  provinceTerritory: string;

  @IsOptional()
  @IsString()
  @Length(0, 32)
  postalCode: string;

  @IsString()
  @Length(1, 35)
  phoneNumber: string;

  @IsString()
  @Length(1, 512)
  typeOfPlace: string;

  @IsString()
  @Length(1, 512)
  howToGetToPlace: string;

  @IsBoolean()
  @TransformBooleanString()
  doesVulnerablePersonLiveThere: boolean;

  @IsBoolean()
  @TransformBooleanString()
  otherPeopleResiding: boolean;
}
