import { IsString, Length, IsOptional, IsNumber, Min } from 'class-validator';

export class UnwillingTravellerDTO {
  @IsNumber()
  @Min(1)
  numTravellers: number;
  
  @IsString()
  @Length(0, 5000)
  @IsOptional() 
  note: string;
}
