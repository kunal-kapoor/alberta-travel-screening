import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logs/logger';
import { ArrivalForm } from '../entities/arrival-form.entity';
import { SubmissionDeterminationDTO } from '../dto/submission-determination.dto';
import { ArrivalFormSearchResultRO } from "../ro/arrival-form-search.ro";
import { ArrivalFormRO } from "../ro/arrival-form.ro";
import { ServiceAlbertaFormQuery } from "../../service-alberta/dto/form-query.dto";
import { ActivityDTO } from '../../service-alberta/dto/activity.dto';
import { db2Connection } from '../../db/db2connection';
import { DbService } from '../../db/dbservice';
import { SearchQueryBuilder } from './search-query-builder';
import { EditFormDTO } from '../dto/edit-form.dto';
import { QuarantineLocation } from '../entities/quarantine-location.entity';

@Injectable()
export class ArrivalFormRepository {

    /**
     *
     * Counts the number of records with the corresponding confirmation number
     * This function is used to verify that a record exists
     *
     * @param code - confirmation number
     *
     * @returns The number of records with a matching confirmation number
     */
    async countByConfirmationNumber(code: string): Promise<number> {
        return await DbService.countByNum(code);
    }

    /**
     * Counts the number of forms in the database
     *
     * @returns - The number of matching records
     */
    async count(): Promise<number> {
        return await DbService.getCount('form');
    }

    /**
     * Updates the quarantineLocation of a form with the given values
     *
     * @param agentId - ID of the agent making the change
     * @param arrivalFormId - The arrival form id to update
     * @param updatedFields - The new quarantine location values
     */
    async editForm(agentId: string, arrivalForm: ArrivalFormRO, updatedFields: EditFormDTO): Promise<void> {
        try {
            const updateRequest = JSON.stringify(
                {"$set":
                    {
                        hasPlaceToStayForQuarantine: updatedFields.hasPlaceToStayForQuarantine,
                        quarantineLocation: updatedFields.hasPlaceToStayForQuarantine? updatedFields.quarantineLocation: undefined
                    }
                }
            );

            // Actually update form with new values
            await DbService.editForm(agentId, arrivalForm.id, updateRequest);
        } catch(e) {
            LoggerService.error(`Failed to edit form with id ${arrivalForm.id}`, e);
            throw e;
        }
    }

    /**
     *
     * Update a form with an officer's determination
     *
     * @param arrivalFormId - The record id
     * @param determination - The result of the officer's review
     *
     * @returns void
     */
    async submitDetermination(arrivalFormId: number, determination: SubmissionDeterminationDTO): Promise<void> {
        try {
            const updateField = JSON.stringify(
                {"$set":
                    {"determination":
                        {
                            notes:determination.notes,
                            determination:determination.determination,
                            exempt:determination.exempt,
                        }
                    }
                }
            );
            await DbService.submitDetermination(updateField,determination.exempt===true, arrivalFormId);
        } catch (e) {
            LoggerService.error(`Failed to submit determination for form ${arrivalFormId}`, e);
            throw e;
        }
    }

    /**
     *
     * Find an arrival form by id
     *
     * @param id - Id of the record
     *
     * @returns Arrival form
     */
    async findOne(id: number): Promise<ArrivalFormRO> {
        try {
            const res = await (await DbService.getForm(id)).single();
            return res;
        } catch (e){
            LoggerService.error('Failed to find form', e);

            throw e;
        }
    }

    /**
     * Find a record by confirmation number
     *
     * @param confirmationNumber
     *
     * @returns - Arrival form with the matching confirmation number
     */
    async findByConfirmationNumber(confirmationNumber: string): Promise<ArrivalFormRO> {
        const res = await(await DbService.getFormByNumber(confirmationNumber))
            .single();
        return res;
    }

    /**
     * Find all arrival forms that have a traveller with the corresponding last name.
     *
     * @param lname - Last name of the record
     *
     * @returns - An array of forms.
     */
    async findByLastName(lname: string): Promise<ArrivalFormSearchResultRO> {
        if (!lname) { return { travellers: [] } }

        const query = lname.length === 2? DbService.getFormByLastNameExact: DbService.getFormByLastName;

        const travellers = await (await query(lname))
            .multiple();

        return new ArrivalFormSearchResultRO(travellers);
    }

    /**
     * Saves an arrival form
     *
     * @param form - form to save
     *
     * @returns - The arrival form
     */
    async save(form: ArrivalForm): Promise<ArrivalForm> {
        const id:number = await(await DbService.saveForm(form)).idOnly();

        form.id = id;

        return form;
    }

    async getFilteredFormList(user:any, query: ServiceAlbertaFormQuery):Promise<any>{
        return new SearchQueryBuilder(user)
            .query(query.query)
            .filter(query.filter)
            .agent(query.agent)
            .fromDate(query.fromDate)
            .toDate(query.toDate)
            .orderBy(query.orderBy, query.order)
            .paginate(query.page, 20)
            .execute();
    }

    async addActivity(activity:ActivityDTO,userId:string,formId:number):Promise<void>{
        await DbService.addActivity(activity, userId, formId);
    }

    async updateStatus(status:string,id:number):Promise<boolean>{
        const conn = await db2Connection()
        try{
            await DbService.updateStatus(status, id);
            return true;
        }catch(err){
            return false;
        }finally{
            await conn.close();
        }

    }
}
