import { arrivalFormQuery } from '../../db/db2connectionUtils';
import { COMPLETED_STATUSES } from "../arrival-form.constants";
import { DbService } from '../../db/dbservice';
import moment from 'moment';

/**
 * Utility class for searching form recoords
 */
export class SearchQueryBuilder {

    args = [];
    queryStr = '';

    filterStr = '';
    orderByStr = '';
    agentStr = '';
    fromDateStr = '';
    toDateStr = '';

    pageStr = '';
    splitNulls = false;
    splitColumnName = "";

    constructor(private readonly user) {}

    /**
     * Adds a WHERE clause which filter based on the property passed in
     * @param filter filter type to search for (unassigned/completed/mine)
     */
    filter(filter): SearchQueryBuilder {
        this.filterStr = '';

        if(filter === "unassigned"){
            this.filterStr += `(f.assigned_to IS NULL) AND (f.status NOT IN (${
                //Take the list of completed statuses, turn it to json
                (JSON.stringify(COMPLETED_STATUSES)
                  // replace all double quotes with single quotes. 
                  .replace(/"/g,'\'')
                  // remove the brackets
                  .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
              }) OR f.status IS NULL) AND `;
        }else if (filter === 'completed'){
            this.filterStr += `(f.status IN (${
                //Take the list of completed statuses, turn it to json
                (JSON.stringify(COMPLETED_STATUSES)
                // replace all double quotes with single quotes. 
                .replace(/"/g,'\'')
                // remove the brackets
                .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
            })) AND `
        }else if(filter ==="mine"){
            this.filterStr += `(f.assigned_to = ? ) AND (f.status NOT IN (${
              //Take the list of completed statuses, turn it to json
              (JSON.stringify(COMPLETED_STATUSES)
                // replace all double quotes with single quotes. 
                .replace(/"/g,'\'')
                // remove the brackets
                .substring(1,JSON.stringify(COMPLETED_STATUSES).length-1))
            }) OR f.status IS NULL) AND `
            this.args.push(this.user.id)
        }

        return this;
    }

    /**
     * Allow filtering of records by agents.
     * Only exact matches are allowed.
     *
     * @param name Name of agent to filter by
     */
    fromDate(fromDate: Date) {
        this.fromDateStr = '';

        if(fromDate) {
            const dtStr = moment.utc(fromDate).format('YYYY-MM-DD HH:mm:ss');

            this.fromDateStr = ` f.determination_date >= ? AND `;
            this.args.push(dtStr);
        }

        return this;
    }

    /**
     * Allow filtering of records by agents.
     * Only exact matches are allowed.
     *
     * @param name Name of agent to filter by
     */
    toDate(toDate: Date) {
        this.toDateStr = '';

        if(toDate) {
            const dtStr = moment.utc(toDate).format('YYYY-MM-DD HH:mm:ss');

            this.toDateStr = ` f.determination_date < ? AND `;
            this.args.push(dtStr);
        }

        return this;
    }

    /**
     * Allow filtering of records by fromDate.
     *
     * @param fromDate date to filter by
     */
    agent(name: string) {
        this.agentStr = '';

        if(name && name.trim() && name.trim() !== 'all') {
            name = name.trim();

            this.agentStr = ' a.name=? AND ';
            this.args.push(name);
        }

        return this;
    }


    /**
     * Adds a order by clause to the query.
     * @param orderby Property to order by date | traveller | owner | status
     * @param order asc|desc (default)
     */
    orderBy(orderby, order): SearchQueryBuilder {
        this.orderByStr = '';

        if(orderby && order){
            let orderval = "";
            switch (orderby){
                // Use date as the default case
                case "date":
                default:
                    orderval = "f.determination_date";
                    break;
                case "firstName":
                    orderval = "f.first_name";
                    break;
                case "lastName":
                    orderval = "f.last_name";
                    break;
                case "owner":
                    orderval = "a.name";
                    break;
                case "status":
                    orderval = "f.status";
                    break;
                case "confirmationNumber":
                    orderval = "f.confirmation_number";
                    break;
                case "lastUpdated":
                    orderval = "f.last_updated";
                    this.splitNulls = true;
                    this.splitColumnName = orderval;
                    break;

            }
            this.orderByStr += ` ORDER BY ${orderval} ${order==="asc"?"ASC":"DESC"} `
        }
        return this;
    }

    /**
     * Filters query by confirmationNumber or lastName
     * @param query Query to filter by
     */
    query(query): SearchQueryBuilder {
        this.queryStr = '';
        if(query){
            this.queryStr = `( (LOWER(f.confirmation_number) LIKE ? || '%')
            OR (LOWER(f.last_name) LIKE ? || '%')
            OR (LOWER(f.first_name) LIKE ? || '%')) AND `
            this.args.push(query.toLowerCase());
            this.args.push(query.toLowerCase());
            this.args.push(query.toLowerCase());
        }

        return this;
    }

    /**
     * Adds pagination to the query.
     * @param page Current page (0 default)
     * @param pageSize Size of page
     */
    paginate(page, pageSize): SearchQueryBuilder {
        page = page? parseInt(page): 0;

        this.pageStr =`
            OFFSET ${pageSize * page} ROWS
            FETCH FIRST ${pageSize} ROWS ONLY
        `;

        return this;
    }

    /**
     * 
     * Splits the query into two queries
     *  - One with columnName is null
     *  - Other with columnName without null and ordered
     * After splitting, the values are unified and pagination is applied
     * 
     * @param query - Query to be split and unified
     * @param orderByStr - The order by attribute for the query
     * @param pageStr - Pagination to be applied for the query
     * @param columnName - Column which is split for null and not null
     */
    private splitNullsWithUnion(query:string, orderByStr:string, pageStr:string, columnName:string): string {
        const unionQueryStr = `${query} AND ${columnName} is null UNION ALL select * from (${query} AND ${columnName} is not null ${orderByStr})  ${pageStr}`;
        return unionQueryStr;
    }

    /**
     * Formats and executes a search query
     */
    async execute() {
        const qs = select => `
            select ${select}
            FROM form f
            LEFT JOIN agent a
            ON a.id = f.assigned_to
            WHERE `;

        // Statement to execute (eventually)
        let stmt = '';

        const formSelect = DbService.FormSelect;
        const countSelect = DbService.CountSelect;

        let selectQuery = qs(formSelect);
        let argsList = this.args;

        if(this.queryStr) {
            // Add query filter
            stmt += this.queryStr;
        }

        if(this.filterStr) {
            // Add filter
            stmt += this.filterStr;
        }

        if(this.agentStr) {
            // Filter query by agent
            stmt += this.agentStr;
        }

        if(this.fromDateStr) {
            // Filter query by fromDate
            stmt += this.fromDateStr;
        }

        if(this.toDateStr) {
            // Filter query by toDate
            stmt += this.toDateStr;
        }

        // All filter statements add an AND at the end. Make sure we terminate that correctly
        stmt += " f.exempt IS FALSE AND f.determination_date IS NOT NULL ";


        // Find total number of results without pagination
        const numberOfResults = await (await arrivalFormQuery(qs(countSelect) + stmt, argsList)).count();

        if( this.splitNulls )
        {
            selectQuery = this.splitNullsWithUnion(qs(formSelect) + stmt , this.orderByStr, this.pageStr, this.splitColumnName);
            
            // Since the query is repeated twice for with and without null values, the args value list is repeated
            argsList = [...this.args,...this.args]
            
        }
        else{
            // Add order by + pagination
            if(this.orderByStr) {
                stmt += this.orderByStr;
            }

            if(this.pageStr) {
                stmt += this.pageStr;
            }

            selectQuery += stmt
        }
        
        // Find actual results (paginated)
        const results = await (await arrivalFormQuery(selectQuery, argsList)).multiple()
        return [selectQuery, results, numberOfResults];
    }
}
