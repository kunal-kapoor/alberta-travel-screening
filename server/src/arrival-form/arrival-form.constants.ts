import { IsNotEmpty, IsString, Length, Matches, Validate } from 'class-validator';
import { IsNotFutureDate } from '../decorators';

export class CitiesAndCountries {
  @IsString()
  @Length(1, 255)
  cityOrTown: string;

  @IsString()
  @Length(1, 255)
  country: string;
}

export class AdditionalTravellers {
  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  lastName: string;

  @IsNotEmpty()
  @Matches(/^\d{4}\/\d{2}\/\d{2}$/)
  @Validate(IsNotFutureDate)
  dateOfBirth: Date;
}

export function stringToBoolean(yesOrNo: string): boolean {
  if (yesOrNo === 'Yes' || yesOrNo === 'No') {
    return yesOrNo === 'Yes'
  } else {
    return false
  }
}

export function booleanToString(bool:boolean):string{
  return bool? 'Yes':'No'
}

export const COMPLETED_STATUSES = [
  'Not called within 7 days',
  'Filled out form online',
  'No answer, 3 call attempts made',
  'Contact made, no issues',
  'Contact made, would not provide info',
  'Contact made, essential/exempt worker',
  'Contact made, essential/exempt worker',
  'Contact made, other',
  'Contact made, indicated unwilling to self-isolate',
  'Incorrect contact info provided'
]
export const ALL_STATUSES = [].concat(
  COMPLETED_STATUSES, 
  [  
  'None',
  '1st call made, no contact',
  '2nd call made, no contact',
  ])