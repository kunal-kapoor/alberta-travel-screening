import { Injectable } from "@nestjs/common";
import testData from '../db/seed-data/arrival-form-seed-data.json'
import { ArrivalFormService } from "./arrival-form.service";
import { ArrivalFormRepository } from "./repositories/arrival-form.repository";
import { LoggerService } from "../logs/logger";
import { plainToClass } from "class-transformer";
import { ArrivalForm } from "./entities/arrival-form.entity";
import { ArrivalFormDTO } from "./dto/arrival-form.dto";

export interface SeedBuilder {
    transform: (cb: (cb: Array<any>) => Array<any>) => SeedBuilder
    create: () => void
}

/**
 * Provides utilities for seeding the DB with entries for testing purposes
 */
@Injectable()
export class ArrivalFormSeedService {
    arrivalFormRepository: ArrivalFormRepository;
    arrivalFormService: ArrivalFormService;

    constructor(
        arrivalFormService: ArrivalFormService,
        arrivalFormRepository: ArrivalFormRepository
      ) {
        this.arrivalFormService = arrivalFormService;
        this.arrivalFormRepository = arrivalFormRepository;
    }

    // Seed the db with testing data
    async seedTestData(): Promise<ArrivalForm[]> {
        return this.seed(testData.forms);
    }

    /**
     * Creates a builder that creates numResults DB form entries, each randomly picked from the given seed json.
     * Allows the caller to transform the results before being saved to the db
     * @param numResults
     *
     */
    async seedBuilder(numResults: number): Promise<SeedBuilder> {
        let entries = await this.seedEntities(numResults);

        const builder: SeedBuilder = {
            transform: cb => {
                entries = cb(entries);

                return builder;
            },
            create: () => this.seed(entries)
        };

        return builder;
    }

    async seedEntities(numResults: number): Promise<any[]> {
        const items = testData.forms;
        return [...Array(numResults)]
            .map(() => items[Math.floor(Math.random() * items.length)])
            // Do a deep copy of <item> to make sure to remove all references to the original item
            .map(item => JSON.parse(JSON.stringify(item)));
    }

    /**
     * Seeds the db with the given elements that map 1<->1 with the ArrivalForm db model.
     * @param elements
     */
    async seed(elements): Promise<ArrivalForm[]> {
        if(await this.arrivalFormRepository.count() > 0) {
            LoggerService.info('Arrival repository has already been seeded, skipping');
            return [];
        }
        const elems = [];

        for(const el of elements) {
            try {
                // Apply class-transform transformations
                const transformedElem = plainToClass(ArrivalFormDTO, el);

                // Apply manual transformations
                const mappendEl = this.arrivalFormService.mapArrivalFormDtoToEntity(transformedElem, el.confirmationNumber);
                elems.push(await this.arrivalFormRepository.save(mappendEl));
            } catch(e) {
                LoggerService.error('Failed to seed testing data', e);

                throw e;
            }
        }

        return elems;
    }
}
