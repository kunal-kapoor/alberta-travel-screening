import { APIStrategy } from "ibmcloud-appid";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { LoggerService } from "../../logs/logger";
import { AppIDConfig } from "../envconfig";

@Injectable()
export class GOAPassportStrategy extends PassportStrategy(APIStrategy, "goa") {
  /**
   * Sets up GOA App ID configuration
   * Implements APIStrategy for passport
   */
  constructor() {
    // AppID configuration
    const envConfig = AppIDConfig('GOA');

    super({
      oauthServerUrl: (envConfig.OAUTH_SERVER_URL || 'aurl').trim(),
      tenantId: (envConfig.TENANT_ID ||'anid').trim(),
    });
    LoggerService.info("GOA AppID registered");
  }
}
