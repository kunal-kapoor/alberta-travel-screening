import {
    ExecutionContext,
    Injectable,
    UnauthorizedException,
    CanActivate,
  } from '@nestjs/common';
import { LoggerService } from "../logs/logger";
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { AHSGuard } from './ahs/ahs.guard';
import { GOAGuard } from './goa/goa.guard';
import { OthersGuard } from './others/others.guard';
import { Role } from './roles.enum';

@Injectable()
export class APIAuthGuard implements CanActivate {
  ahsGuard:AHSGuard;
  goaGuard:GOAGuard;
  othersGuard:OthersGuard;
  
  constructor( private readonly reflector: Reflector
    ) {
      this.ahsGuard = new AHSGuard();
      this.goaGuard = new GOAGuard();
      this.othersGuard = new OthersGuard();
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    
    // Allow the request if `isPublicRoute` is set as metadata on the given execution context
    // This happens for endpoints with the @PublicRoute decorator
    const isPublic = this.reflector.get<boolean>( "isPublicRoute", context.getHandler() );

    if(isPublic) {
      return true;
    }
    const req = context.switchToHttp().getRequest();
    switch(req.headers.authissuer){
      default:
        return false;
      case Role.AHS:
        return (this.ahsGuard.canActivate(context));
      case Role.GOA:
        return this.goaGuard.canActivate(context);
      case Role.OTHER:
        return (this.othersGuard.canActivate(context));
    }
  }

  handleRequest(err: any, user: any, info: any): any {
    if (err || !user) {
      // Login failed
      LoggerService.info(info);
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
