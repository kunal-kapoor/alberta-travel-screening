import { LoggerService } from "../logs/logger";
import { HttpException, HttpStatus } from "@nestjs/common";

export const nameOrEmail = user => {
    if(!user.name && !user.email && !user.sub) {
        const userStr = JSON.stringify(user || {});
        LoggerService.error(`Failed to determine name of user ${userStr}`);

        throw new HttpException('Failed to determine user name', HttpStatus.BAD_REQUEST);
    }

    return user.name || user.email || user.sub;
};
