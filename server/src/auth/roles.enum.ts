export enum Role {
    AHS = 'AHS',
    GOA = 'GOA',
    OTHER = 'OTHER'
  }

  export enum Scope {
    SCREENER = 'screener_portal',
    ADMIN = 'admin_report_access'
  }