import { APIStrategy } from "ibmcloud-appid";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable } from "@nestjs/common";
import { LoggerService } from "../../logs/logger";
import { AppIDConfig } from "../envconfig";

@Injectable()
export class AHSPassportStrategy extends PassportStrategy(APIStrategy, "ahs") {
  /**
   * Sets up App ID configuration
   * Implements APIStrategy for passport
   */
  constructor() {
    // AppID configuration
    const envConfig = AppIDConfig('AHS');

    super({
      oauthServerUrl: (envConfig.OAUTH_SERVER_URL || 'aurl').trim(),
      tenantId: (envConfig.TENANT_ID ||'anid').trim(),
    });
    LoggerService.info("AHS APP ID registered");
  }
}
