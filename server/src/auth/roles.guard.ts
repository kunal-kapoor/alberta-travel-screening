
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { isString } from 'util';
import { ACLRepository } from '../service-alberta/acl.repository';

@Injectable()
export class RolesGuard implements CanActivate {

    reportRepository: ACLRepository;
    constructor(private reflector: Reflector) {
        this.reportRepository = new ACLRepository();
    }

    /**
     * Restrict access based on
     * - A users authentication issuer (GOA | AHS | OTHER)
     * - Oauth Scopes granted by IBM appID
     * - Internal scopes granted to email addresses by the report access dashboard
     */
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const roles = this.reflector.get<any[]>('roles', context.getHandler());
        if (!roles) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const userScope = request.appIdAuthorizationContext?.accessTokenPayload?.scope;

        // Look up scopes granted to user
        // TODO: Should rename report access to something more applicable
        const internalScopes = request.user?.email? await this.reportRepository.findReportAccessByAgentEmail(request.user.email): [];

        if (roles.includes("REPORTACCESS")) {
            // Manual check to verify that you have access to the report you're requesting
            return internalScopes.reportName.find((ele) => ele === request.params.reportName);
        } else if (!roles.find((e) => {
            if (isString(e)) {
                // Check auth issuer
                // @Roles('GOA')
                return e === request.headers.authissuer;

            } else if(e.ROLE || e.SCOPE || e.INTERNAL_SCOPE) {
                let allowed = true;

                if(e.ROLE) {
                    // Match Auth Issuer (GOA/AHS/OTHER)
                    // @Roles({ROLE: 'GOA'})
                    allowed = allowed && e.ROLE === request.headers.authissuer;
                }

                if(e.SCOPE) {
                    // Match Scope provided by AppID
                    // @Roles({SCOPE: 'admin_dashboard'})
                    allowed = allowed && userScope.includes(e.SCOPE);
                }

                if(e.INTERNAL_SCOPE) {
                    // Match internal scope provided by "report access dashboard"
                    // @Roles({INTERNAL_SCOPE: 'monitoring-dashboard'})
                    allowed = allowed && internalScopes.reportName.includes(e.INTERNAL_SCOPE);
                }

                return allowed
            }
        })) {
            return false;
        } else {
            return true;
        }

    }
}