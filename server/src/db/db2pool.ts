import { Pool } from 'ibm_db';

export class DB2Pool {
    connStr: string;
    pool: Pool;
    name: string;

    constructor(hostname, certificate, name) {
        const DB_NAME = process.env.database;
        const UID = process.env.username;
        const PWD = process.env.password;
        const PORT = process.env.port;
        const USE_SSL = process.env.useSSL === 'true';
        const SCHEMA = process.env.schema || 'selfiso';
        this.name = name;

        this.connStr = `DATABASE=${DB_NAME};HOSTNAME=${hostname};UID=${UID};PWD=${PWD};PORT=${PORT};PROTOCOL=TCPIP;CURRENTSCHEMA=${SCHEMA};`;
        
        if (USE_SSL) {
            this.connStr = `${this.connStr}Security=SSL; SSLServerCertificate=${certificate}`;
        }
        
        this.pool = new Pool();
        this.pool.setMaxPoolSize(parseInt(process.env.MAX_CONNECTION_POOL_SIZE || "15"));
    }
    open(cb) {
        this.pool.open(this.connStr, cb);
    }
}
