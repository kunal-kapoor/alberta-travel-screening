import { Household } from "../rtop/entities/household.entity";
import { query } from "./db2connection";
import { EnrollmentFormRO } from "../rtop/ro/enrollment-form.ro";
import { ServiceAlbertaFormRO } from "../service-alberta/ro/form.ro";
import { HttpException, HttpStatus } from "@nestjs/common";
import { HouseholdMonitoringRO } from "../rtop/ro/household-monitoring.ro";

/**
 * Provides utilities to transform results of Household queries
 * to a form that's more easier to work with
 */
class EnrollmentFormResultTransformer {
    constructor(private result) {}

    count = ():number => {
        const [{COUNT=0}] = this.result || [{COUNT: 0}];
        return COUNT;
    }

    /**
     * Returns a ArrivalFormDTO as the FORM_RECORD column of the first
     * row in the query results.
     */
    single = async ():Promise<EnrollmentFormRO> => {
        const [form] = this.result || [null];
        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            
            record.id = form['ID'];
            record.determinationDate = form['ARRIVAL_DATE'];
            record.enrollmentStatus = form['ENROLLMENT_STATUS'];
            record.householdCardStatus = form['CARD_STATUS']
            return new EnrollmentFormRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleRaw = async (): Promise<Household> => {
        const [form] = this.result || [null];
        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.id = form['ID'];
            record.determinationDate = form['ARRIVAL_DATE'];
            return record;
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleService = async ():Promise<ServiceAlbertaFormRO> => {
        const [form] = this.result || [null];

        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.agent = form['AGENTNAME'];
            record.agentId = form['AGENTID'];

            // For Service-Alberta purposes, the date of determination submission
            // should be considered as the travellers Arrival Date
            record.arrivalDate = form['ARRIVAL_DATE'];
            record.id = form['ID'];
            return new ServiceAlbertaFormRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleMonitoring = async ():Promise<HouseholdMonitoringRO> => {
        const [form] = this.result || [null];

        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.agent = form['AGENTNAME'];
            record.agentId = form['AGENTID'];
            record.enrollmentStatus = form['ENROLLMENT_STATUS'];
            record.householdCardStatus = form['CARD_STATUS'];
            record.email = form['CONTACT_EMAIL'] || record.email;
            record.phoneNumber = form['CONTACT_PHONE_NUMBER'] || record.phoneNumber;
            record.contactMethod = form['CONTACT_METHOD'];

            // For Service-Alberta purposes, the date of determination submission
            // should be considered as the travellers Arrival Date
            record.id = form['ID'];
            return new HouseholdMonitoringRO(record);
        }
        
        return null;
    }

    /**
     * Returns a list of Household as the FORM_RECORD columns
     * from all the search results
     */
    multiple = async (): Promise<Household[]> => {
        return this.result.map(({ID, FORM_RECORD,STATUS, ENROLLMENT_STATUS, OWNER,OWNERID, LAST_UPDATED, ARRIVAL_DATE, UPDATED_TIME}) => {
            const rec = JSON.parse(FORM_RECORD);
            rec.id = ID;
            rec.status = STATUS;
            rec.enrollmentStatus = ENROLLMENT_STATUS;
            rec.owner = OWNER;
            rec.lastUpdated = UPDATED_TIME;
            rec.assignedTO = OWNERID;
            rec.determinationDate = ARRIVAL_DATE;
            return rec;
        });
    }

    multipleTraveller = async(): Promise<any>=> {
        return this.result.map(({FIRST_NAME, LAST_NAME, CONFIRMATION_NUMBER, SUBMISSION_STATUS, ARRIVAL_DATE, STATUS, DATE, DOB_ATTEMPT, DOB_ATTEMPT_TIME}) => {
            return {            
                firstName: FIRST_NAME,
                lastName: LAST_NAME,
                confirmationNumber: CONFIRMATION_NUMBER,
                submissionStatus: SUBMISSION_STATUS,
                status: STATUS,
                date: DATE,
                arrivalDate: ARRIVAL_DATE,
                dobVerificationAttempt: DOB_ATTEMPT,
                dobAttemptTime: DOB_ATTEMPT_TIME
            };
        });
    }

    /**
     * Returns the id of the first row of the results.
     */
    idOnly = async (): Promise<number> => {
        if(!this.result || !this.result.length) {return null}
        const [{ID=null}] = this.result || [];

        return ID;
    }

    value = ():any => this.result;
}

export async function enrollmentFormQuery(queryStr, queryArgs?) {
    const res = await query(queryStr, queryArgs);
    return new EnrollmentFormResultTransformer(res);
}