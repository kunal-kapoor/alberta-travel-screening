import { Injectable } from "@nestjs/common";
import { query } from "./db2connection";
import { enrollmentFormQuery } from "./rtop-db2connectionUtils";
import { IsolationPlanReportProperty } from "../arrival-form/repositories/isolation-plan.repository";
import { IsolationPlanReport } from "../arrival-form/entities/isolation-plan-report.entity";
import { Household, EnrollmentStatus } from "../rtop/entities/household.entity";
import moment from "moment";
import { UnwillingTravellerRO } from "../arrival-form/ro/unwilling-traveller.ro";
import { AgentDTO } from "../service-alberta/service-alberta.repository";
import { CheckPointResultRO } from "../service-alberta/ro/checkpoint-report.ro";
import { NOTIFICATION_STATUS, CONTACT_METHOD } from "../rtop-admin/repositories/daily-reminder.repository.";
import { RTOPTraveller } from "../rtop/entities/rtop-traveller.entity";

import { HouseholdTravellersQueryStatusRO } from "../rtop/ro/household-submission-status.ro";
import { DAILY_STATUS, CARD_STATUS_REASON } from "../rtop/constants";
import { ContactMethodVerification } from "../rtop/entities/contact-method-verification";
import { ContactVerificationStatus } from "../rtop/entities/contact-method-verification-status";
import { ContactMethodVerificationDTO } from "../rtop/dto/contact-method-verification.dto";
import { DATE_FORMAT } from "../rtop/enrollment-form.constants";

@Injectable()
// TODO: all the queries are for the arrival form -> kindly change them accordingly
export class RTOPDbService {
    // Enrollment form queries
    private static readonly AddForm: string = `
        select id from final table(
            insert into household(
                form_record,
                last_name,
                first_name,
                confirmation_number,
                arrival_date,
                contact_phone_number,
                contact_email,
                phone_number,
                email,
                enrollment_status,
                contact_method
            )
            values (SYSTOOLS.JSON2BSON(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        )`;
    private static readonly FindByLastName: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from household WHERE LOWER(last_name) LIKE ? || '%'`;
    private static readonly FindByLastNameExact: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from household WHERE LOWER(last_name) = ?`;
    private static readonly FindByTravellerConfirmationNumber: string = `
        select h.id, SYSTOOLS.BSON2JSON(h.form_record) as form_record
        from household as h
        join rtop_traveller t on t.household_id=h.id
        WHERE LOWER(t.confirmation_number)=?`;
    private static readonly FindByNumber: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record, enrollment_status, card_status from household WHERE LOWER(confirmation_number)=?`;
    private static readonly FindById: string = `select id, SYSTOOLS.BSON2JSON(form_record) as form_record from household where id=?`;

    private static readonly FindTravellerByConfirmationNumber: string = `
        select * from rtop_traveller r
        where LOWER(r.confirmation_number)=?
    `;

    // Submit determination
    private static readonly SubmitDetermination: string = `
        update household SET
            form_record=SYSTOOLS.JSON_UPDATE(form_record,?),
            exempt=?,
            determination_date=MIN(CURRENT_TIMESTAMP, arrival_date)
        where id=?`;

    private static readonly EditForm: string = `
        update household SET
            form_record=SYSTOOLS.JSON_UPDATE(form_record,?)
        where id=?`;

    private static readonly CountNumber: string = `select count(*) as count from household where confirmation_number=?;`;
    private static readonly AddActivity: string = `INSERT INTO rtop_activity(date, agent_id, household_id, type, note, status) VALUES (?,?,?,?,?,?)`;
    private static readonly UpdateStatus: string = `UPDATE household SET status = ? , last_updated = ? WHERE id = ?`;
    public static readonly CountSelect: string = `count(*) as count`;

    // Note on last_updated: If act.date (latest activity for record) is not defined
    // we fall back to the created date of the record. We convert it to a TIMESTAMP
    // so we can compare the two for sorting purposes. 
    public static readonly FormSelect: string = `
        f.id AS ID,
        f.card_status AS STATUS,
        f.enrollment_status,
        a.name AS OWNER,
        f.assigned_to AS OWNERID,
        f.last_updated,
        f.arrival_date,
        f.card_status_time as UPDATED_TIME,
        SYSTOOLS.BSON2JSON(f.form_record) AS FORM_RECORD
    `;

    // Isolation plan queries
    private static readonly AddReport: string = `select id from final table(INSERT INTO report(date, passed, failed, passedThenFailed, failedThenPassed) values (?,?,?,?,?))`;
    private static readonly GetReport: string = 'select * from report where id=?';
    private static readonly GetReportByDate: string = 'SELECT * FROM report WHERE date=?;';

    // Unwilling Traveller queries
    private static readonly AddUnwillingTraveller: string = `select id from final table(insert into unwilling_traveller(agent_id, num_travellers, note) values (?,?,?));`;
    private static readonly GetUnwillingTraveller: string = `select * from unwilling_traveller where id=?`;

    // Service Alberta queries
    private static readonly ListActiveAgents = `
        select distinct a.name from rtop_activity act
        join agent a
            on a.id=act.agent_id
    `;
    private static readonly GetAgent = `select * from agent where id=?`;
    private static readonly CreateAgent = `select id from final table(insert into agent(id, name) values(?, ?))`;
    private static readonly AssignAgent = `update household set assigned_to=?, prev_user=? where id=?`;
    private static readonly GetActivity = `
        select act.id, a.name as agentname, act.date, act.type, act.note, act.status from rtop_activity act
        join agent a
          on a.id=act.agent_id
        where act.household_id=?
        ORDER BY 
        act.date DESC
    `;
    private static readonly GetFormByNumber = `
        select
            f.id as id,
            f.determination_date,
            a.id AS agentId,
            a.name AS agentName,
            f.enrollment_status,
            f.card_status,
            f.contact_method,
            f.contact_email,
            f.contact_phone_number,
            SYSTOOLS.BSON2JSON(form_record) as form_record
        FROM household f
        LEFT JOIN agent a
            ON f.assigned_to=a.id
        WHERE LOWER(f.confirmation_number)=?
    `;
    private static readonly AssgnedTo = `select agent.id, agent.name from household inner join agent on household.assigned_to=agent.id where household.id=?`;

    // Report Queries

    private static readonly ServiceAlbertaReport = `
    SELECT status, COUNT(*) as count
    FROM household
    WHERE ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY status;
    `;

    private static readonly CheckpointAllTravellers = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from household
    WHERE ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly CheckpointExempt= `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from household
    WHERE exempt = TRUE
    AND ? <= arrival_date
    AND ? >= arrival_date
    AND JSON_VAL(form_record,'determination.determination', 's:32672' ) is not null
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;
    private static readonly CheckpointRequiringAccommodation = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, 
    COALESCE(SUM(1 + COALESCE(SYSTOOLS.JSON_LEN(form_record, 'additionalTravellers'), 0)),0) as COUNT from household
    WHERE JSON_VAL(form_record,'determination.determination', 's:32672' ) = 'Support'
    AND JSON_VAL(form_record,'hasPlaceToStayForQuarantine','s:32672') = FALSE
    AND ? <= arrival_date
    AND ? >= arrival_date
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly CheckpointRevision = `
    select JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150') as ENTRY, COUNT(*) as COUNT from household
    WHERE JSON_VAL(form_record,'determination.determination', 's:32672' ) = 'Support'
    AND ? <= arrival_date
    AND ? >= arrival_date
    GROUP BY JSON_VAL(form_record, 'nameOfAirportOrBorderCrossing', 's:150')
    `;

    private static readonly callCount = `
       SELECT 
            agent.id, agent.name, count(*) call_count
        FROM
            rtop_activity
            INNER JOIN agent
                ON agent.id = rtop_activity.agent_id
            WHERE ? <= rtop_activity.date
            AND ? >= rtop_activity.date
        GROUP BY 
            agent.id, agent.name
        ORDER BY 
            agent.name
    `;

    private static readonly totalCallCount = `
    SELECT 
        'Total' as name, count(*) call_count
     FROM
         rtop_activity
         WHERE ? <= rtop_activity.date
         AND ? >= rtop_activity.date
 `;
    
    private static readonly selectReportAccess = `
    SELECT ar.REPORT_NAME,ara.AGENT_EMAIL FROM AGENT_REPORT_ACCESSCONTROL ara 
        INNER JOIN AVAILABLE_REPORT ar ON ARA.REPORT_TYPE = ar.ID 
        `;


    private static readonly agentActiveReportAccess = `
    ${RTOPDbService.selectReportAccess} WHERE AGENT_EMAIL = ? AND ENABLED = ?;`

    private static readonly agentFullReportAccess = `
    ${RTOPDbService.selectReportAccess} WHERE AGENT_EMAIL = ? AND ar.REPORT_NAME in (?);`

    private static readonly createReportAccess = `
    INSERT INTO AGENT_REPORT_ACCESSCONTROL 
        (AGENT_EMAIL, REPORT_TYPE, ENABLED,LAST_UPDATED) 
        SELECT ?,ID,TRUE,CURRENT_TIMESTAMP FROM AVAILABLE_REPORT WHERE REPORT_NAME = ?;
    `;

    private static readonly fetchAccessControlList = `
    ${RTOPDbService.selectReportAccess} WHERE ENABLED = TRUE ORDER BY AGENT_EMAIL;
    `;

    private static readonly updateReportAccess = `
    UPDATE AGENT_REPORT_ACCESSCONTROL 
        SET ENABLED = ?, LAST_UPDATED = CURRENT_TIMESTAMP 
        WHERE AGENT_EMAIL = ?
    `;

    private static readonly enableReportAccess = `
    ${RTOPDbService.updateReportAccess} AND REPORT_TYPE = 
        (SELECT ID FROM AVAILABLE_REPORT WHERE REPORT_NAME = ?) AND ENABLED = FALSE;
    `;


    private static readonly revokeFullReportAccess = `
    ${RTOPDbService.updateReportAccess} AND ENABLED = TRUE;
    `;

    private static readonly VerifyByBirthDate = `
        SELECT t.id
        FROM rtop_traveller t
        JOIN household h ON t.household_id=h.id
        JOIN daily_reminder rem ON rem.household_id=h.id AND rem.token=?
        WHERE t.confirmation_number=? AND t.birth_date=?
    `;

    private static readonly FindTravellersByToken = `
        SELECT
            rt.FIRST_NAME,
            rt.LAST_NAME,
            rt.CONFIRMATION_NUMBER,
            rt.ID,
            dr.DATE,
            ds.STATUS,
            h.ARRIVAL_DATE,
            rt.dob_attempt,
            rt.dob_attempt_time,
        CASE WHEN ds.id IS NULL THEN 'not_submitted' ELSE 'submitted' END AS submission_status
        FROM RTOP_TRAVELLER rt 
        INNER JOIN HOUSEHOLD h
        ON rt.household_id = h.ID
        INNER JOIN DAILY_REMINDER dr
        ON dr.household_id = h.ID
        LEFT JOIN daily_submission ds
        ON ds.REMINDER_ID = dr.id AND ds.TRAVELLER_ID  = rt.ID
        WHERE dr.TOKEN=?
    `;

    private static readonly AddDailySubmission = `
        SELECT ID
        FROM FINAL TABLE(
            INSERT INTO DAILY_SUBMISSION(STATUS, SUBMISSION, TRAVELLER_ID, REMINDER_ID)
            VALUES (?, SYSTOOLS.JSON2BSON(?), ?, ?)
        )
    `;

    private static readonly GetReminderId = `
        SELECT dr.ID, rt.ID as TRAVELLER_ID, dr.date
        FROM DAILY_REMINDER dr 
        INNER JOIN HOUSEHOLD h ON h.ID = dr.HOUSEHOLD_ID 
        INNER JOIN RTOP_TRAVELLER rt ON h.ID = rt.HOUSEHOLD_ID 
        WHERE rt.CONFIRMATION_NUMBER = ? AND dr.TOKEN=?
    `;

    private static readonly ListNotCreatedReminderHouseholds = `
        select h.ID from household as h
        left join daily_reminder as rem on rem.household_id=h.id
            and rem.date=?
        where rem.household_id is null and h.enrollment_status='${EnrollmentStatus.ENROLLED}'
    `;

    private static readonly RetrieveNotCreatedReminderHousehold = `
        select h.ID from household as h
        left join daily_reminder as rem on rem.household_id=h.id
            and rem.date=?
        where rem.household_id is null
            and h.enrollment_status='${EnrollmentStatus.ENROLLED}'
            and h.id=?
    `;

    private static readonly CreateDailyReminders = `
        insert into daily_reminder(date, household_id, status, token, sms_status, email_status)
        values
    `;

    private static readonly ListUnsentDailyReminders = `
        select rem.*, h.contact_email as email, h.email as form_email, h.contact_phone_number as phone_number, h.contact_method from daily_reminder as rem 
        inner join household h on h.id=rem.household_id and h.enrollment_status='${EnrollmentStatus.ENROLLED}'
        where rem.date=? and rem.sms_status!='sent' and rem.email_status!='sent';
    `;

    private static readonly ListCallInDailyReminders = `
        select rem.*, h.contact_method from daily_reminder as rem 
        inner join household h on h.id=rem.household_id and h.enrollment_status='${EnrollmentStatus.ENROLLED}'
        where rem.date=? and rem.contact_method='callIn';
    `;

    private static readonly RetrieveTodaysReminder = `
        select rem.*, h.contact_email as email, h.contact_phone_number as phone_number, h.contact_method from daily_reminder as rem 
        inner join household h on h.id=rem.household_id and h.enrollment_status='${EnrollmentStatus.ENROLLED}'
        where rem.date=? and h.id=?;
    `;

    private static readonly UpdateEmailStatus = `
        update daily_reminder
        set email_status=?
        where id=?
    `;

    private static readonly UpdateSmsStatus = `
        update daily_reminder
        set sms_status=?
        where id=?
    `;
    
    private static readonly CreateTravellerRecords = `
        insert into rtop_traveller(first_name, last_name, birth_date, confirmation_number, household_id)
            values
    `;

    private static readonly EnrolHouseholdFortraveller = `
        UPDATE household
            SET enrollment_status='${EnrollmentStatus.ENROLLED}',
            determination_date=CURRENT TIMESTAMP

        WHERE id=(
            SELECT h.id FROM household AS h
            INNER JOIN rtop_traveller t ON t.household_ID=h.id AND LOWER(t.confirmation_number)=?
            WHERE h.enrollment_status='${EnrollmentStatus.APPLIED}'
        )
    `;

    private static readonly GetTravellerSubmissionStatus = `
        SELECT
            rt.ID as TRAVELLER_ID,
            rt.FIRST_NAME,
            rt.LAST_NAME,
            rt.CONFIRMATION_NUMBER,
            rt.BIRTH_DATE, 
            rt.CARD_STATUS,
            ds.STATUS,
            dr.DATE,
            dr.TOKEN,
            rt.CARD_STATUS_TIME,
            SYSTOOLS.BSON2JSON(ds.SUBMISSION) as SUBMISSION,
            rt.ISOLATION_STATUS
        FROM HOUSEHOLD h
        INNER JOIN RTOP_TRAVELLER rt ON 
            h.ID = rt.HOUSEHOLD_ID
        LEFT JOIN DAILY_SUBMISSION ds 
            ON rt.ID = ds.TRAVELLER_ID
        LEFT JOIN DAILY_REMINDER dr 
            ON ds.REMINDER_ID = dr.ID
        WHERE h.CONFIRMATION_NUMBER = ?;
    `;

    private static readonly UpdateEnrollmentStatus = `
    UPDATE HOUSEHOLD SET ENROLLMENT_STATUS = ? , 
        LAST_UPDATED = CURRENT_TIMESTAMP 
        WHERE ID = ?;
    `;

    private static readonly UpdateEnrollmentStatusWithDate = `
    UPDATE HOUSEHOLD SET ENROLLMENT_STATUS = ? , 
        LAST_UPDATED = CURRENT_TIMESTAMP, DETERMINATION_DATE = CURRENT_TIMESTAMP
        WHERE ID = ?;
    `;
    // Contact method verification
    private static readonly CreateContactMethodVerification = `
    insert into contact_method_verification(token, contact_method, email, phone_number, verification_code, verification_id)
        values(?, ?, ?, ?, ?, ?);
    `;

    private static readonly UpdateVerificationStatus = `
    update contact_method_verification
        SET status=?,
            household_id=?,
            verified_at=CASE WHEN ?='${ContactVerificationStatus.VERIFIED}' THEN CURRENT TIMESTAMP ELSE NULL END
        WHERE
            id=?;
    `;

    private static readonly UpdateContactInfo = `
    update household
        SET contact_email=?,
            contact_phone_number=?,
            contact_method=?
        WHERE
            id=?;
    `;

    private static readonly FindVerificationToken: string = `
        select
            v.id,
            v.created,
            v.status,
            v.email,
            v.phone_number,
            v.contact_method,
            v.household_id
        from contact_method_verification v
        where v.token=?
    `;

    private static readonly FindVerificationTokenByCode: string = `
        select
            v.id,
            v.created,
            v.status,
            v.email,
            v.phone_number,
            v.contact_method,
            v.household_id,
            v.token
        from contact_method_verification v
        where v.verification_id=?
            and v.verification_code=?
    `;


    private static readonly UpdateExpiredHouseholdStatus = `
        UPDATE HOUSEHOLD h
        SET h.CARD_STATUS = '${DAILY_STATUS.YELLOW}', h.CARD_STATUS_TIME = CURRENT TIMESTAMP, h.CARD_STATUS_REASON = '${CARD_STATUS_REASON.DAILY_TRACKING_FLAG}'
        WHERE h.ID IN (
            SELECT h.ID
            FROM DAILY_REMINDER dr 
            INNER JOIN HOUSEHOLD h ON h.ID = dr.HOUSEHOLD_ID
            WHERE h.CARD_STATUS != '${DAILY_STATUS.RED}'
                AND  dr."DATE" BETWEEN CURRENT TIMESTAMP - 1 DAY AND CURRENT TIMESTAMP
                AND dr.ID NOT IN (
                    SELECT dr.ID 
                    FROM DAILY_REMINDER dr  
                    INNER JOIN DAILY_SUBMISSION ds ON ds.REMINDER_ID = dr.ID
                )
            )
        `;
    /* private static readonly UpdateWeeklyExpiredHouseholdStatus = `
        UPDATE HOUSEHOLD h
        SET h.CARD_STATUS = '${DAILY_STATUS.YELLOW}', h.CARD_STATUS_TIME = CURRENT TIMESTAMP, h.CARD_STATUS_REASON = '${CARD_STATUS_REASON.NOT_TESTED}'
        WHERE h.ID IN (
            SELECT h.ID
            FROM DAILY_REMINDER dr 
            INNER JOIN HOUSEHOLD h ON h.ID = dr.HOUSEHOLD_ID
            WHERE h.CARD_STATUS != '${DAILY_STATUS.RED}'
            AND  dr."DATE" BETWEEN CURRENT TIMESTAMP - 9 DAY AND CURRENT TIMESTAMP
            )
        `; */
    private static readonly UpdateCompletedStatus = `
        UPDATE HOUSEHOLD h
        SET h.STATUS = 'completed'
        WHERE h.STATUS = '${EnrollmentStatus.ENROLLED}'
            AND h.CARD_STATUS = '${DAILY_STATUS.GREEN}'
            AND  h.DETERMINATION_DATE + 14 DAY >= CURRENT TIMESTAMP
        `;

    private static readonly GetStatusReason = `
        SELECT h.CARD_STATUS_REASON
        FROM HOUSEHOLD h 
        WHERE h.CONFIRMATION_NUMBER = ?
        `;

    private static readonly GetLatestDailyReminderForHousehold = `
    SELECT * FROM DAILY_REMINDER dr WHERE HOUSEHOLD_ID = ? ORDER BY "DATE" DESC FETCH FIRST 1 ROWS ONLY;
    `;

    private static readonly UpdateTravellerTestDate = `
    update rtop_traveller set test2_date=?
    where test2_date IS NULL and id=?
    `;

    private static readonly OverrideCardStatus = `
        UPDATE HOUSEHOLD h
        SET h.CARD_STATUS = ?, h.CARD_STATUS_TIME = CURRENT TIMESTAMP, h.CARD_STATUS_REASON = ? 
        WHERE h.CONFIRMATION_NUMBER = ?
    `;

    private static readonly GetExpiredTravellers = `
    SELECT rt.ID AS TRAVELLER_ID, rt.CONFIRMATION_NUMBER 
        FROM DAILY_REMINDER dr INNER JOIN HOUSEHOLD h2 
        ON h2.ID = dr.HOUSEHOLD_ID INNER JOIN RTOP_TRAVELLER rt 
        ON rt.HOUSEHOLD_ID = h2.ID WHERE h2.ENROLLMENT_STATUS = ? AND 
        dr."DATE" = ? AND rt.ID NOT IN (
            SELECT rt2.ID FROM DAILY_REMINDER dr 
            LEFT JOIN DAILY_SUBMISSION ds ON 
            ds.REMINDER_ID = dr.ID INNER JOIN 
            RTOP_TRAVELLER rt2 ON rt2.ID = ds.TRAVELLER_ID 
            WHERE dr."DATE" = ?);
    `;

    private static getUpdateCardStatusQuery = (confirmationNumbers) =>  `
        UPDATE HOUSEHOLD h
        SET h.CARD_STATUS = ?, h.CARD_STATUS_TIME = CURRENT TIMESTAMP, h.CARD_STATUS_REASON = ? 
        WHERE h.ID IN (
            SELECT h.ID FROM HOUSEHOLD h INNER JOIN RTOP_TRAVELLER rt 
            ON rt.HOUSEHOLD_ID = h.ID WHERE rt.CONFIRMATION_NUMBER IN (${confirmationNumbers}) ) 
            AND (h.CARD_STATUS IS NULL OR h.CARD_STATUS != '${DAILY_STATUS.RED}')
        `;

    private static getUpdateTravellerCardStatusQuery = (travellerIds) => `
        UPDATE RTOP_TRAVELLER 
            SET CARD_STATUS = ?, CARD_STATUS_TIME = CURRENT_TIMESTAMP 
            WHERE ID IN (${travellerIds})
    `;

    private static readonly UpdateDOBVerificationAttempts = `
    UPDATE RTOP_TRAVELLER 
        SET DOB_ATTEMPT = DOB_ATTEMPT + 1, 
        DOB_ATTEMPT_TIME = CURRENT_TIMESTAMP 
        WHERE CONFIRMATION_NUMBER = ?;
    `;

    private static readonly ResetDOBVerificationAttempts = `
    UPDATE RTOP_TRAVELLER 
        SET DOB_ATTEMPT = 0
        WHERE CONFIRMATION_NUMBER = ?;
    `;

    private static readonly UpdateTravellerIsolationStatus = `
    UPDATE RTOP_TRAVELLER 
        SET ISOLATION_STATUS = ?
        WHERE CONFIRMATION_NUMBER = ?;
    `;

    /**
     * Returns the number of rows in a table
     *
     * @param name - table name
     */
    public static async getCount(name: string): Promise<any> {
        return (await enrollmentFormQuery(`select count(*) as count from ${name};`))
            .count();

    }

    // Arrival form
     /**
     * Inserts a new form into a database
     *
     * @param form - from details
     */
    public static async saveForm(form: Household) {
        const formAsJsonString = JSON.stringify(form);

        return await enrollmentFormQuery(
            RTOPDbService.AddForm,
            [
                formAsJsonString,
                form.lastName,
                form.firstName,
                form.confirmationNumber,
                moment(form.arrivalDate).format('YYYY-MM-DD HH:mm:ss'),
                form.contactPhoneNumber,
                form.contactEmail,
                form.phoneNumber,
                form.email,
                form.enrollmentStatus,
                form.contactMethod
            ]);
    }
    /**
     * Selects a specific form by id
     *
     * @param id - from id
     */
    public static async getForm(id: number) {
        return await enrollmentFormQuery(RTOPDbService.FindById, [id]);
    }
    /**
     * Selects a specific form by confirmation number
     *
     * @param confirmationNumber - confirmation number
     */
    public static async getFormByNumber(confirmationNumber: string) {
        return await enrollmentFormQuery(RTOPDbService.FindByNumber, [confirmationNumber.toLowerCase()]);
    }

    /**
     * Selects a specific verification token
     *
     * @param token - the token of the contact method verification
     */
    public static async getVerificationToken(token: string) {
        return await enrollmentFormQuery(RTOPDbService.FindVerificationToken, [token]);
    }

    /**
     * Selects a specific verification token by phoneNumber and sms verification code
     *
     * @param token - the token of the contact method verification
     */
    public static async getVerificationTokenByCode(verificationId: string, code: string) {
        return await enrollmentFormQuery(RTOPDbService.FindVerificationTokenByCode, [verificationId, code]);
    }

    /**
     * Get the household the traveller with the given confirmation is part of
     * 
     * @param confirmationNumber - confirmation number
     */
    public static async getFormByTravellerConfirmationNumber(confirmationNumber: string) {
        return await enrollmentFormQuery(RTOPDbService.FindByTravellerConfirmationNumber, [confirmationNumber.toLowerCase()]);
    }

    /**
     * Selects a specific report by last name partial matching
     *
     * @param lname - last name of a traveller who submitted a form
     */
    public static async getFormByLastName(lname: string) {
        return await enrollmentFormQuery(RTOPDbService.FindByLastName, [lname.toLowerCase()]);
    }

    /**
     * Selects a specific report by last name exact matching
     *
     * @param lname - last name of a traveller who submitted a form
     */
    public static async getFormByLastNameExact(lname: string) {
        return await enrollmentFormQuery(RTOPDbService.FindByLastNameExact, [lname.toLowerCase()]);
    }
    
    /**
     * Submits determination for a form
     *
     * @param arrivalFormId - form id
     * @param updateField - JSON string of determination
     */
    public static async submitDetermination(updateField: any, exemptStatus:boolean ,arrivalFormId: number) {
        return await enrollmentFormQuery(RTOPDbService.SubmitDetermination, [updateField,exemptStatus,arrivalFormId]);
    }

    /**
     * Updates the forms fields of the given form to the given values 
     *
     * @param agentID - ID of the agent making the change
     * @param arrivalFormId - ID of the form to update
     * @param updateField - JSON string of fields to update
     */
    public static async editForm(agentID: string, arrivalFormId: number, updatedFields: string) {
        const res = await enrollmentFormQuery(RTOPDbService.EditForm, [updatedFields, arrivalFormId]);
        return res;
    }

    /**
     * Returns the number of submitted forms by confirmation number
     *
     * @param num - confirmation number
     */
    public static async countByNum(num: string) {
        return (await enrollmentFormQuery(RTOPDbService.CountNumber, [num])).count();
    }

    /**
     * Creates a new activity
     *
     * @param activity - activity data
     * @param userId - agent id
     * @param formId - id of a form
     */
    public static async addActivity(activity:any, userId:string, formId:number): Promise<any> {
        const args = [
            moment().utc().format('YYYY-MM-DD HH:mm:ss'),
            userId,
            formId,
            "Status Changed",
            activity.note,
            activity.status];

        return (await enrollmentFormQuery(RTOPDbService.AddActivity, args));
    }
    /**
     * Updates activity status
     *
     * @param status - new status of a form
     * @param id - form id
     */
    public static async updateStatus(status:string, id:number): Promise<any> {
        return (await query(RTOPDbService.UpdateStatus, [status, moment().utc().format('YYYY-MM-DD HH:mm:ss') ,id]));
    }


    // isolation plan
    /**
     * Updates report
     *
     * @param id - report id
     * @param property - property to be updated
     */
    public static async updateReport(id: number, property: IsolationPlanReportProperty) {
        return await query(`
              UPDATE report SET 
              ${property}=${property}+1
              WHERE id=?;
          `, [id]);
    }

    /**
     * Inserts a new report into a database
     *
     * @param plan - report properties
     */
    public static async saveReport(plan: IsolationPlanReport): Promise<any> {
        return await query(RTOPDbService.AddReport,
            [plan.date, plan.passed, plan.failed, plan.passedThenFailed, plan.failedThenPassed]);
    }
    /**
     * Selects a specific report by id
     *
     * @param id - report id
     */
    public static async getReportById(id: number): Promise<any> {
        return await query(RTOPDbService.GetReport, [id]);
    }
    /**
     * Selects a specific report by data
     *
     * @param date - report date
     */
    public static async getReportByDate(date: string): Promise<any> {
        return await query(RTOPDbService.GetReportByDate, [date]);
    }



    // Service Alberta
    /**
     * Returns the information about an agent
     *
     * @param user
     */
    public static async getAgent(user: any): Promise<any> {
        return await query(RTOPDbService.GetAgent, [user.sub]);
    }
    /**
     * Creates a new agent
     *
     * @param user
     */
    public static async createAgent(user: any): Promise<any> {
        return await query(RTOPDbService.CreateAgent, [user.sub, user.name]);
    }

    /**
     * Lists agents that have submitted at least one form activity
     */
    public static async listActiveAgents(): Promise<AgentDTO[]> {
        return (await query(RTOPDbService.ListActiveAgents, []))
            .map(r => ({id: r.ID, name: r.NAME}));
    }

    /**
     * Returns limited form data for Service Alberta
     *
     * @param num - confirmation number
     */
    public static async getMonitoringFormByNumber(num: string): Promise<any> {
        return await enrollmentFormQuery(RTOPDbService.GetFormByNumber, [num.toLowerCase()]);
    }
    /**
     * Returns notes for specific traveller
     *
     * @param formId - id of a submitted form
     */
    public static async getActivity(formId: number): Promise<any> {
        return await query(RTOPDbService.GetActivity, [formId]);
    }
    /**
     * Assigns a new agent to a form
     *
     * @param agent - agent id (null - for unassign)
     * @param prevAgent - id of a person who was assigned to this form before (null - if none)
     * @param form - form id
     */
    public static async assignTo(agent: any, prevAgent: any, form: number): Promise<any> {
        return await query(RTOPDbService.AssignAgent, [agent, prevAgent, form]);
    }
    /**
     * Returs information about the agent who is assigned to the form
     *
     * @param form - id of a submitted form
     */
    public static async assignedTo(form: number): Promise<any> {
        return await query(RTOPDbService.AssgnedTo, [form]);
    }

    /**
     * Creates a new unwilling traveller record
     */
    public static async recordUnwillingTraveller(agentId: string, numTravellers: number, note: string): Promise<UnwillingTravellerRO> {
        const res = await query(RTOPDbService.AddUnwillingTraveller, [agentId, numTravellers, note || '']);
        const [{ID}] = res || {ID: null};
        return new UnwillingTravellerRO(ID);
    }

    /**
     * Retrieves an unwilling traveller record
     */
    public static async getUnwillingTraveller(id: number): Promise<UnwillingTravellerRO> {
        const res = await query(RTOPDbService.GetUnwillingTraveller, [`${id}`]);

        const [{ID, CREATED, AGENT_ID, NOTE, NUM_TRAVELLERS}] = res || {};

        const record = new UnwillingTravellerRO(ID);

        record.created = CREATED;
        record.agentId = AGENT_ID;
        record.note = NOTE;
        record.numTravellers = NUM_TRAVELLERS;

        return record;
    }

    public static async generateCheckpointReport(startDate:string,endDate:string) : Promise<CheckPointResultRO[]> {

        return [
            new CheckPointResultRO('# of travellers',(await query(this.CheckpointAllTravellers,[startDate,endDate]))),
            new CheckPointResultRO('# of exempt travellers',(await query(this.CheckpointExempt,[startDate,endDate]))),
            new CheckPointResultRO('# of isolation plans requiring revisions',(await query(this.CheckpointRevision,[startDate,endDate]))),
            new CheckPointResultRO('# of travellers requiring accommodation/ transportation',(await query(this.CheckpointRequiringAccommodation,[startDate,endDate])))
        ]
    }
    public static async generateServiceAlbertaReport(startDate:string,endDate:string){
        return await query(this.ServiceAlbertaReport,[startDate,endDate]);  
    }
    public static async generateCallCountReport(startDate:string,endDate:string){
        const agentReport = await query(this.callCount,[startDate,endDate]);
        const overallReport = await query(this.totalCallCount,[startDate,endDate]);

        return [].concat(overallReport).concat(agentReport).map(({NAME, CALL_COUNT}) =>({
            'Agent': NAME,
            'Calls Made': CALL_COUNT
        }));
    }

    public static async getActiveReportAccessForAgent(agentEmail: string): Promise<any> {
        return await query(RTOPDbService.agentActiveReportAccess, [agentEmail.toLowerCase(), true ]);
    }

    public static async getReportAccessStatusForAgent(agentEmail: string,reportName: string): Promise<any> {
        return await query(RTOPDbService.agentFullReportAccess, [agentEmail.toLowerCase(), reportName ]);
    }

    public static async createReportAccessForAgent(agentEmail: string, reportName: string): Promise<any>{
        return await query(RTOPDbService.createReportAccess, [agentEmail.toLowerCase(), reportName]);
    }

    public static async enableReportAccessForAgent(agentEmail: string, reportName: string): Promise<any>{
        return await query(RTOPDbService.enableReportAccess, [true, agentEmail.toLowerCase(), reportName]);
    }

    public static async revokeFullReportAccessForAgent(agentEmail: string): Promise<any>{
        return await query(RTOPDbService.revokeFullReportAccess, [false, agentEmail.toLowerCase()]);
    }

    public static async getReportAccessControlList(): Promise<any>{
        return await query(RTOPDbService.fetchAccessControlList);
    }

    /**
     * Validates traveller by bithdate
     *
     * @param id - travellers id
     */
    public static async verifyByBirthDate(confirmationNumber: string, reminderToken: string, birthDate: string): Promise<any> {
        return await enrollmentFormQuery(RTOPDbService.VerifyByBirthDate, [reminderToken, confirmationNumber, birthDate]);
    }
    
    /**
     * Returns the list of travllers
     *
     * @param token - daily reminder id
     */
    public static async getFormByToken(token: string): Promise<any> {
        return await enrollmentFormQuery(RTOPDbService.FindTravellersByToken, [token]);
    }

    /**
     * Lists households that have not received a reminder for the given date
     * @param date - day to create reminders for
     */
    public static async listNotCreatedReminderHouseholds(date: string, householdId?: number) {
        if(householdId){
            return query(RTOPDbService.RetrieveNotCreatedReminderHousehold, [date, householdId]);
        } else {
            return query(RTOPDbService.ListNotCreatedReminderHouseholds, [date]);
        }
        
    }

    /**
     * Get todays reminder for a household
     */
    public static async getTodaysReminder(householdId: number, date: string) {
        return query(RTOPDbService.RetrieveTodaysReminder, [date, householdId]);        
    }
    
    /**
     * Creates daily reminders for the given households
     */
    public static async createDailyReminders(households) {
        const placeholders = households.map(() => '(?, ?, ?, ?, ?, ?)').join(',');
        const qs = `${RTOPDbService.CreateDailyReminders} ${placeholders}`;

        return query(qs, households.flat());
    }

    /**
     * Lists reminders that have not yet been sent for the given day
     * @param date - the date to list unsent reminders for
     */
    public static async listUnsentDailyReminders(date: string) {
        return query(RTOPDbService.ListUnsentDailyReminders, [date]);
    }
    
    /**
     * Lists call in reminders for the given day
     * @param date - the date to list unsent reminders for
     */
    public static async listCallInDailyReminders(date: string) {
        return query(RTOPDbService.ListCallInDailyReminders, [date]);
    }

    /**
     * Updates the email status of the given reminder
     * @param id the id of the reminder to update
     * @param status the new status of the reminder
     */
    public static async updateReminderEmailStatus(id: string, status: NOTIFICATION_STATUS) {
        return query(RTOPDbService.UpdateEmailStatus, [status, id]);
    }
    
    /**
     * Updates the sms status of the given reminder
     * @param id the id of the reminder to update
     * @param status the new sms status of the reminder
     */
    public static async updateReminderSMSstatus(id: string, status: NOTIFICATION_STATUS) {
        return query(RTOPDbService.UpdateSmsStatus, [status, id]);
    }

    /**
     * Create rtop_traveller DB records for use by daily reminders
     * @param records records to create
     */
    public static async createTravellerRecords(household: Household, records: RTOPTraveller[]) {
        const rows: any = records.map(r => [r.firstName, r.lastName, moment(r.dateOfBirth).format(DATE_FORMAT), r.confirmationNumber, household.id]);
        const placeholders = rows.map(() => '(?, ?, ?, ?, ?)').join(',');
        const qs = `${RTOPDbService.CreateTravellerRecords} ${placeholders}`;

        return await query(qs, rows.flat());
    }

    /**
     * Submit daily questionnaire for traveller
     *
     * @param submission - submitted answers
     * @param travellerId - confirmation number
     * @param reminderId - reminder id
     */
    public static async createDailySubmission(status: DAILY_STATUS, jsonSubmission: string, travellerId: number, reminderId: number): Promise<any> {
        return await enrollmentFormQuery(RTOPDbService.AddDailySubmission, [status, jsonSubmission, travellerId, reminderId]);
    }

    /**
     * Returns reminder id
     *
     * @param travellerId - confirmation number
     */
    public static async getReminderId(confirmationNumber: string, token: string): Promise<any> {
        return await enrollmentFormQuery(RTOPDbService.GetReminderId, [confirmationNumber, token]);
    }

    /**
     * Enrol the household the given traveller is part of
     * @param confirmationNumber the traveller confirmation number to enrol the household for
     */
    static enrolHouseholdForTraveller(confirmationNumber: string) {
        return query(RTOPDbService.EnrolHouseholdFortraveller, [confirmationNumber.toLowerCase()]);
    }

    public static async getTravellerStatusForHousehold(confirmationNumber: string){
        return new HouseholdTravellersQueryStatusRO(await query(RTOPDbService.GetTravellerSubmissionStatus, [confirmationNumber]));
    }

    public static async updateEnrollmentStatus(enrollmentStatus: EnrollmentStatus ,id: number) {
        return query(RTOPDbService.UpdateEnrollmentStatus, [enrollmentStatus, id]);
    }

    public static async updateEnrollmentStatusWithDate(enrollmentStatus: EnrollmentStatus ,id: number) {
        return query(RTOPDbService.UpdateEnrollmentStatusWithDate, [enrollmentStatus, id]);
    }

    /**
     * Fetches the latest reminder sent for the household
     * @param date - the date to list unsent reminders for
     */
    public static async getLatestDailyReminders(id: number) {
        return query(RTOPDbService.GetLatestDailyReminderForHousehold, [id]);
    }

    /**
     * Returns the reason for status change
     * @param confirmationNumber - travellers confirmation number
     */
    public static async getStatusReason(confirmationNumber: string): Promise<any> {
        return query(RTOPDbService.GetStatusReason, [confirmationNumber]);
    }

    /**
     * Changes household status
     * @param confirmationNumber - travellers confirmation number
     * @param dailyStatus - household status
     */
    public static async updateHouseholdCardStatus(confirmationNumber: string[], dailyStatus: string, reason: string): Promise<any> {
        const parametersLength = '?' + (confirmationNumber.length > 1 ? ',?'.repeat(confirmationNumber.length - 1): '');
        return query(RTOPDbService.getUpdateCardStatusQuery(parametersLength), [dailyStatus, reason, ...confirmationNumber]);
    }
    
    /**
     * Create contact method verification entry
     * @param token Token that for a time period can be used to look up record
     * @param contactMethod sms | email
     * @param householdId id of household this belongs to
     */
    static createContactMethodVerification(token: string, contactMethod: CONTACT_METHOD, email: string, phoneNumber: string, verificationCode: string, verificationId: string) {
        return query(RTOPDbService.CreateContactMethodVerification, [token, contactMethod, email, phoneNumber, verificationCode, verificationId]);
    }

    /**
     * Updates the status of the given verification and assigns the given household to the record
     */
    static updateVerificationStatus(verification: ContactMethodVerification, householdId: number, status: ContactVerificationStatus) {
        return query(RTOPDbService.UpdateVerificationStatus, [status, householdId, status, verification.id]);
    }
  
    /**
     * Find taveller by confirmation number
     */
    static findTravellerByConfirmationNumber(confirmationNumber: string) {
        return enrollmentFormQuery(RTOPDbService.FindTravellerByConfirmationNumber, [confirmationNumber]);
    }
  
    /**
     * Update tested_date for traveller if not already set
     */
    static updateTravellerTestedDate(travellerId: number, date: string) {
        return query(RTOPDbService.UpdateTravellerTestDate, [date, `${travellerId}`]);
    }
  

    /**
     * Update contact info of household
     */
    static updateContactInfo(householdId: number, contactInfo: ContactMethodVerificationDTO) {
        return query(RTOPDbService.UpdateContactInfo, [contactInfo.email || '', contactInfo.phoneNumber || '', contactInfo.contactMethod, householdId]);
    }

    /**
     * overrides household status
     * @param confirmationNumber - travellers confirmation number
     * @param dailyStatus - household status
     */
    public static async overrideHouseholdCardStatus(confirmationNumber: string, dailyStatus: string, reason: string): Promise<any> {
        return query(RTOPDbService.OverrideCardStatus, [dailyStatus, reason, confirmationNumber]);
    }

    /**
     * Changes traveller card status
     * @param travellerId - travellerId
     * @param dailyStatus - traveller status
     */
    public static async updateTravellerCardStatus(travellerId: number[], dailyStatus: string): Promise<any> {
        const parametersLength = '?' + (travellerId.length > 1 ? ',?'.repeat(travellerId.length - 1): '');
        return query(RTOPDbService.getUpdateTravellerCardStatusQuery(parametersLength) , [dailyStatus, ...travellerId ]);
    }

    /**
     * Fetches all the travellers for household whose enrollment status is enrolled.
     * Travellers are checked for submission only for the previous day
     * 
     * Scheduler runs 11.59 PM MT daily - which is the next day in UTC
     * 
     * @param enrollmentStatus - only 'enrolled' household status are checked for submissions
     * @param yesterdayDate - 
     */
    public static async getExpiredTravellerStatus(enrollmentStatus: string, yesterdayDate: string): Promise<any>  {
        return query(RTOPDbService.GetExpiredTravellers, [enrollmentStatus, yesterdayDate, yesterdayDate]);
    }

    public static async updateCompletedHouseholdStatus(): Promise<void>  {
        return query(RTOPDbService.UpdateCompletedStatus);
    }

    /**
     * 
     * Inserts data into householdstatushistory table to keep a track of the possible status updates to the household
     * 
     */
    public static async createHouseholdStatusHistory(confirmationNumber: string[], dailyStatus: string, reason: string, dailySubmission: number) {
        const insertQuery = `INSERT INTO HOUSEHOLD_STATUS_HISTORY (HOUSEHOLD_ID, STATUS, STATUS_REASON, DAILY_SUBMISSION_ID)`;
        const placeholders = ` values` + ([...Array(confirmationNumber.length)].map(() => ` ((SELECT h.ID FROM HOUSEHOLD h INNER JOIN RTOP_TRAVELLER rt 
            ON rt.HOUSEHOLD_ID = h.ID WHERE rt.CONFIRMATION_NUMBER = ? ),?,?,?)`).join(","));
        const rows: any = confirmationNumber.map(r => [r, dailyStatus, reason, dailySubmission]);
        const qs = `${insertQuery} ${placeholders}`;

        return await query(qs, rows.flat());
    }

    public static async updateDOBVerificationAttempts(confirmationNumber: string){
        return query(RTOPDbService.UpdateDOBVerificationAttempts, [confirmationNumber]);
    }

    public static async resetDOBVerificationAttempts(confirmationNumber: string){
        return query(RTOPDbService.ResetDOBVerificationAttempts, [confirmationNumber]);
    }

    public static async updateTravellerIsolationStatus(confirmationNumber: string, isolationStatus: boolean){
        return query(RTOPDbService.UpdateTravellerIsolationStatus, [isolationStatus, confirmationNumber]);
    }
}