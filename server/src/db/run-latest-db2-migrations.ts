import { MigrateLatest } from "./db2migrations";
import { LoggerService } from "../logs/logger";

/**
 * Utility function to run the latest DB migration to be applied
 */
(async () => {
    try {
        await MigrateLatest();
        LoggerService.info('Successfully migrated db');
    } catch(e) {
        LoggerService.error('Failed to migrate db', e);
    }
})();

