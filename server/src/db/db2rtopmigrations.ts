import { db2Connection } from "./db2connection";
import { LoggerService } from "../logs/logger";
import { EnrollmentStatus } from "../rtop/entities/household.entity";

const createDailyReminerTable = `
CREATE TABLE daily_reminder(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    date VARCHAR(12) NOT NULL,
    household_id INT NOT NULL,
    status VARCHAR(255) NOT NULL,
    sms_status VARCHAR(255) NOT NULL,
    email_status VARCHAR(255) NOT NULL,
    token VARCHAR(512) NOT NULL UNIQUE,

    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const createDailySubmissionTable = `
CREATE TABLE daily_submission(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    status VARCHAR(255) NOT NULL,
    submission BLOB,
    traveller_id INT NOT NULL,
    reminder_id INT NOT NULL,
    
    FOREIGN KEY(reminder_id)
        REFERENCES daily_reminder(id),
    FOREIGN KEY(traveller_id)
        REFERENCES rtop_traveller(id),
    CONSTRAINT submission_traveller_unique
        UNIQUE(traveller_id, reminder_id)
);
`;

const createRtopTravellerTable = `
CREATE TABLE rtop_traveller(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    first_name VARCHAR(512) NOT NULL,
    last_name VARCHAR(512) NOT NULL,
    birth_date VARCHAR(12) NOT NULL,
    household_id INT NOT NULL,

    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addRtopTravellerConfirmationNumber = `
ALTER TABLE rtop_traveller
    ADD COLUMN confirmation_number VARCHAR(64) NOT NULL default '';
`;

const createHouseholdTable = `
CREATE TABLE HOUSEHOLD (
    ID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) , 
    ENROLLMENT_STATUS VARCHAR(120) DEFAULT '${EnrollmentStatus.APPLIED}',
    FORM_RECORD BLOB , 
    ASSIGNED_TO VARCHAR(512) ,
    STATUS VARCHAR(128) , 
    PREV_USER VARCHAR(512) ,
    ARRIVAL_DATE TIMESTAMP , 
    LAST_NAME VARCHAR(512) , 
    EXEMPT BOOLEAN WITH DEFAULT FALSE , 
    LAST_UPDATED TIMESTAMP WITH DEFAULT NULL , 
    DETERMINATION_DATE TIMESTAMP WITH DEFAULT NULL , 
    FIRST_NAME VARCHAR(512) , 
    CONFIRMATION_NUMBER VARCHAR(20),
    EMAIL VARCHAR(512),
    PHONE_NUMBER VARCHAR(20), 

    FOREIGN KEY(ASSIGNED_TO)
      REFERENCES agent(id),
    FOREIGN KEY(PREV_USER)
      REFERENCES agent(id)
    );
`;

const addCardStatusToHouseHold = `
ALTER TABLE household ADD COLUMN card_status VARCHAR(10);
`;

const addCardStatusToRtopTraveller = `
ALTER TABLE rtop_traveller ADD COLUMN card_status VARCHAR(10);
`;

const addCardStatusTimeToHouseHold = `
ALTER TABLE household ADD COLUMN card_status_time TIMESTAMP WITH DEFAULT NULL;
`;

const addCardStatusTimeToRtopTraveller = `
ALTER TABLE rtop_traveller ADD COLUMN card_status_time TIMESTAMP WITH DEFAULT NULL;
`;

const addContactMethod = `
ALTER TABLE HOUSEHOLD
    ADD COLUMN CONTACT_METHOD VARCHAR(32) NOT NULL default '';
`;

const addCardStatusReasonToRtopTraveller = `
ALTER TABLE HOUSEHOLD ADD COLUMN CARD_STATUS_REASON VARCHAR(512);
`;

const createContactMethodVerificationTable = `
CREATE TABLE contact_method_verification(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    status VARCHAR(30) NOT NULL WITH DEFAULT 'created',
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    verified_at TIMESTAMP,
    token VARCHAR(256) NOT NULL,
    contact_method VARCHAR(32) NOT NULL,
    email VARCHAR(32),
    phone_number VARCHAR(32),
    household_id INT,
    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addHouseholdTest2Column = `
ALTER TABLE rtop_traveller
ADD COLUMN test2_date TIMESTAMP NULL;
`;
const addContactMethodFields = `
ALTER TABLE HOUSEHOLD
ADD COLUMN contact_email VARCHAR(512) NOT NULL default ''
ADD COLUMN contact_phone_number VARCHAR(20) NOT NULL default '';
`;

const increaseContactInfoEmailLength = `
ALTER TABLE contact_method_verification
    ALTER COLUMN email
    SET DATA TYPE VARCHAR(512);
`;

const createActivityTable = `
CREATE TABLE rtop_activity(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
    date TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
    agent_id VARCHAR(512) NOT NULL,
    household_id INT NOT NULL,
    type VARCHAR(255) NOT NULL,
    note VARCHAR(2048),
    status VARCHAR(255) NOT NULL,

    FOREIGN KEY(agent_id)
        REFERENCES agent(id),
    FOREIGN KEY(household_id)
        REFERENCES household(id)
);
`;

const addContactMethodVerificationCode = `
    ALTER TABLE contact_method_verification
        ADD COLUMN verification_code VARCHAR(32)
        ADD COLUMN verification_id VARCHAR(256);
`;

const createHouseholdStatusHistoryTable = `
CREATE TABLE household_status_history(
    id integer not null PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
    created TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP, 
    household_id INT NOT NULL, 
    status VARCHAR(255) NOT NULL, 
    status_reason VARCHAR(512), 
    daily_submission_id INT DEFAULT NULL , 
    
    FOREIGN KEY(daily_submission_id) 
        REFERENCES DAILY_SUBMISSION(id), 
    FOREIGN KEY(household_id) 
        REFERENCES household(id));
`;

const addDOBVerificationAttempts = `
ALTER TABLE RTOP_TRAVELLER 
    ADD COLUMN dob_attempt INT DEFAULT 0
    ADD COLUMN dob_attempt_time TIMESTAMP DEFAULT NULL;
`;

const addIsolationStatus = `
ALTER TABLE RTOP_TRAVELLER 
    ADD COLUMN isolation_status BOOLEAN WITH DEFAULT FALSE;
`;

export const CreateRTOPTables = async (withLogging?: boolean):Promise<void> => {
    const conn = await db2Connection();

    const res = [];
    
    res.push(await conn.querySync(createHouseholdTable));
    res.push(await conn.querySync(createDailyReminerTable));
    res.push(await conn.querySync(createRtopTravellerTable));
    res.push(await conn.querySync(createDailySubmissionTable));
    res.push(await conn.querySync(addRtopTravellerConfirmationNumber));
    res.push(await conn.querySync(addCardStatusToHouseHold));
    res.push(await conn.querySync(addCardStatusToRtopTraveller));
    res.push(await conn.querySync(addCardStatusTimeToHouseHold));
    res.push(await conn.querySync(addCardStatusTimeToRtopTraveller));
    res.push(await conn.querySync(addCardStatusReasonToRtopTraveller));
    res.push(await conn.querySync(addContactMethod));
    res.push(await conn.querySync(createContactMethodVerificationTable));
    res.push(await conn.querySync(addHouseholdTest2Column));
    res.push(await conn.querySync(addContactMethodFields));
    res.push(await conn.querySync(increaseContactInfoEmailLength));
    res.push(await conn.querySync(createActivityTable));
    res.push(await conn.querySync(addContactMethodVerificationCode));
    res.push(await conn.querySync(createHouseholdStatusHistoryTable));
    res.push(await conn.querySync(addDOBVerificationAttempts));
    res.push(await conn.querySync(addIsolationStatus));

    await conn.close();
    if(withLogging) {
        LoggerService.info(`Created tables with the following results: ${JSON.stringify(res)}`);
    }
}
