import { Controller, Get, UseInterceptors, ClassSerializerInterceptor, Post, Req, Param, Body, HttpException, HttpStatus, RequestMethod, Put, Res, RequestMapping, Query } from '@nestjs/common';
import { ServiceAlbertaService } from './service-alberta.service';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { Roles, PublicRoute } from '../decorators';
import { DbService } from '../db/dbservice';
import { ReportDTO } from './dto/report.dto';
import { AgenReportAccessDTO } from './dto/report-admin.dto';
import moment from 'moment';
import { Role, Scope } from '../auth/roles.enum';
import { ACLRepository } from './acl.repository';
import { ReportAccessControlListRO } from './ro/report-access.ro';
import { DATE_FORMAT } from '../rtop/enrollment-form.constants';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('/api/v1/admin/report')
export class ACLController {

    constructor(
        private readonly serviceAlbertaService: ServiceAlbertaService,
        private readonly reportRepository: ACLRepository,
    ) { }

    /**
     * Update access to the given entity for a user
     */
    @Roles({ ROLE: Role.OTHER, SCOPE: Scope.ADMIN })
    @Put('/accesscontrols')
    async updateAccessControl(@Req() req, @Body() body: AgenReportAccessDTO): Promise<any> {
        LoggerService.logData(req, req.method === "POST" ? ActionName.GRANT_REPORT_ACCESS : ActionName.UPDATE_REPORT_ACCESS, body.agentEmail, body.reportName);
        await this.reportRepository.createorUpdateReportAccessForAgent(body);
        return 'Ok';
    }

    /**
     * Grant access to the given entity for a user
     */
    @Roles({ ROLE: Role.OTHER, SCOPE: Scope.ADMIN })
    @Post('/accesscontrols')
    async createAccessControl(@Req() req, @Body() body: AgenReportAccessDTO): Promise<any> {
        return this.updateAccessControl(req, body);
    }

    /**
     * List current access grants
     */
    @Roles({ ROLE: Role.OTHER, SCOPE: Scope.ADMIN })
    @Get('/accesscontrols')
    async getAccessControl(@Req() req): Promise<ReportAccessControlListRO> {
        const res = await this.reportRepository.getAccessControlList();
        LoggerService.logData(req, ActionName.LIST_REPORT_ACCESS, "FullReportAccessControlList", [res.accessControl.length]);
        return res;
    }

    @Roles('AHS','GOA','OTHER')
    @Get('/access/:agentEmail')
    async getAgentReportAccess(@Req() req, @Param('agentEmail') agentEmail:string): Promise<any> {
        const res = await this.reportRepository.findReportAccessByAgentEmail(agentEmail);
        LoggerService.logData(req, ActionName.SEARCH, "FullReportAccessControlList", []);
        return res;
    }

    @Roles('REPORTACCESS')
    @Get('/:reportName/details')
    async makeReport(@Query() query: ReportDTO, @Param('reportName') reportName) {
        const startDate = moment(query.fromDate).format(DATE_FORMAT);
        const endDate = moment(query.toDate).subtract(1, 'd').format(DATE_FORMAT);
        const startDateTime = moment(query.fromDate).utc().format("YYYY-MM-DD HH:MM:SS");
        const endDateTime = moment(query.toDate).utc().format("YYYY-MM-DD HH:MM:SS");
        
        switch (reportName) {
            case 'checkpoint':
                return this.serviceAlbertaService.filterReportsByEntry(await DbService.generateCheckpointReport(startDate, endDate));
            case 'serviceAlberta':
                return this.serviceAlbertaService.filterReportResults(await DbService.generateServiceAlbertaReport(startDate, endDate));
            case 'callCount':
                return await DbService.generateCallCountReport(startDateTime, endDateTime);
            default:
                throw new HttpException('Invalid report type', HttpStatus.BAD_REQUEST);
        }
    }

    @Roles('AHS','GOA','OTHER')
    @Get('/active')
    async getAvailableReports(): Promise<any> {
        const res = await this.reportRepository.getAvailableReportsList();
        return {'availableReportsList': res};
    }
}