import { ServiceAlbertaFromQueryRO } from "./ro/service-alberta-query.ro";
import { ServiceAlbertaFormRO } from "./ro/form.ro";
import { ServiceAlbertaTravellerRO } from "./ro/traveller.ro";
import { CheckPointResultRO } from "../service-alberta/ro/checkpoint-report.ro";
import { CHECKED_STATUSES, AIRPORT_OR_BORDER_CROSSINGS } from "./constants";

export class ServiceAlbertaService{
    mapToQueryRO(result:any,userId): ServiceAlbertaFromQueryRO{
        return{
            id:result.id,
            lastUpdated: result.lastUpdated,
            confirmationNumber:result.confirmationNumber,
            agent:result.owner,
            firstName:result.firstName,
            lastName:result.lastName,
            status:result.status,
            assignedToMe:result.assignedTo===userId,
            // Use determination date as arrival date for Service Alberta
            arrivalDate:result.determinationDate
        }
    }

    mapToFormRO(result:any, userId): ServiceAlbertaFormRO{
        return{
            id:result.id,
            firstName:result.firstName,
            lastName:result.lastName,
            age: result.age,
            phoneNumber: result.phoneNumber,
            email: result.email,
            assignedTo: result.assignedTo,
            agent: result.agent,
            additionalTravellers: result.additionalTravellers && result.additionalTravellers.map(this.mapTravellers),
            arrivalDate: result.arrivalDate,
            activities: result.activities,
            assignedToMe: result.agentId === userId
        }
    }

    mapTravellers(additionalTraveller: any): ServiceAlbertaTravellerRO {
        return {
            id:  additionalTraveller.id,
            firstName: additionalTraveller.firstName,
            lastName: additionalTraveller.lastName,
            age: additionalTraveller.age
        }
    }
    
    filterReportResults(queryResults) {
        const results = {};
        for(const x in queryResults){
            if(CHECKED_STATUSES.includes((queryResults[x].STATUS)) ){
                results[queryResults[x].STATUS] = queryResults[x].COUNT;
            }
        }
        for(const x in CHECKED_STATUSES){
            if(!results[CHECKED_STATUSES[x]]){
                results[CHECKED_STATUSES[x]] = 0
            }
        }
        return [results];
    }

    filterReportsByEntry(queryResults: CheckPointResultRO[]) {
        const results = [];
        const totalValues = {
            "Border": "Total"
        }

        queryResults.forEach(element => {
            totalValues[element.reportField] = 0;
        });
        
        for (const eachBorder of AIRPORT_OR_BORDER_CROSSINGS) {
            const resultObj = {
                "Border": eachBorder
            }
            queryResults.forEach(eachReport => {
                resultObj[eachReport.reportField] = eachReport.values[eachBorder] || 0;
                totalValues[eachReport.reportField] += resultObj[eachReport.reportField];
            });
            results.push(resultObj);
        }
        results.push(totalValues);
        return results;
    }
}
