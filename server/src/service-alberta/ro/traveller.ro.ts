import { AdditionalTraveller } from "../../arrival-form/entities/additional-traveller.entity";
import { Exclude, Expose } from 'class-transformer';
import moment from 'moment';

@Exclude()
export class ServiceAlbertaTravellerRO {
  @Expose()
  id: number;

  @Expose()
  firstName: string;

  @Expose()
  lastName: string;

  @Expose()
  age: string;

  constructor(data: AdditionalTraveller) {
    this.age = (moment(new Date(), "YYYY/MM/DD").diff(moment(data.dateOfBirth, "YYYY/MM/DD"), 'years') <= 18)? "Minor": "Adult";
    Object.assign(this, data);

  }
}
