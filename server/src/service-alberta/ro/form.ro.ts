import { ServiceAlbertaTravellerRO } from "./traveller.ro";
import { Exclude, Expose } from 'class-transformer';
import { ActivityRO } from './activity.ro';
import { ValidateNested } from "class-validator";
import moment from 'moment';

@Exclude()
export class ServiceAlbertaFormRO {
  @Expose()
  id: number;
  @Expose()
  firstName: string;
  @Expose()
  lastName: string;

  agentId?: string;
  
  @Expose()
  agent: string;

  @Expose()
  age: string;

  @Expose()
  phoneNumber: string;
  
  @Expose()
  email: string;

  @Expose()
  @ValidateNested({each: true})
  additionalTravellers: Array<ServiceAlbertaTravellerRO>;
  
  @Expose()
  arrivalDate: string;

  @Expose()
  @Expose()
  @ValidateNested({each: true})
  activities: Array<ActivityRO>;

  @Expose()
  assignedToMe: boolean;

  @Expose()
  assignedTo: string;

  constructor(data) {
    data.age = (moment(new Date(), "YYYY/MM/DD").diff(moment(data.dateOfBirth, "YYYY/MM/DD"), 'years') <= 18)? "Minor": "Adult";
    Object.assign(this, data);
    this.additionalTravellers = data.additionalTravellers? data.additionalTravellers.map(t => new ServiceAlbertaTravellerRO(t)): [];
    this.activities = data.activities? data.activities.map(t => new ActivityRO(t)): [];
  }
}
