import { IsString, IsIn, IsOptional, Length, IsDate } from 'class-validator';
import { Type } from 'class-transformer';

export class ServiceAlbertaFormQuery {
    @IsString()
    @IsOptional()
    @Length(3,64)
    query: string;

    @IsString()
    @IsOptional()
    @Length(0, 256)
    agent: string;

    @IsDate()
    @Type(() => Date)
    @IsOptional()
    fromDate: Date;
  
    @IsDate()
    @Type(() => Date)
    @IsOptional()
    toDate: Date;

    @IsString()
    @IsIn(["mine", "unassigned", "completed", "all"])
    @IsOptional()
    filter: string;

    @IsOptional()
    @IsString()
    @IsIn(["asc", "desc"])
    order: string;

    @IsOptional()
    @IsString()
    @IsIn(["status", "owner", "date", "firstName", "lastName", "confirmationNumber", "lastUpdated"])
    orderBy: string;

    @IsString()
    @IsOptional()
    page: string
}
