module.exports = {
  testEnvironment: 'node',
  modulePathIgnorePatterns: ['<rootDir>/build/'],
  coveragePathIgnorePatterns: ['/node_modules/'],
  globals: {
    'ts-jest': {
      'tsconfig': './tsconfig.json'
    }
  },
  moduleFileExtensions: [
    'ts',
    'js',
    'json',
  ],
  transform: {
    '^.+\\.(ts)$': 'ts-jest'
  }
};
