import axios from "axios";

export const AxiosPublic = axios.create({
  headers: {
    Accept: "application/json",
    "Content-type": "application/json",
  },
});

export const AxiosPrivate = axios.create({
  headers: {
    Accept: "application/json",
    "Content-type": "application/json",
  },
});

AxiosPublic.interceptors.response.use((response) => response.data);
AxiosPrivate.interceptors.response.use((response) => response.data);
AxiosPrivate.interceptors.request.use((config) => {
  const auth = JSON.parse(localStorage.getItem("auth"));
  if (auth) {
    config.headers["Authorization"] = `Bearer ${auth.user.accessToken}`;
    config.headers["AuthIssuer"] = `${auth.user.authissuer}`;
  }
  return config;
});
