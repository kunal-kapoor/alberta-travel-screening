import { Place, Vehicle, Places, Vehicles, Question, Province, Provinces } from '../constants';
import { dateToString } from './date';

export const serializeEnrollmentFormValues = (values) => {
  const valuesCopy = { ...values };

  if (valuesCopy.hasAdditionalTravellers === Question.No) {
    valuesCopy.additionalTravellers = [];
  }
  if (valuesCopy.typeOfPlace === Place.Other) {
    valuesCopy.typeOfPlace = valuesCopy.typeOfPlaceDetails;
  }
  if (valuesCopy.howToGetToPlace === Vehicle.Other) {
    valuesCopy.howToGetToPlace = valuesCopy.howToGetToPlaceDetails;
  }
  if (valuesCopy.provinceTerritory === Province.Other) {
    valuesCopy.provinceTerritory = valuesCopy.provinceTerritoryDetails;
  }
  delete valuesCopy.typeOfPlaceDetails;
  delete valuesCopy.howToGetToPlaceDetails;
  delete valuesCopy.provinceTerritoryDetails;

  delete valuesCopy.numberOfAdditionalTravellers;

  return valuesCopy;
};

export const normalizeEnrollmentFormValues = (values) => {
  const valuesCopy = { ...values };

    const fillOtherDetails = (optionsList, dropDownValue, detailsValue, criteria) => {
      if (!optionsList.find(eachValue => eachValue.value === valuesCopy[dropDownValue])) {
        valuesCopy[detailsValue] = valuesCopy[dropDownValue];
        valuesCopy[dropDownValue] = criteria;
      } else {
        valuesCopy[detailsValue] = '';
      }  
    }

    fillOtherDetails(Places, 'typeOfPlace', 'typeOfPlaceDetails', Place.Other);
    fillOtherDetails(Vehicles, 'howToGetToPlace', 'howToGetToPlaceDetails', Vehicle.Other);
    fillOtherDetails(Provinces, 'provinceTerritory', 'provinceTerritoryDetails', Province.Other);

  if (valuesCopy.additionalTravellers?.length > 0) {
    valuesCopy.additionalTravellers.forEach(traveller => traveller.dateOfBirth = dateToString(traveller.dateOfBirth, false));
  }

  valuesCopy.dateOfBirth = dateToString(valuesCopy.dateOfBirth, false);
  valuesCopy.numberOfAdditionalTravellers = valuesCopy.additionalTravellers?.length || 0;

  return valuesCopy;
}
