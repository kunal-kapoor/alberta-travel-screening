import React, { Fragment, useEffect, useState } from 'react';
import Drawer from '@material-ui/core/Drawer';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import { Formik, Form as FormikForm, FastField } from 'formik';
import { useParams, useHistory, useLocation } from 'react-router-dom';

import { ServiceAlbertaDeterminationSchema, ServiceAlbertaStatuses, notesRequiredForStatus } from '../../../constants';
import { useServiceAlbertaLookupConfirmationNumber, useServiceAlbertaDetermination, useConfirmationOnExit } from '../../../hooks';
import { addDaysToDate, dateToString } from '../../../utils';

import { Page, Button, ScrollContainer, Menu, Card, InputFieldLabel } from '../../../components/generic';
import { RenderSelectField, RenderTextField } from '../../../components/fields';

export default () => {
  const params = useParams();
  const history = useHistory();
  const location = useLocation();
  const [isMobileDrawerOpen, setMobileDrawerOpen] = useState(false);

  const {
    lookup,
    assign,
    unassign,
    error,
    initialValues,
    formValues,
    isFetching: isLookupFetching,
  } = useServiceAlbertaLookupConfirmationNumber();

  const {
    submit,
    isFetching: isDeterminationFetching,
  } = useServiceAlbertaDetermination();

  useConfirmationOnExit();

  /**
   * On page load, grab the ID from the url and perform a search query to find the matching form data.
   */
  useEffect(() => {
    const { formId } = location.state || {};
    (async () => {
      if (formId) {
        await assign(formId, params.confirmationNumber);
        history.replace();
      } else {
        await lookup(params.confirmationNumber);
      }
    })();
  }, []);

  const renderSidebar = () => (
    <Box
      p={4}
      height="100%"
      bgcolor="common.white"
      boxShadow="-15px 0px 20px -15px rgba(0, 0, 0, 0.2)"
      borderColor="divider"
      borderTop={1}
      overflow="auto"
    >
      <Formik
        onSubmit={values => submit(formValues.id, values)}
        validationSchema={ServiceAlbertaDeterminationSchema}
        initialValues={initialValues}
      >
        {({values}) => (
        <FormikForm>
          <Grid container spacing={2}>

            {/** Title + (Actions) */}
            <Grid item xs={12}>
              <Box mb={1} component={Grid} container alignItems="center" justify="space-between">
                <Grid item>
                  <Typography variant="subtitle1" color="textSecondary">
                    {formValues.assignedToMe ? 'Update Submission' : 'Submission Details'}
                  </Typography>
                </Grid>
                {formValues.agent && (
                  <Grid item>
                    <Menu
                      label="Actions"
                      size="small"
                      options={[{
                        label: 'Unassign Record',
                        onClick: () => unassign(formValues.id, params.confirmationNumber, formValues.agent),
                      }]}
                    />
                  </Grid>
                )}
              </Box>
              <Divider />
            </Grid>

            {/** Assigned Agent */}
            <Grid item xs={12}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>Assigned Agent</Typography>
              <Box display="flex" alignItems="center">
                <AccountCircleOutlinedIcon />&nbsp;{formValues.agent || 'Unassigned'}
              </Box>
            </Grid>

            {/** Status + Notes */}
            {!formValues.assignedToMe ? (
              <Fragment>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="textSecondary" gutterBottom>Current Status</Typography>
                  {formValues.activities[0]?.status || 'None'}
                </Grid>
              </Fragment>
            ) : (
              <Fragment>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="textSecondary" gutterBottom>Status</Typography>
                  <FastField
                    name="status"
                    placeholder="Select New Status..."
                    component={RenderSelectField}
                    options={ServiceAlbertaStatuses}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography
                      variant="subtitle2"
                      color="textSecondary"
                      gutterBottom>
                    Notes {notesRequiredForStatus(values.status) && '*'}
                  </Typography>
                  <FastField
                    name="note"
                    component={RenderTextField}
                    placeholder="Add notes to document your interaction..."
                    variant="outlined"
                    multiline
                    rows={7}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    type="submit"
                    text="Submit"
                    loading={isDeterminationFetching}
                  />
                </Grid>
              </Fragment>
            )}

            {/** (Assign To Me) */}
            {!formValues.agent && (
              <Grid item xs={12}>
                <Button
                  text="Assign To Me"
                  onClick={() => assign(formValues.id, params.confirmationNumber)}
                />
              </Grid>
            )}

            {/** Previous Notes */}
            <Box mt={1.5} component={Grid} item xs={12}>
              <Typography variant="subtitle2" gutterBottom>Previous Notes</Typography>
              <Divider />
              {formValues.activities.length === 0 ? (
                <Box py={1.5} component={Typography} variant="body2" color="common.darkGrey">
                  There are no notes on this record
                </Box>
              ) : formValues.activities.map(({ date, agent, status, type, note }, index, array) => {
                const isLastChild = index === array.length - 1;
                const formattedDate = dateToString(date, true, 'DD MMM YYYY, hh:mm A');
                return (
                  <Fragment key={index}>
                    <Box pt={2} pb={3}>
                      <Grid container alignItems="center" justify="space-between" spacing={1}>
                        <Grid item>
                          <Box
                            fontSize="14px"
                            lineHeight="20px"
                            letterSpacing="-0.25px"
                          >
                            {formattedDate}
                          </Box>
                        </Grid>
                        <Grid item>
                          <Box
                            fontSize="14px"
                            lineHeight="20px"
                            letterSpacing="-0.25px"
                            display="flex"
                            alignItems="center"
                          >
                            <AccountCircleOutlinedIcon fontSize="small" />&nbsp;{agent}
                          </Box>
                        </Grid>
                      </Grid>
                      <Box
                        pt={2.5}
                        fontSize="16px"
                        fontWeight="bold"
                        lineHeight="19px"
                        letterSpacing="0"
                        color="secondary.main"
                      >
                        {type}: {status}
                      </Box>
                      <Box pt={1.25}>
                        <Typography variant="body2">{note}</Typography>
                      </Box>
                    </Box>
                    {!isLastChild && <Divider />}
                  </Fragment>
                );
              })}
            </Box>
          </Grid>
        </FormikForm>)}
      </Formik>
    </Box>
  );

  if (isLookupFetching) return (
    <Page hideFooter centerContent>
      <CircularProgress />
    </Page>
  );

  if (error) return (
    <Page hideFooter centerContent>
      <Container maxWidth="sm" align="center">
        <Typography variant="body1" paragraph>{error}</Typography>
        <Button
          text="Go Back"
          onClick={() => history.goBack()}
          fullWidth={false}
        />
      </Container>
    </Page>
  );

  return (
    <Page hideFooter>
      <Box component={Grid} height="calc(100vh - 44px)" container>

        {/** Form */}
        <Box component={Grid} item xs={12} sm={7} height="100%">
          <ScrollContainer
            headerContent={(
              <Box
                p={3}
                bgcolor="secondary.main"
                color="common.white"
                display="flex"
                alignItems="center"
                justifyContent="space-between"
              >
                <Container maxWidth="sm" disableGutters>
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="h4">Traveller Submission</Typography>
                    <Button
                      style={{ width: 125 }}
                      text="Back"
                      variant="outlined"
                      size="small"
                      color="inherit"
                      fullWidth={false}
                      onClick={() => history.goBack()}
                    />
                  </Box>
                </Container>
              </Box>
            )}
            bodyContent={(
              <Box p={3}>
                <Container maxWidth="sm" disableGutters>
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} align="center">
                      <Card>
                        <Typography variant="overline" color="textPrimary">
                          Arrival Date:
                        </Typography>
                        <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                          {dateToString(formValues.arrivalDate, true, 'DD MMM YYYY')}
                        </Box>
                      </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} align="center">
                      <Card>
                        <Typography variant="overline" color="textPrimary">
                          Quarantine Complete:
                        </Typography>
                        <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                          {dateToString(addDaysToDate(formValues.arrivalDate, 14), true, 'DD MMM YYYY')}
                        </Box>
                      </Card>
                    </Grid>
                    <Grid item xs={12}>
                      <Card title="Primary Contact" isTitleSmall>
                        <Grid style={{ overflowWrap: 'anywhere' }} container spacing={2}>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Full Name" />
                            <Typography variant="body1" color="textPrimary">{`${formValues.firstName} ${formValues.lastName}`}</Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Age" />
                            <Typography
                              style={{ backgroundColor: formValues.age === 'Minor' ? '#FEBA35' : 'initial', width: 'fit-content' }}
                              variant="body1"
                              color="textPrimary"
                            >
                              {formValues.age}
                            </Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Phone Number" />
                            <Typography variant="body1" color="textPrimary">{formValues.phoneNumber}</Typography>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <InputFieldLabel label="Email" />
                            <Typography variant="body1" color="textPrimary">{formValues.email || 'Not Provided'}</Typography>
                          </Grid>
                        </Grid>
                      </Card>
                    </Grid>
                    {formValues.additionalTravellers.length > 0 && (
                      <Grid item xs={12}>
                        <Card title="Additional Travellers" isTitleSmall>
                          {formValues.additionalTravellers.map(({ firstName, lastName, age }, index, array) => {
                            const isLastItem = index === array.length - 1;
                            return (
                              <Grid style={{ overflowWrap: 'anywhere' }} key={index} container spacing={2}>
                                <Grid item xs={12} md={6}>
                                  <InputFieldLabel label="Full Name" />
                                  <Typography variant="body1" color="textPrimary">{`${firstName} ${lastName}`}</Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                  <InputFieldLabel label="Age" />
                                  <Typography
                                    style={{ backgroundColor: age === 'Minor' ? '#FEBA35' : 'initial', width: 'fit-content' }}
                                    variant="body1"
                                    color="textPrimary"
                                  >
                                    {age}
                                  </Typography>
                                </Grid>
                                {!isLastItem && (
                                  <Grid item xs={12}>
                                    <Divider />
                                  </Grid>
                                )}
                              </Grid>
                            );
                          })}
                        </Card>
                      </Grid>
                    )}
                  </Grid>
                </Container>
              </Box>
            )}
            footerContent={(
              <Hidden smUp>
                <Button
                  style={{ height: 95, borderRadius: 0 }}
                  text="Submit Your Determination"
                  onClick={() => setMobileDrawerOpen(true)}
                />
              </Hidden>
            )}
          />
        </Box>

        {/** Sidebar - Desktop */}
        <Hidden xsDown>
          <Box component={Grid} item xs={12} sm={5} height="100%">
            {renderSidebar()}
          </Box>
        </Hidden>

        {/** Sidebar - Mobile */}
        <Hidden smUp>
          <Drawer
            anchor="right"
            open={isMobileDrawerOpen}
            onClose={() => setMobileDrawerOpen(false)}
          >
            {renderSidebar()}
          </Drawer>
        </Hidden>
      </Box>
    </Page>
  );
};
