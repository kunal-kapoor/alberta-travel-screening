import React, { useEffect, useState } from 'react';
import Drawer from '@material-ui/core/Drawer';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import { Formik, Form as FormikForm, FastField } from 'formik';
import { useParams, useHistory } from 'react-router-dom';

import IsolationPlanFailIcon from '../../../assets/images/isolation-fail-admin.svg';
import IsolationPlanPassIcon from '../../../assets/images/isolation-pass.svg';

import { BackOfficeDeterminationSchema, BackOfficeDeterminations } from '../../../constants';
import { useBackOfficeLookupConfirmationNumber, useBackOfficeDetermination, useConfirmationOnExit, useUpdateForm } from '../../../hooks';

import { Form } from '../../../components/form/Form';
import { Page, Button, ScrollContainer, Card, InfoBox } from '../../../components/generic';
import { RenderButtonGroup, RenderTextField, RenderCheckbox } from '../../../components/fields';

export default () => {
  const params = useParams();
  const history = useHistory();
  const [isMobileDrawerOpen, setMobileDrawerOpen] = useState(false);
  const { updateForm, isFetching, isEditing, setIsEditing } = useUpdateForm();

  const {
    lookup,
    error,
    formValues,
    initialValues,
    isFetching: isLookupFetching,
  } = useBackOfficeLookupConfirmationNumber();

  const {
    submit,
    isFetching: isDeterminationFetching,
  } = useBackOfficeDetermination();

  
  useConfirmationOnExit();

  /**
   * On page load, grab the ID from the url and perform a search query to find the matching form data.
   */
  useEffect(() => {
    (async () => await lookup(params.confirmationNumber))();
  }, [params.confirmationNumber]);

  const renderSidebar = () => (
    <Box
      p={4}
      height="100%"
      bgcolor="common.white"
      boxShadow="-15px 0px 20px -15px rgba(0, 0, 0, 0.2)"
      borderColor="divider"
      borderTop={1}
      overflow="auto"
    >
      <Formik
        onSubmit={values => submit(formValues.id, values)}
        validationSchema={BackOfficeDeterminationSchema}
        initialValues={initialValues}
      >
        {({ values }) => (
        <FormikForm>
          <Grid container spacing={3}>

            {/** Title */}
            <Grid item xs={12}>
              <Typography variant="subtitle1" color="textSecondary" gutterBottom>Provincial Official Determination</Typography>
              <Divider />
            </Grid>

            {/** Determination */}
            <Grid item xs={12}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>Determination*</Typography>
              <FastField
                name="determination"
                component={RenderButtonGroup}
                options={BackOfficeDeterminations}
              />
            </Grid>

            {/** Exempt status */}
            <Grid item xs={12}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>Exempt Worker Status</Typography>
              <FastField
                name="exempt"
                component={RenderCheckbox}
                label="Traveller is an exempt worker"
              />
              {(values.exempt) && (
                <InfoBox message="Please ensure that you add notes about the nature of the travellers exemption below" />
              )}
            </Grid>

            {/** Notes */}
            <Grid item xs={12}>
              <Typography variant="subtitle2" color="textSecondary" gutterBottom>Notes*</Typography>
              <FastField
                name="notes"
                component={RenderTextField}
                placeholder="Add notes to support your decision..."
                variant="outlined"
                multiline
                rows={10}
              />
            </Grid>

            {/** Submit */}
            <Grid item xs={12}>
              <Button
                type="submit"
                text="Submit"
                loading={isDeterminationFetching}
              />
            </Grid>
          </Grid>
        </FormikForm>
        )}
      </Formik>
    </Box>
  );

  if (isLookupFetching) return (
    <Page hideFooter centerContent>
      <CircularProgress />
    </Page>
  );

  if (error) return (
    <Page hideFooter centerContent>
      <Container maxWidth="sm" align="center">
        <Typography variant="body1" paragraph>{error}</Typography>
        <Button
          text="Go Back"
          onClick={() => history.goBack()}
          fullWidth={false}
        />
      </Container>
    </Page>
  );

  return (
   <Page hideFooter>
     <Box component={Grid} height="calc(100vh - 44px)" container>

       {/** Form */}
       <Box component={Grid} item xs={12} sm={7} height="100%">
         <ScrollContainer
           headerContent={(
             <Box
               p={3}
               bgcolor="secondary.main"
               color="common.white"
               display="flex"
               alignItems="center"
               justifyContent="space-between"
             >
               <Container maxWidth="sm" disableGutters>
                 <Box display="flex" alignItems="center" justifyContent="space-between">
                   <Typography variant="h4">Traveller Submission</Typography>
                   <Button
                     style={{ width: 125 }}
                     text="Back"
                     variant="outlined"
                     size="small"
                     color="inherit"
                     fullWidth={false}
                     onClick={() => history.goBack()}
                   />
                 </Box>
               </Container>
             </Box>
           )}
           bodyContent={(
             <Box p={3}>
               <Container maxWidth="sm" disableGutters>
                 <Grid container spacing={3}>
                   <Grid item xs={12} md={6} align="center">
                     <Card>
                       <Typography variant="overline" color="textPrimary">
                         Confirmation Number:
                       </Typography>
                       <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                         {formValues.confirmationNumber}
                       </Box>
                     </Card>
                   </Grid>
                   <Grid item xs={12} md={6} align="center">
                     <Card>
                       <Typography variant="overline" color="textPrimary">
                         Isolation Plan Status:
                       </Typography>
                       <Box my={1.5} component={Typography} variant="h2" color="text.secondary">
                         <img
                           src={formValues.viableIsolationPlan ? IsolationPlanPassIcon : IsolationPlanFailIcon}
                           height={46}
                           alt="Isolation Plan Status"
                         />
                       </Box>
                     </Card>
                   </Grid>
                   <Grid item xs={12}>
                     <Form
                        onSubmit={values => updateForm(formValues.id, values)}
                        isFetching={isFetching}
                        initialValues={formValues}
                        isEditing={isEditing}
                        setIsEditing={setIsEditing}
                        isDisabled
                        canEdit
                      />
                   </Grid>
                 </Grid>
               </Container>
             </Box>
           )}
           footerContent={(
             <Hidden smUp>
               <Button
                 style={{ height: 95, borderRadius: 0 }}
                 text="Submit Your Determination"
                 onClick={() => setMobileDrawerOpen(true)}
               />
             </Hidden>
           )}
         />
       </Box>

       {/** Sidebar - Desktop */}
       <Hidden xsDown>
         <Box component={Grid} item xs={12} sm={5} height="100%">
           {renderSidebar()}
         </Box>
       </Hidden>

       {/** Sidebar - Mobile */}
       <Hidden smUp>
         <Drawer
           anchor="right"
           open={isMobileDrawerOpen}
           onClose={() => setMobileDrawerOpen(false)}
         >
           {renderSidebar()}
         </Drawer>
       </Hidden>
     </Box>
   </Page>
  );
};
