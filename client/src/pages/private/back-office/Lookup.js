import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';

import { Route } from '../../../constants';
import { useValidateLookupQuery } from '../../../hooks';

import { Page, Card, SearchBar } from '../../../components/generic';
import { UnwillingTravellerButton } from '../../../components/form/UnwillingTraveller';

export default () => {
  const history = useHistory();
  const { validateQuery } = useValidateLookupQuery();

  const handleSearchLastName = (value) => value && history.push(Route.BackOfficeLookupLastName.dynamicRoute(`?query=${value}`));
  const handleSearchConfirmationNumber = (value) => value && history.push(Route.BackOfficeLookupConfirmationNumber.dynamicRoute(value));

  return (
    <Page hideFooter centerContent>
      <Grid container justify="center">
        <Grid item xs={12} sm={8} md={6} lg={4} xl={3}>
          <Box m={2}>
            <Card title="Submission Lookup">
              <Grid container spacing={3}>

                {/** Confirmation Number */}
                <Grid item xs={12}>
                  <Typography variant="body1" color="textSecondary" noWrap gutterBottom>By confirmation number:</Typography>
                  <SearchBar
                    name="number"
                    placeholder="Eg: AB12345"
                    onSearch={(value) => validateQuery(value, handleSearchConfirmationNumber)}
                  />
                </Grid>

                {/** Last Name */}
                <Grid item xs={12}>
                  <Typography variant="body1" color="textSecondary" noWrap gutterBottom>By traveller last name:</Typography>
                  <SearchBar
                    name="name"
                    placeholder="Enter last name..."
                    onSearch={(value) => validateQuery(value, handleSearchLastName)}
                  />
                </Grid>
              </Grid>
            </Card>
          </Box>
        </Grid>

        <Grid item xs={12}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <UnwillingTravellerButton/>
          </Box>
        </Grid>
      </Grid>
    </Page>
  );
};
