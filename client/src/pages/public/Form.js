import React, { useState, useEffect } from 'react';
import Collapse from '@material-ui/core/Collapse';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Link from '@material-ui/core/Link';
import { useHistory, Prompt } from "react-router-dom";
import { useTranslation } from "react-i18next";

import { useForm } from '../../hooks';
import { useConfirmationOnExit } from '../../hooks';

import { Page } from '../../components/generic';
import { Form } from '../../components/form/Form';
import { Route } from '../../constants';
import { CollectionNotice } from '../../components/generic/CollectionNotice';

export default () => {
  const history = useHistory();
  const { t, i18n } = useTranslation();
  const { submit, isFetching } = useForm();
  const [isGreyBannerExpanded, setGreyBannerExpanded] = useState(window.innerWidth > 750);
  const [isCollectionNoticeExpanded, setCollectionNoticeExpanded] = useState(false);

  useEffect(() => {
    const lng = history.location.pathname.substring(1) || 'en';
    (async () => {
      await i18n.changeLanguage(lng);
      document.title = t("Alberta Isolation Questionnaire");
      setTimeout(() => window.scrollTo(0, 0), 100);
    })();
  }, []);

  useConfirmationOnExit();

  return (
    <Page>

      {/** Prompts the user before refreshing the page - specifically - the logo is clicked */}
      <Prompt message={(location) => {
        const isHeaderLogoClicked = location.pathname === Route.Root;
        return isHeaderLogoClicked ? t("Are you sure you want to leave?") : true
      }} />

      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Box mb={2.5} component={Typography} variant="h1">
            {t("Alberta Isolation Questionnaire")}
          </Box>
          <Typography variant="subtitle1">
            {t("One member of each household must complete the Alberta Isolation Questionnaire and submit it at the provincial check point. The Government of Alberta will be contacting all households to ensure compliance with public health measures.")}
          </Typography>
          <Hidden smUp>
            <Box
              mt={0.25}
              component={Typography}
              variant="subtitle1"
              color="common.white"
              style={{ cursor: 'pointer', textDecoration: 'underline' }}
              onClick={() => setGreyBannerExpanded(prevState => !prevState)}
            >
              {!isGreyBannerExpanded ? t("Show More") : t("Show Less")}
            </Box>
          </Hidden>
        </Container>
      </Box>

      {/** Grey Banner */}
      <Collapse in={isGreyBannerExpanded}>
        <Box pt={[3, 6]} pb={[3, 6]} bgcolor="common.lightGrey">
          <Container maxWidth="sm">
            <Box mb={2.5} component={Typography} variant="body1" color="text.primary">
              {t("The Government of Alberta has implemented public health measures to help stop the spread of COVID-19. All travellers arriving from outside Canada are legally required, in accordance with the requirements of Alberta's Chief Medical Officer of Health (CMOH) Orders 05-2020 and 11-2020, to:")}
            </Box>
            <Typography component="div" variant="body1" color="textPrimary">
              <ul>
                <li>
                  {t("Be in quarantine for a minimum of 14 days;")}
                </li>
                <li>
                  {t("Complete an Isolation Questionnaire; and")}
                </li>
                <li>
                  {t("If exhibiting symptoms of COVID 19, which are not related to a pre-existing illness or health condition, be in isolation for a minimum of 10 days from the start of symptoms, or until symptoms resolve, whichever is longer.")}
                </li>
              </ul>
            </Typography>
          </Container>
        </Box>
      </Collapse>

      {/** Collection Notice */}
      <CollectionNotice/>

      {/** Form */}
      <Container maxWidth="sm">
        <Box mt={2} mb={4}>
          <Form
            onSubmit={submit}
            isFetching={isFetching}
            initialValues={{

              // Primary Contact.
              firstName: '',
              lastName: '',
              dateOfBirth: '',
              phoneNumber: '',
              email: '',

              // Travel Information.
              hasAdditionalTravellers: '',
              numberOfAdditionalTravellers: 0,
              additionalTravellers: [],

              // Arrival Information.
              nameOfAirportOrBorderCrossing: '',
              arrivalDate: '',
              arrivalCityOrTown: '',
              arrivalCountry: '',
              numberOfAdditionalCitiesAndCountries: 0,
              additionalCitiesAndCountries: [],

              // Isolation Questionnaire.
              hasPlaceToStayForQuarantine: '',
              quarantineLocation: {
                address: '',
                cityOrTown: '',
                provinceTerritory: '',
                postalCode: '',
                provinceTerritoryDetails: '',
                phoneNumber: '',
                typeOfPlace: '',
                typeOfPlaceDetails: '',
                howToGetToPlace: '',
                howToGetToPlaceDetails: '',
                doesVulnerablePersonLiveThere: '',
                otherPeopleResiding: '',
              },
              isAbleToMakeNecessaryArrangements: '',
            }}
          />
        </Box>
      </Container>
    </Page>
  );
};
