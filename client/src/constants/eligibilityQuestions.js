import React from 'react';

export const EligibilityQuestions = [
    {
        name: "residency",
        required: "Must specify if permitted entry into Canada",
        question: "Are all members of your household a returning Canadian citizen, a permanent resident or a foreign national currently permitted entry into Canada (for example, immediate and extended family members of Canadian citizens and permanent residents)?",
        eligibleAnswer: "Yes"
    },
    {
        name: "symptoms",
        required: "Must specify if you have COVID-19 symptoms",
        question: <span>Do you currently have any signs or <a href="https://www.alberta.ca/covid-19-testing-in-alberta.aspx" target="_blank">symptoms</a> of COVID-19?</span>,
        eligibleAnswer: "No"
    },
    {
        name: "covidContact",
        required: "Must specify if you have been in contact with confirmed COVID-19 case",
        question: "Have you, or anyone in your household, been in contact with a confirmed COVID-19 case in the last 14 days?",
        eligibleAnswer: "No"
    },
    {
        name: "quarantinePlan",
        required: "Must specify if you have acceptable quarantine plan",
        question: "Does your household have an acceptable quarantine plan?",
        eligibleAnswer: "Yes"
    },
    {
        name: "remainingInAlberta",
        required: "Must specify if all members are remaining in Alberta for 14 days",
        question: "Are all members of your household remaining in Alberta for 14 days after entering Canada or are you departing the country directly from Alberta prior to the end of 14 days?",
        eligibleAnswer: "Yes"
    }
];

// Returns true if the submitted answers
// all match the eligibleAnswer of the corresponding 
// EligibilityQuestion
export const answersAreEligible = (answers) => {
    return !Object.entries(answers).find(([question, answer]) => EligibilityQuestions.find(q => q.name === question).eligibleAnswer !== answer);
}