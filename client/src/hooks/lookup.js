import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { Route, BackOfficeLookupLastNameColumns, ServiceAlbertaLookupColumns, ServiceAlbertaLookupFilters, 
  DailyMonitoringLookupColumns, DailyMonitoringLookupFilters, HouseholdMembersColumns, TravellerMonitoringColumns, EnrollmentStatus } from '../constants';
import {
  AxiosPrivate,
  normalizeFormValues,
  getOptionalURLParams,
  setOptionalURLParams,
  removeOptionalURLParam,
  dateToString,
  AxiosPublic,
} from '../utils';
import { useModal, useToast } from '.';

import { Button } from '../components/generic';
import moment from 'moment';
import { useReportsAccessControl } from './admin';

export const useValidateLookupQuery = () => {
  const { openToast } = useToast();
  return {
    validateQuery: (value, searchFunction) => {
      if (value.length < 2 && value.length > 0) {
        openToast({ status: 'error', message: 'Value entered must be 2 characters or more' });
      } else if (value.length > 64) {
        openToast({ status: 'error', message: 'Value entered must be 64 characters or less' });
      } else {
        searchFunction(value);
      }
    },
  }
};

export const useBackOfficeLookupLastName = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(true);
  const [tableData, setTableData] = useState({
    columns: BackOfficeLookupLastNameColumns,
    rows: [],
  });

  useEffect(() => {
    (async () => {
      try {
        const { query = '' } = getOptionalURLParams(history);
        const { travellers = [] } = await AxiosPrivate.get(`/api/v1/admin/back-office/lastname/${query}`);
        handleSetTableData(travellers);
      } catch (e) {
        setTableData(prevState => ({ ...prevState, rows: [] }));
      } finally {
        setFetching(false);
      }
    })();
  }, [history.location.search]);

  const handleSetTableData = (travellers) => {
    setTableData(prevState => ({
      ...prevState,
      rows: travellers.map((traveller) => ({
        date: dateToString(traveller.arrivalDate, false),
        lastName: traveller.lastName,
        firstName: traveller.firstName,
        cityCountry: `${traveller.arrivalCityOrTown}, ${traveller.arrivalCountry}`,
        confirmationNumber: traveller.confirmationNumber,
        viewMore: (
          <Button
            onClick={() => history.push(Route.BackOfficeLookupConfirmationNumber.dynamicRoute(traveller.confirmationNumber))}
            size="small"
            text="View"
          />
        ),
      })),
    }));
  };

  return {
    isFetching,
    tableData,
    onSearch: (query) => setOptionalURLParams(history, { query }),
  }
};

export const useBackOfficeLookupConfirmationNumber = () => {
  const [isFetching, setFetching] = useState(true);
  const [error, setError] = useState(null);
  const [formValues, setFormValues] = useState(null);
  const [initialValues, setInitialValues] = useState({
    determination: '',
    notes: '',
    exempt: false,
  });

  return {
    isFetching,
    error,
    formValues,
    initialValues,
    lookup: async (confirmationNumber) => {
      try {
        const results = await AxiosPrivate.get(`/api/v1/admin/back-office/form/${confirmationNumber}`);
        const { determination = '', notes = '', exempt = false } = results.determination || {};
        setInitialValues({ determination, notes, exempt });
        setFormValues(normalizeFormValues(results));
      } catch (e) {
        setError(`Failed to find submission with Confirmation Number: "${confirmationNumber}"`);
      } finally {
        setFetching(false);
      }
    }
  }
};

export const useBackOfficeDetermination = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const { openToast } = useToast();

  return {
    isFetching,
    submit: async (formId, values) => {
      try {
        setFetching(true);
        await AxiosPrivate.patch(`/api/v1/admin/back-office/form/${formId}/determination`, values);
        openToast({ status: 'success', message: 'Submission updated' });
        history.push(Route.BackOfficeLookup);
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to update this submission' });
        setFetching(false);
      }
    }
  }
};

export const useServiceAlbertaLookup = () => {
  const { openToast } = useToast();
  const history = useHistory();
  const [isFetching, setFetching] = useState(true);
  const [tableData, setTableData] = useState({
    columns: ServiceAlbertaLookupColumns,
    rows: [],
    totalRows: 0,
    currentPage: 0,
  });

  /**
   * Format text that should be used for filter pills.
   * 
   * display: Given the current filter values, should return a string that should be displayed
   * isZeroState: Given the current filter values, should return true if the current values can
   *    be considered to be the values zero state
   */
  const formatFilterPills = {
    date: {
      display: ({fromDate, toDate}) => {
        if(fromDate && toDate) {
          return `${dateToString(fromDate)} - ${dateToString(toDate)}`;
        } else if (fromDate) {
          return `From ${dateToString(fromDate)}`;
        } else if (toDate) {
          return `To ${dateToString(toDate)}`;
        } else {
          return '';
        }
      },
      isZeroState: ({fromDate, toDate}) => !fromDate && !toDate
    },
    agent: {
      display: ({agent}) => {
        if(agent && agent.length > 18) {
          agent = `${agent.substring(0, 18)}...`;
        }

        return `${agent}`;
      },
      isZeroState: ({agent}) => agent === 'all' || !agent
    },
    query: {
      display: ({query}) => {
        if(query && query.length > 18) {
          query = `${query.substring(0,18)}...`;
        }
        return `${query}`
      },
      isZeroState: ({query}) => !query
    }
  }

  const [initialFilters, setInitialFilters] = useState(null);
  const [selectedSearchFilters, setSelectedSearchFilters] = useState([]);
  const [zeroStates] = useState({
    agent: 'all',
    fromDate: '',
    toDate: '',
    query: '',
  });

  const validDateOrEmpty = dstr => {
    if(!dstr) { return ''; }
    const asDate = moment(dstr);
    return asDate.isValid()? asDate.format('YYYY-MM-DD'): '';
  };

  // Parse url filters / sorting / query string from the current url
  // Formats dates + decodes url params if necessary
  const parseUrlFilters = () => {
    let { page = 0, fromDate='', toDate='', agent='', query = '', order = 'asc', orderBy = ServiceAlbertaLookupColumns[0].value, filter = ServiceAlbertaLookupFilters[1].value } = getOptionalURLParams(history);
    if(fromDate) {
      fromDate = moment(fromDate).startOf('day');
    } 

    if(toDate) {
      toDate = moment(toDate).endOf('day');
    }

    if(agent) {
      try {
        agent = decodeURIComponent(agent);
      } catch {}
    }

    if(query) {
      try {
        query = decodeURIComponent(query);
      } catch {}
    }

    return {page, agent, query, order, orderBy, filter, fromDate, toDate};
  }

  // Set initial filters on page load
  useEffect(() => {
    const {agent, fromDate, toDate, query} = parseUrlFilters();

    setInitialFilters({
      agent: agent || 'all',
      fromDate: validDateOrEmpty(fromDate),
      toDate: validDateOrEmpty(toDate),
      query,
    });
  }, []);

  useEffect(() => {
    (async () => {
      try {
        setFetching(true);

        const filterValues = parseUrlFilters();
        const { page, agent, query, order, orderBy, filter, fromDate, toDate } = filterValues;

        // Find appliccable filters that have been changed (not in their zero-state)
        const appliedFilters = ['query', 'agent', 'date'].filter(f => !formatFilterPills[f].isZeroState(filterValues));
        
        // Update state with the filters that have been applied
        // this populates the filter "pills" at the top of the work list
        setSelectedSearchFilters(appliedFilters.map(f => ({
          value: f,
          label: formatFilterPills[f].display(filterValues)
        })));
        
        // Construct query
        const queryFilter = query ? `query=${query}&` : '';

        let qs = `${queryFilter}filter=${filter}&orderBy=${orderBy}&order=${order}&page=${page}&agent=${agent}`;
        if(fromDate) {
          qs += `&fromDate=${moment(fromDate).toISOString()}`;
        }
        if(toDate) {
          qs += `&toDate=${moment(toDate).toISOString()}`;
        }

        // Actually perform search
        const { results = [], numberOfResults = 0 } = await AxiosPrivate.get(`/api/v1/admin/service-alberta/form?${qs}`);
        handleSetTableData(results, numberOfResults, page);
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to lookup' });
      } finally {
        setFetching(false);
      }
    })();
  }, [history.location.search]);

  const handleSetTableData = (results, numberOfResults, currentPage) => {

    const getStatus = (status) => {
      if (!status) return 'None';
      return status;
    };

    setTableData(prevState => ({
      ...prevState,
      totalRows: numberOfResults,
      currentPage: parseInt(currentPage),
      rows: results.map((item) => ({
        date: dateToString(item.arrivalDate, true, 'DD MMM YYYY'),
        agent: item.agent || 'None',
        firstName: item.firstName,
        lastName:item.lastName,
        status: getStatus(item.status),
        lastUpdated: item.lastUpdated ?
          moment
            .utc(item.lastUpdated)
            .local()
            .format('DD MMM YYYY hh:mm A')
          : "-",
        viewMore: (!item.assignedToMe && !item.agent) ? (
          <Button
            text="Assign to Me"
            size="small"
            onClick={() => history.push(Route.ServiceAlbertaLookupConfirmationNumber.dynamicRoute(item.confirmationNumber), {
              formId: item.id,
              confirmationNumber: item.confirmationNumber,
              agent: item.agent,
            })}
          />
        ) : (
          <Button
            text="View"
            variant="outlined"
            size="small"
            onClick={() => history.push(Route.ServiceAlbertaLookupConfirmationNumber.dynamicRoute(item.confirmationNumber))}
          />
        ),
      })),
    }));
  };

  return {
    onSubmit: ({query, fromDate, toDate, agent}) => {
      if(fromDate) {
        fromDate = validDateOrEmpty(fromDate);
      }
      
      if(toDate) {
        toDate = validDateOrEmpty(toDate);
      }

      return setOptionalURLParams(history, {query, fromDate, toDate, agent, page: 0})
    },
    onSearch: (query) => setOptionalURLParams(history, { query, page: 0 }),
    onFilter: (filter) => setOptionalURLParams(history, { filter, page: 0 }),
    onAgent: (agent) => setOptionalURLParams(history, { agent, page: 0 }),
    onSort: (orderBy, order) => setOptionalURLParams(history, { orderBy, order, page: 0 }),
    onChangePage: (page) => setOptionalURLParams(history, { page }),
    clearFilter: (filter) => removeOptionalURLParam(history, filter),
    tableData,
    isFetching,
    initialFilters,
    selectedSearchFilters,
    zeroStates,
  }
};

export const useServiceAlbertaLookupConfirmationNumber = () => {
  const { openModal, closeModal } = useModal();
  const { openToast } = useToast();
  const [isFetching, setFetching] = useState(true);
  const [error, setError] = useState(null);
  const [formValues, setFormValues] = useState(null);
  const [initialValues, setInitialValues] = useState({ status: '', note: '' });

  const handleAssign = async (formId, confirmationNumber) => {
    try {
      setFetching(true);
      await AxiosPrivate.post(`/api/v1/admin/service-alberta/form/${formId}/assign`);
      await handleLookup(confirmationNumber);
      openToast({
        status: 'warning',
        message: `Record ${confirmationNumber} has been assigned to you.`,
        timeout: null,
        actionText: 'Undo Assignment',
        onActionClick: () => handleUnassign(formId, confirmationNumber),
      });
    } catch (e) {
      setError(e.message || 'Failed to assign this submission');
    } finally {
      setFetching(false);
    }
  };

  const handleUnassign = async (formId, confirmationNumber) => {
    try {
      closeModal();
      setFetching(true);
      await AxiosPrivate.post(`/api/v1/admin/service-alberta/form/${formId}/assign/undo`);
      await handleLookup(confirmationNumber);
      openToast({ status: 'success', message: 'Submission unassigned' });
    } catch (e) {
      openToast({ status: 'error', message: e.message || 'Failed to unassign this submission' });
    } finally {
      setFetching(false);
    }
  };

  const handleLookup = async (confirmationNumber) => {
    try {
      setFetching(true);
      const results = await AxiosPrivate.get(`/api/v1/admin/service-alberta/form/${confirmationNumber}`);
      const { status = '' } = results.activities[0] || {};
      setInitialValues({ status, note: '' });
      setFormValues(normalizeFormValues(results));
    } catch (e) {
      setError(`Failed to find submission with Confirmation Number: "${confirmationNumber}"`);
    } finally {
      setFetching(false);
    }
  };

  return {
    isFetching,
    error,
    initialValues,
    formValues,
    lookup: handleLookup,
    assign: handleAssign,
    unassign: (formId, confirmationNumber, agent) => {
      openModal({
        title: 'Are you Sure?',
        description: `This record is already assigned to ${agent}. Are you sure you want to unassign this record?`,
        negativeActionText: 'No',
        positiveActionText: 'Yes',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleUnassign(formId, confirmationNumber),
      });
    },
  }
};

export const useServiceAlbertaDetermination = () => {
  const history = useHistory();
  const [isFetching, setFetching] = useState(false);
  const { openModal, closeModal } = useModal();
  const { openToast } = useToast();

  const handleSubmit = async (formId, values) => {
    try {
      closeModal();
      setFetching(true);
      await AxiosPrivate.post(`/api/v1/admin/service-alberta/form/${formId}/activity`, values);
      openToast({ status: 'success', message: 'Submission updated' });
      history.push(Route.ServiceAlbertaLookup.dynamicRoute());
    } catch (e) {
      openToast({ status: 'error', message: e.message || 'Failed to update this submission' });
      setFetching(false);
    }
  };

  return {
    isFetching,
    submit: (formId, values) => {
      openModal({
        title: 'Ready to Submit?',
        description: 'Are you ready to submit this record?',
        negativeActionText: 'Make Changes',
        positiveActionText: 'Yes, submit now.',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleSubmit(formId, values),
      });
    }
  }
};

export const useReportLookup = () => {
  const { openToast } = useToast();

  const [isFetching, setFetching] = useState(false);
  const [hasExecuted, setHasExecuted] = useState(false);
  const [tableData, setTableData] = useState({
      columns: [],
      rows: [],
  });
  const [reportOptions, setReportOptions] = useState([]);
  const { reportAccess } = useReportsAccessControl();
  const { availableReports } = useDefaultReportDashboards();

  useEffect(() => {
      setReportOptions(reportAccess.reports.map((item) => {
          return availableReports.find((ele) => ele.value === item)
      }));
  }, [reportAccess, availableReports]);

  const executeReport = async({ fromDate, toDate, report, ...rest }) => {
      setFetching(true);
      try {
          const result = await AxiosPrivate.get(`/api/v1/admin/report/${report}/details`, {
            params: {
              fromDate: moment(fromDate).startOf('day').local(true).format(),
              toDate: moment(toDate).endOf('day').local(true).format()
            }
          });
          handleSetTableData(result || []);
      } catch (e) {
          openToast({ status: 'error', message: e.message || 'Failed to execute report' });
          handleSetTableData([]);
      } finally {
          setFetching(false);
          setHasExecuted(true);
      }
  }

  const handleSetTableData = (result=[]) => {
    const columns = result.length? Object.keys(result[0]).map(r => ({key: r, label: r})): [{
      label: 'No Results',
      value: 'noResults'
    }];
    
    result = result.length? result: [{noResult: 'No Result'}];

      setTableData(prevState => ({
          ...prevState,
          columns,
          rows: result,
      }));
  };

  return {
      isFetching,
      tableData,
      hasExecuted,
      reportOptions,
      executeReport
  }
};

export const useAccessControlLookup = () => {
  const { openToast } = useToast();

  const [isFetching, setFetching] = useState(false);
  const [tableData, setTableData] = useState({
    rows: []
  });
  const { openModal, closeModal } = useModal();

  const fetchAccessControl = async () => {
    try {
      setFetching(true);
      const { accessControl = [] } = await AxiosPrivate.get(`/api/v1/admin/report/accesscontrols`);
      handleSetTableData(accessControl);
    }
    catch (e) {
      openToast({ status: 'error', message: e.message || 'Failed to fetch data' });
    }
    finally {
      setFetching(false);
    }
  }

  const handleSetTableData = (results) => {

    setTableData(prevState => ({
      rows: results.map((item) => ({
        agentEmail: item.agentEmail,
        reportName: item.reportName,
      })),
    }));
  };

  useEffect(() => {
    (async () => {
      await fetchAccessControl();
    })();
  }, []);

  const handleCreateOrUpdateAccessControl = async ({ reportName, agentEmail }, resetForm, isUpdate) => {
    try {
      closeModal();
      setFetching(true);
      const accessControlsURL = `/api/v1/admin/report/accesscontrols`;
      if (isUpdate) {
        await AxiosPrivate.put(accessControlsURL, {
          reportName,
          agentEmail
        });
      }
      else {
        await AxiosPrivate.post(accessControlsURL, {
          reportName,
          agentEmail
        });
      }
      openToast({ status: 'success', message: 'Submission updated' });
      if (resetForm) {
        resetForm({
          'report': [],
          'userEmailAddress': ''
        })
      }
      await fetchAccessControl();

    } catch (e) {
      openToast({ status: 'error', message: e.message || 'Failed to update this submission' });
    }
    finally {
      setFetching(false);
    }
  }

  return {
    isFetching,
    tableData,
    updateAccessControl: (values, isUpdate, resetForm) => {
      openModal({
        title: 'Ready to Submit?',
        description: 'Are you ready to submit the changes?',
        negativeActionText: 'Cancel',
        positiveActionText: 'Yes, submit now.',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleCreateOrUpdateAccessControl(values, resetForm, isUpdate),
      });
    }
  }
};

export const useDefaultReportDashboards = () => {
  const [availableReports, setAvailableReports] = useState([]);

  useEffect(() => {
      (async() => {
          const { availableReportsList = {} } = await AxiosPrivate.get(`/api/v1/admin/report/active`);
          setAvailableReports(Object.entries(availableReportsList).map(([key, value]) => {
              return { value, label: key };
          }));
      })();
  }, []);

  return {
      availableReports
  }
};
