import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Route, DailyMonitoringLookupColumns, DailyMonitoringLookupFilters, HouseholdMembersColumns, TravellerMonitoringColumns, EnrollmentStatus, DailySymptoms } from '../constants';
import {
  AxiosPrivate,
  normalizeEnrollmentFormValues,
  getOptionalURLParams,
  setOptionalURLParams,
  removeOptionalURLParam,
  dateToString,
} from '../utils';
import { useModal, useToast } from '.';

import { Button, StatusChips } from '../components/generic';
import { HouseholdStatus } from '../constants';
import moment from 'moment';
import Switch from '@material-ui/core/Switch';


export const useDailyMonitoringLookup = () => {
    const { openToast } = useToast();
    const history = useHistory();
    const [isFetching, setFetching] = useState(true);
    const [tableData, setTableData] = useState({
      columns: DailyMonitoringLookupColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
  
    /**
     * Format text that should be used for filter pills.
     * 
     * display: Given the current filter values, should return a string that should be displayed
     * isZeroState: Given the current filter values, should return true if the current values can
     *    be considered to be the values zero state
     */
    const formatFilterPills = {
      date: {
        display: ({fromDate, toDate}) => {
          if(fromDate && toDate) {
            return `${dateToString(fromDate)} - ${dateToString(toDate)}`;
          } else if (fromDate) {
            return `From ${dateToString(fromDate)}`;
          } else if (toDate) {
            return `To ${dateToString(toDate)}`;
          } else {
            return '';
          }
        },
        isZeroState: ({fromDate, toDate}) => !fromDate && !toDate
      },
      query: {
        display: ({query}) => {
          if(query && query.length > 18) {
            query = `${query.substring(0,18)}...`;
          }
          return `${query}`
        },
        isZeroState: ({query}) => !query
      }
    }
  
    const [initialFilters, setInitialFilters] = useState(null);
    const [selectedSearchFilters, setSelectedSearchFilters] = useState([]);
    const [zeroStates] = useState({
      enrollmentStatus: 'all',
      fromDate: '',
      toDate: '',
      query: '',
    });
  
    const validDateOrEmpty = dstr => {
      if(!dstr) { return ''; }
      const asDate = moment(dstr);
      return asDate.isValid()? asDate.format('YYYY-MM-DD'): '';
    };
  
    // Parse url filters / sorting / query string from the current url
    // Formats dates + decodes url params if necessary
    const parseUrlFilters = () => {
      let { page = 0, fromDate='', toDate='', agent='', enrollmentStatus='all', query = '', order = 'asc', orderBy = DailyMonitoringLookupColumns[0].value, cardStatus = DailyMonitoringLookupFilters[1].value } = getOptionalURLParams(history);
      if(fromDate) {
        fromDate = moment(fromDate).startOf('day');
      } 
  
      if(toDate) {
        toDate = moment(toDate).endOf('day');
      }
  
      if(cardStatus) {
        try {
          cardStatus = decodeURIComponent(cardStatus);
        } catch {}
      }
  
      if(query) {
        try {
          query = decodeURIComponent(query);
        } catch {}
      }
  
      return {page, agent, cardStatus, enrollmentStatus, query, order, orderBy, fromDate, toDate};
    }
  
    // Set initial filters on page load
    useEffect(() => {
      const {cardStatus, fromDate, toDate, query, agent, enrollmentStatus} = parseUrlFilters();
      
      setInitialFilters({
        enrollmentStatus: enrollmentStatus || 'all',
        agent: agent || 'all',
        fromDate: validDateOrEmpty(fromDate),
        toDate: validDateOrEmpty(toDate),
        query,
      });
    }, []);
  
    useEffect(() => {
      (async () => {
        try {
          setFetching(true);
  
          const filterValues = parseUrlFilters();
          const { page, agent, query, order, orderBy, cardStatus, fromDate, toDate, enrollmentStatus } = filterValues;
  
          // Find appliccable filters that have been changed (not in their zero-state)
          const appliedFilters = ['query','date'].filter(f => !formatFilterPills[f].isZeroState(filterValues));
          
          // Update state with the filters that have been applied
          // this populates the filter "pills" at the top of the work list
          setSelectedSearchFilters(appliedFilters.map(f => ({
            value: f,
            label: formatFilterPills[f].display(filterValues)
          })));
          
          // Construct query
          const queryFilter = query ? `query=${query}&` : '';
  
          let qs = `${queryFilter}cardStatus=${cardStatus}&orderBy=${orderBy}&order=${order}&page=${page}&agent=${agent}&enrollmentStatus=${enrollmentStatus}`;
          if(fromDate) {
            qs += `&fromDate=${moment(fromDate).toISOString()}`;
          }
          if(toDate) {
            qs += `&toDate=${moment(toDate).toISOString()}`;
          }
  
          // Actually perform search
          const { results = [], numberOfResults = 0 } = await AxiosPrivate.get(`/api/v2/rtop-admin/form?${qs}`);
          handleSetTableData(results, numberOfResults, page);
        } catch (e) {
          openToast({ status: 'error', message: e.message || 'Failed to lookup' });
        } finally {
          setFetching(false);
        }
      })();
    }, [history.location.search]);
  
    const handleSetTableData = (results, numberOfResults, currentPage) => {
  
      const getStatus = (status) => {
        if (!status) return 'None';
        return status;
      };
  
      setTableData(prevState => ({
        ...prevState,
        totalRows: numberOfResults,
        currentPage: parseInt(currentPage),
        rows: results.map((item) => ({
          date: dateToString(item.arrivalDate, true, 'DD MMM YYYY'),
          firstName: item.firstName,
          lastName:item.lastName,
          status:  item.status ? (
            <StatusChips status={item.status} />
          ): 'None',
          enrollmentStatus:item.enrollmentStatus?.toUpperCase(),
          lastScreened : item.lastUpdated ? dateToString(item.lastUpdated, false, 'DD MMM YYYY'): '-',
          viewMore: (
            <Button
              text="View"
              variant="outlined"
              size="small"
              onClick={() => history.push(Route.RtopAdminLookupConfirmationNumber.dynamicRoute(item.confirmationNumber))}
            />
          ),
        })),
      }));
    };
  
    return {
      onSubmit: ({query, fromDate, toDate, agent, enrollmentStatus}) => {
        if(fromDate) {
          fromDate = validDateOrEmpty(fromDate);
        }
        
        if(toDate) {
          toDate = validDateOrEmpty(toDate);
        }
  
        return setOptionalURLParams(history, {query, fromDate, toDate, agent, enrollmentStatus, page: 0})
      },
      onSearch: (query) => setOptionalURLParams(history, { query, page: 0 }),
      onFilter: (cardStatus) => setOptionalURLParams(history, { cardStatus, page: 0 }),
      onAgent: (agent) => setOptionalURLParams(history, { agent, page: 0 }),
      onSort: (orderBy, order) => setOptionalURLParams(history, { orderBy, order, page: 0 }),
      onChangePage: (page) => setOptionalURLParams(history, { page }),
      clearFilter: (cardStatus) => removeOptionalURLParam(history, cardStatus),
      tableData,
      isFetching,
      initialFilters,
      selectedSearchFilters,
      zeroStates,
    }
  };


  export const useMonitoringPortalActivity = () => {
    const history = useHistory();
    const [isFetching, setFetching] = useState(false);
    const { openModal, closeModal } = useModal();
    const { openToast } = useToast();
  
    const handleSubmit = async (formId, values) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/activity`, values);
        openToast({ status: 'success', message: 'Submission updated' });
        history.push(Route.RtopAdminLookupDailyReports.dynamicRoute());
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to update this submission' });
        setFetching(false);
      }
    };
  
    return {
      isFetching,
      submit: (formId, values) => {
        openModal({
          title: 'Ready to Submit?',
          description: 'Are you ready to submit this record?',
          negativeActionText: 'Make Changes',
          positiveActionText: 'Yes, submit now.',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleSubmit(formId, values),
        });
      }
    }
  };
  
  export const useMonitoringPortalShowSubmission = () => {
    const { openModal, closeModal } = useModal();

    return {
      showSubmission: description => {
        openModal({
          title: 'Submission',
          description,
          size: 'sm',
          textAlign: 'left',
          negativeActionText: 'Close',
          negativeActionOnClick: () => closeModal(),
        });
      }
    }
  }
  
  export const useDailyMonitoringLookupConfirmationNumber = () => {
    const { openModal, closeModal } = useModal();
    const { openToast } = useToast();
    const [isFetching, setFetching] = useState(true);
    const [error, setError] = useState(null);
    const [formValues, setFormValues] = useState(null);
    const [initialValues, setInitialValues] = useState({ status: '', note: '' });
    const [tableData, setTableData] = useState({
      columns: HouseholdMembersColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
    const [statusTableData, setStatusData] = useState({
      columns: TravellerMonitoringColumns,
      rows: [],
      totalRows: 0,
      currentPage: 0,
    });
    const [enrollmentActions, setEnrollmentActions] = useState([]);
    const [statusReason, setStatusReason] = useState(null);
  
    const handleEnrollModal = async (confirmationNumber) => {
      openModal({
        title: 'Are you Sure?',
        description: `Are you sure you want to enroll this traveller to the program?`,
        negativeActionText: 'No',
        positiveActionText: 'Yes',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleEnroll(confirmationNumber),
      });
    }
  
    const handleEnroll = async (confirmationNumber) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/enroll`);
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Traveller enrolled!' });
      }
      catch (e) {
        setError(e.message || 'Failed to enroll the traveller');
      } finally {
        setFetching(false);
      }
    }

    const handleGenerateLink = async (id, confirmationNumber) => {
      try{
        setFetching(true);
        const link = await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${id}/send-reminder`, {});
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Generated daily questionnaire link!' });
      }
      catch (e) {
        setError(e.message || 'Failed to enroll the traveller');
      } finally {
        setFetching(false);
      }
    }
  
    const handleWithdrawModal = async (confirmationNumber) => {
      openModal({
        title: 'Are you Sure?',
        description: `Are you sure you want to withdraw this traveller from the program?`,
        negativeActionText: 'No',
        positiveActionText: 'Yes',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleWithdraw(confirmationNumber),
      });
    }
  
    const handleWithdraw = async (confirmationNumber) => {
      try{
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/withdraw`);
        await handleLookup(confirmationNumber);
        openToast({status: 'success', message: 'Traveller withdrawn!' });
      }
      catch (e) {
        setError(e.message || 'Failed to withdraw the traveller');
      } finally {
        setFetching(false);
      }
    }
  
    const handleAssign = async (formId, confirmationNumber) => {
      try {
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/assign`);
        await handleLookup(confirmationNumber);
        openToast({
          status: 'warning',
          message: `Record ${confirmationNumber} has been assigned to you.`,
          timeout: null,
          actionText: 'Undo Assignment',
          onActionClick: () => handleUnassign(formId, confirmationNumber),
        });
      } catch (e) {
        setError(e.message || 'Failed to assign this submission');
      } finally {
        setFetching(false);
      }
    };
  
    const handleUnassign = async (formId, confirmationNumber) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.post(`/api/v2/rtop-admin/form/${formId}/assign/undo`);
        await handleLookup(confirmationNumber);
        openToast({ status: 'success', message: 'Submission unassigned' });
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to unassign this submission' });
      } finally {
        setFetching(false);
      }
    };
  
    const handleLookup = async (confirmationNumber) => {
      try {
        setFetching(true);
        const results = await AxiosPrivate.get(`/api/v2/rtop-admin/form/${confirmationNumber}`);
        const { status = '' } = {};
        setInitialValues({ status, note: '' });
        results.household = normalizeEnrollmentFormValues(results.household);
        handleSetTableData(normalizeEnrollmentFormValues(results));

        const statusRes = await AxiosPrivate.get(`/api/v2/rtop-admin/form/${confirmationNumber}/reason`);
        setStatusReason(statusRes[0]?.CARD_STATUS_REASON);
      } catch (e) {
        setError(`Failed to find submission with Confirmation Number: "${confirmationNumber}"`);
      } finally {
        setFetching(false);
      }
    };
  
    const setActions = (enrollmentStatus, confirmationNumber, householdCardStatus) => {
      const EnrollmentActionOptions = {
        "ENROLL" : { label: "Enroll", onClick: () => handleEnrollModal(confirmationNumber)},
        "WITHDRAW" : { label: "Withdraw", onClick: () => handleWithdrawModal(confirmationNumber)},
      };
  
      if( enrollmentStatus === EnrollmentStatus.APPLIED )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["ENROLL"],
          EnrollmentActionOptions["WITHDRAW"]
        ]);
      }
      else if ( enrollmentStatus === EnrollmentStatus.ENROLLED )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["WITHDRAW"]
        ]);
      }
      else if ( enrollmentStatus === EnrollmentStatus.WITHDRAWN )
      {
        setEnrollmentActions([
          EnrollmentActionOptions["ENROLL"]
        ]);
      }

      if( householdCardStatus && householdCardStatus != HouseholdStatus.GREEN)
      {
        setEnrollmentActions((prevState) => [ ...prevState , { label: "Clear Flag", onClick: () => handleChangeStatusModal(confirmationNumber, HouseholdStatus.GREEN)}]);
      }
    }

    const constructSubmissionAnswers = (submission) => {
      let submissionAnswers = [];
      if (submission) {
        submissionAnswers = submission.map(({question, answer}) => {
          return {question: DailySymptoms.find(({value, label}) => value === question ).label.toUpperCase(), answer: answer.toUpperCase()};
        })
      }

      return submissionAnswers;
    }

    const setSubmissionHistoryModal = (status) => {
      setStatusData(prevState => ({
        ...prevState,
        rows: status.map((ele) => ({
          status: ele.status,
          date: ele.date,
          submission: constructSubmissionAnswers(ele.submission)
        })),
      }));
      
    }

    const handleIsolationStatusChangeModal = (event, travellerConfirmationNumber, householdConfirmationNumber) => {
      const isolationStatus = event.target.checked;
      openModal({
        title: 'Change Isolation Status of traveller',
        description: `You are about to change isolation status for ${travellerConfirmationNumber}. Are you sure you want to do this?`,
        negativeActionText: 'Cancel',
        positiveActionText: 'Confirm',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleIsolationStatusChange(travellerConfirmationNumber, isolationStatus, householdConfirmationNumber),
      });
    }

    const handleIsolationStatusChange = async (confirmationNumber, isolationStatus, householdConfirmationNumber) => {
      try {
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/isolation-status`, { isolationStatus });
        await handleLookup(householdConfirmationNumber);
        openToast({
          status: 'warning',
          message: `Changed isolation status for ${confirmationNumber}`,
        });
      } catch (e) {
        setError(e.message || 'Failed to change status');
      } finally {
        setFetching(false);
        closeModal();
      }
    }
  
    const handleSetTableData = (results) => {  
      setFormValues(results);
      setActions(results.enrollmentStatus, results.confirmationNumber, results.householdCardStatus);
      setTableData(prevState => ({
        ...prevState,
        rows: results.travellerStatus.map((item) => ({
          name: `${item.firstName} ${item.lastName}`,
          age: item.age,
          confirmationNumber: item.confirmationNumber,
          status: item.cardStatus ? (
            <StatusChips status={item.cardStatus} />
          ): 'None',
          lastScreened: item.lastUpdated,
          isolationStatus: (
            <Switch checked={false || item.isolationStatus} color="primary" onChange={(e) => handleIsolationStatusChangeModal(e, item.confirmationNumber, results.confirmationNumber)}/>          
          ),
          statusHistory: (item.status && 
            <Button
              text="View History"
              variant="outlined"
              size="small"
              onClick={() => setSubmissionHistoryModal(item.status)} 
            />
              
          ),
        })),
      }));
    };

    const handleChangeStatus = async (confirmationNumber, cardStatus) => {
      try {
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${confirmationNumber}/override-status/${cardStatus}`);
        await handleLookup(confirmationNumber);;
        openToast({
          status: 'warning',
          message: `Changed status for ${confirmationNumber}`,
        });
      } catch (e) {
        setError(e.message || 'Failed to change status');
      } finally {
        setFetching(false);
        closeModal();
      }
    };

    const handleChangeStatusModal = async (confirmationNumber, cardStatus) => {
      openModal({
        title: 'Change status to green',
        description: `You are about to change status for ${confirmationNumber} to green. Are you sure you want to do this?`,
        negativeActionText: 'Cancel',
        positiveActionText: 'Confirm',
        negativeActionOnClick: () => closeModal(),
        positiveActionOnClick: () => handleChangeStatus(confirmationNumber, cardStatus),
      });
    };

    const handleSendReminderLink = async (formId, confirmationNumber) => {
      try {
        closeModal();
        setFetching(true);
        await AxiosPrivate.patch(`/api/v2/rtop-admin/form/${formId}/send-reminder`, {});
        await handleLookup(confirmationNumber);
        openToast({ status: 'success', message: 'Daily link was successfully sent' });
      } catch (e) {
        openToast({ status: 'error', message: e.message || 'Failed to re-send link' });
      } finally {
        setFetching(false);
      }
    };
  
    return {
      isFetching,
      error,
      initialValues,
      formValues,
      lookup: handleLookup,
      assign: handleAssign,
      changeStatus: handleChangeStatusModal,
      generateLink: handleGenerateLink,
      statusReason: statusReason,
      unassign: (formId, confirmationNumber, agent) => {
        openModal({
          title: 'Are you Sure?',
          description: `This record is already assigned to ${agent}. Are you sure you want to unassign this record?`,
          negativeActionText: 'No',
          positiveActionText: 'Yes',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleUnassign(formId, confirmationNumber),
        });
      },
      tableData,
      statusTableData,
      enrollmentActions,
      setStatusData,
      showStatusHistory: description => {
          openModal({
            title: 'Status history for traveller',
            description,
            size: 'md',
            textAlign: 'left',
            negativeActionText: 'Close',
            negativeActionOnClick: () => closeModal() && setStatusData({rows : []}),
          });
      },
      sendReminderLink: (formId, confirmationNumber) => {
        openModal({
          title: 'Re-send reminder?',
          description: 'This will re-send the latest daily questionairre to the household.',
          negativeActionText: 'Cancel',
          positiveActionText: 'Yes, send now.',
          negativeActionOnClick: () => closeModal(),
          positiveActionOnClick: () => handleSendReminderLink(formId, confirmationNumber),
        });
      }
    }
  };