import React, { Suspense, lazy, useEffect, useState } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Route as Routes, UserType } from '../constants';
import { AxiosPrivate } from '../utils';
import { useAuth, useToast } from '../hooks';
import { isBorderPilotApp } from '../appversion';

const Form = lazy(() => import('../pages/public/Form'));
const DailySurvey = lazy(() => import('../pages/public/DailySurvey'));
const Confirmation = lazy(() => import('../pages/public/Confirmation'));
const Login = lazy(() => import('../pages/public/Login'));
const BackOfficeLookup = lazy(() => import('../pages/private/back-office/Lookup'));
const BackOfficeLookupLastName = lazy(() => import('../pages/private/back-office/LookupLastName'));
const BackOfficeLookupConfirmationNumber = lazy(() => import('../pages/private/back-office/LookupConfirmationNumber'));
const ServiceAlbertaLookup = lazy(() => import('../pages/private/service-alberta/Lookup'));
const ServiceAlbertaLookupConfirmationNumber = lazy(() => import('../pages/private/service-alberta/LookupConfirmationNumber'));
const ReportDashboard = lazy(() => import('../pages/private/service-alberta/ReportDashboard'));
const UserRegistration = lazy(() => import('../pages/private/admin/AccessControlDashboard'));
const EnrollmentForm = lazy(() => import('../pages/public/EnrollmentForm'));
const EligibilityForm = lazy(() => import('../pages/public/EligibilityForm'));
const EnrollmentConfirmation = lazy(() => import('../pages/public/EnrollmentConfirmation'));
const RtopAdminLookupDailyReports = lazy(() => import('../pages/private/monitoring/LookupDailyReports'));
const RtopAdminLookupDailyTravellerStatus = lazy(() => import('../pages/private/monitoring/LookupDailyTravellerStatus'));
const RtopAdminUploadNightlyReport = lazy(() => import('../pages/private/monitoring/UploadNightlyReport'));

const redirectTarget = isBorderPilotApp()? Routes.RtopAdminLookupDailyReports.dynamicRoute(): Routes.ServiceAlbertaLookup.dynamicRoute()

const PrivateGuard = ({ component: Component, loginRedirect, ...rest }) => {
  const { openToast } = useToast();
  const { clearAuthState, state: { userType, isAuthenticated } } = useAuth();
  const [isFetching, setFetching] = useState(false);
  const [redirect, setRedirect] = useState('');

  useEffect(() => {
    (async () => {
      try {
        setFetching(true);

        // Valid JWT. Therefore they should see the private route.
        await AxiosPrivate.get('/api/v1/auth/validate');
      } catch (e) {

        // Invalid JWT. Therefore they should not see the private route.
        setRedirect(loginRedirect || {
          [UserType.BackOffice]: Routes.BackOfficeLogin,
          [UserType.ServiceAlberta]: Routes.ServiceAlbertaLogin,
        }[userType] || Routes.Root);

        // If they were authenticated, we want to display an error and clear the authentication state.
        // This only gets hit when JWT fails AND they refresh they page, NOT when they click the logout
        // button.
        if (isAuthenticated) {
          openToast({ status: 'error', message: 'Authentication error. Please login again' });
          clearAuthState();
        }
      } finally {
        setFetching(false);
      }
    })();
  }, [isAuthenticated]);

  /**
   * 1. Is it validating the JWT?
   *    - Show loading spinner.
   *
   * 2. Was a redirect set?
   *    - Go there.
   *
   * 3. Are they a Back Office user trying to access a Service Alberta route or vice versa?
   *    - Redirect them. Otherwise, show the component.
   */
  return isFetching ? <LinearProgress /> : (
    <Route {...rest} render={(props) => redirect ? <Redirect to={redirect} /> : {
      [UserType.BackOffice]: rest.location.pathname.includes('back-office') || rest.location.pathname.includes("reports")
        ? <Component {...props} />
        : <Redirect to={Routes.BackOfficeLookup} />,
      [UserType.ServiceAlberta]: isBorderPilotApp()
          || rest.location.pathname.includes('monitoring')
          || rest.location.pathname.includes('household')
          || rest.location.pathname.includes('service-alberta')
          || rest.location.pathname.includes("reports")
          || rest.location.pathname.includes("rtop-admin")
        ? <Component {...props} />
        : <Redirect to={redirectTarget} />,
      [UserType.FreshworksAdmin]: rest.location.pathname.includes('admin-lookup')
        ? <Component {...props} />
        : <Redirect to={Routes.AdminLookup} />,
      }[userType]}
    />
  );
};

const PublicGuard = ({ component: Component, ...rest }) => {
  const { clearAuthState, state: { userType, isAuthenticated } } = useAuth();
  const [isFetching, setFetching] = useState(false);
  const [redirect, setRedirect] = useState('');

  useEffect(() => {
    (async () => {

      // Is the user authenticated?
      if (isAuthenticated) {
        try {
          setFetching(true);
          await AxiosPrivate.get('/api/v1/auth/validate');

          // Valid JWT. Therefore they should not see the public route.
          setRedirect({
            [UserType.BackOffice]: Routes.BackOfficeLookup,
            [UserType.ServiceAlberta]: redirectTarget,
            [UserType.FreshworksAdmin]: Routes.AdminLookup,
          }[userType]);
        } catch (e) {

          // Invalid JWT. Therefore they should see the public route.
          clearAuthState();
        } finally {
          setFetching(false);
        }
      }
    })();
  }, [isAuthenticated]);

  /**
   * 1. Is it validating the JWT?
   *    - Show loading spinner.
   *
   * 2. Was a redirect set?
   *    - Go there.
   *
   * 3. No redirect set...
   *    - Show the component.
   */
  return isFetching ? <LinearProgress /> : (
    <Route {...rest} render={(props) => redirect
      ? <Redirect to={redirect} />
      : <Component {...props} />}
    />
  );
};

/**
 * Routes to include in the travelling facing app.
 */
const TravellerRoutes = () => (
  <Switch>
    <Route exact path={Routes.Root + 'fr'} component={Form} />
    <Route exact path={Routes.Root + 'en'} component={Form} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <Route render={() => <Redirect to={Routes.Root + 'en'} />} />
  </Switch>
);

/**
 * Routes to include in the admin facing app.
 */
const AdminRoutes = () => (
  <Switch>
    <Route exact path={Routes.AdminForm} component={Form} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <PublicGuard exact path={Routes.Root} component={Login} />
    <PublicGuard exact path={Routes.BackOfficeLogin} component={Login} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />
    <PrivateGuard exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard exact path={Routes.BackOfficeLookup} component={BackOfficeLookup} />
    <PrivateGuard exact path={Routes.BackOfficeLookupLastName.staticRoute} component={BackOfficeLookupLastName} />
    <PrivateGuard exact path={Routes.BackOfficeLookupConfirmationNumber.staticRoute} component={BackOfficeLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookup.staticRoute} component={ServiceAlbertaLookup} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookupConfirmationNumber.staticRoute} component={ServiceAlbertaLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.AdminLookup} component={UserRegistration} />
    <Route render={() => <Redirect to={Routes.Root} />} />
  </Switch>
);

/**
 * Routes to include in the travelling facing app.
 */
const RtopTravellerRoutes = () => (
  <Switch>
    <Route exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <Route exact path={Routes.EnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.EnrollmentForm} component={EnrollmentForm} />
    <Route exact path={Routes.EligibilityForm} component={EligibilityForm} />
    <Route render={() => <Redirect to={Routes.EligibilityForm} />} />
  </Switch>
);

/**
 * Routes to include in the monitoring facing app.
 */
const RtopAdminRoutes = () => (
  <Switch>
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.AdminLookup} component={UserRegistration} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />

    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminLookupConfirmationNumber.staticRoute} component={RtopAdminLookupDailyTravellerStatus} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminUploadNightlyReport} component={RtopAdminUploadNightlyReport} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard loginRedirect={Routes.ServiceAlbertaLogin} exact path={Routes.RtopAdminLookupDailyReports.staticRoute} component={RtopAdminLookupDailyReports} />

    <Route render={() => <Redirect to={Routes.ServiceAlbertaLogin} />} />
  </Switch>
);

/**
 * All available routes.
 */
const AllRoutes = () => (
  <Switch>
    <Route exact path={Routes.Root + 'fr'} component={Form} />
    <Route exact path={Routes.Root + 'en'} component={Form} />
    <Route exact path={`${Routes.Daily}/:token`} component={DailySurvey} />
    <Route exact path={Routes.Confirmation} component={Confirmation} />
    <Route exact path={Routes.EnrollmentConfirmation} component={EnrollmentConfirmation} />
    <Route exact path={Routes.EnrollmentForm} component={EnrollmentForm} />
    <Route exact path={Routes.EligibilityForm} component={EligibilityForm} />
    <PublicGuard exact path={Routes.BackOfficeLogin} component={Login} />
    <PublicGuard exact path={Routes.ServiceAlbertaLogin} component={Login} />
    <PrivateGuard exact path={Routes.ReportDashboard} component={ReportDashboard} />
    <PrivateGuard exact path={Routes.BackOfficeLookup} component={BackOfficeLookup} />
    <PrivateGuard exact path={Routes.BackOfficeLookupLastName.staticRoute} component={BackOfficeLookupLastName} />
    <PrivateGuard exact path={Routes.BackOfficeLookupConfirmationNumber.staticRoute} component={BackOfficeLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookup.staticRoute} component={ServiceAlbertaLookup} />
    <PrivateGuard exact path={Routes.ServiceAlbertaLookupConfirmationNumber.staticRoute} component={ServiceAlbertaLookupConfirmationNumber} />
    <PrivateGuard exact path={Routes.AdminLookup} component={UserRegistration} />
    <PrivateGuard exact path={Routes.RtopAdminLookupDailyReports.staticRoute} component={RtopAdminLookupDailyReports} />
    <PrivateGuard exact path={Routes.RtopAdminLookupConfirmationNumber.staticRoute} component={RtopAdminLookupDailyTravellerStatus} />
    <PrivateGuard exact path={Routes.RtopAdminUploadNightlyReport} component={RtopAdminUploadNightlyReport} />
    <Route render={() => <Redirect to={Routes.Root + 'en'} />} />
  </Switch>
);

const AppRoutes = () => {

  let appOutput = process.env.REACT_APP_OUTPUT || 'ALL';

  // Include rtop traveller routes if
  // running on border pilot domain
  if(appOutput === 'TRAVELLER' && isBorderPilotApp()) {
    appOutput = 'RTOP_TRAVELLER';
  }

  // Include rtop admin routes if
  // running on border pilot domain
  if(appOutput === 'ADMIN' && isBorderPilotApp()) {
    appOutput = 'RTOP_ADMIN';
  }


  /**
   * Choose which routes to include based on
   * the REACT_APP_OUTPUT environment variable.
   */
  const EnvRoutes = {
    'TRAVELLER': TravellerRoutes,
    'ADMIN': AdminRoutes,
    'RTOP_TRAVELLER': RtopTravellerRoutes,
    'RTOP_ADMIN': RtopAdminRoutes,
    'ALL': AllRoutes,
  }[appOutput];

  return (
    <Suspense fallback={<LinearProgress />}>
      <EnvRoutes />
    </Suspense>
  );
}

export { AppRoutes as Routes };
