import React, { Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { getIn } from 'formik';
import { useTranslation } from "react-i18next";

import { InputFieldError, InputFieldLabel } from '../generic';

export const RenderSelectField = ({
  field: { value, name, onChange, onBlur },
  form: { touched, errors, setFieldTouched },
  label,
  options,
  ...props
}) => {
  const error = getIn(errors, name);
  const touch = getIn(touched, name);
  const { t } = useTranslation();

  return (
    <Fragment>
      {label && <InputFieldLabel label={t(label)} />}
      <TextField
        name={name}
        select
        fullWidth
        variant="outlined"
        inputProps={{ displayEmpty: true, onClose: () => setFieldTouched(name, true) }}
        error={touch && !!error}
        value={value || ''}
        onChange={onChange}
        onBlur={onBlur}
        {...props}
      >
        <MenuItem value="" disabled>{t('Select...')}</MenuItem>
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>{t(option.label)}</MenuItem>
        ))}
      </TextField>
      {touch && !!error && <InputFieldError error={t(error)} />}
    </Fragment>
  );
};