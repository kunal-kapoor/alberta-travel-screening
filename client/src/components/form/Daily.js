import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FastField } from 'formik';
import { useTranslation } from "react-i18next";

import { Card, Button } from '../generic';
import { RenderRadioGroup } from '../fields';
import { Questions, DailySymptoms } from '../../constants';


export const Daily = ({ isDisabled, isFetching }) => {
  const { t } = useTranslation();
  
  return (
    <Card title={t('Daily Check-In')}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Box my={1}>
            <Typography variant="h4">Are you experiencing any of the following:</Typography>
              <Box m={1}>
                <ul>
                  {DailySymptoms.map(item => (
                    <Box>
                      <li>{item.label}</li>
                      <Box my={2}>
                        <FastField
                          name={item.value}
                          component={RenderRadioGroup}
                          disabled={isDisabled}
                          options={Questions}
                        />
                      </Box>
                    </Box>
                  ))}
                </ul>
              </Box>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12}>
          <Button
            text="Submit Daily Check-In"
            sFetching={isFetching}
            type="submit"
          />
        </Grid>
      </Grid>
    </Card>
  );
};
