import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import { Typography } from '@material-ui/core';
import { Button } from '../generic';
import { StatusCell } from '../generic/StatusCell';

export const TravellerSelector = ({ options, handleSelect, handleDOBAttempts }) => {

  return (
    <Container maxWidth="sm">
      <Box mt={2} mb={4}>
        <h2>Travellers</h2>
        <Grid container spacing={2}>
          
          {
            options.map((element) => (
              <>
                <Grid item xs={6} >
                  <Typography style={{lineHeight: '42px'}}>{`${element.firstName} ${element.lastName}`}</Typography>
                </Grid>
                <Grid item xs={4}>
                  <StatusCell submissionStatus={element.submissionStatus} status={element.status} />
                </Grid>
                <Grid item xs={2}>
                  <Button 
                    onClick={() => handleSelect(element)} 
                    text="Select"
                    disabled={element.submissionStatus === 'submitted' || element.status === 'red' || handleDOBAttempts(element.dobVerificationAttempt, element.dobAttemptTime)}
                  />
                </Grid>
              </>
            ))
          }
        </Grid>
      </Box>
    </Container>
  )
}