import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { FastField, useFormikContext, Field } from 'formik';
import { useTranslation } from "react-i18next";
import moment from 'moment';

import { RTOPAirportOrBorderCrossings, Countries, TravelReason, StayDuration, ExemptionType } from '../../../constants';

import { Card } from '../../generic';
import { RenderDateField, RenderSelectField, RenderTextField } from '../../fields';

export const ArrivalInformation = ({ isDisabled }) => {
  const { t } = useTranslation();
  const { values } = useFormikContext();
  
  return (
    <Card title={t("Arrival Information")}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FastField
            name="nameOfAirportOrBorderCrossing"
            label="Name of Airport or Border Crossing*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={RTOPAirportOrBorderCrossings}
          />
        </Grid>
        <Grid item xs={12}>
          <FastField
            name="arrivalDate"
            label="Arrival Date* (YYYY/MM/DD)"
            component={RenderDateField}
            placeholder="Required"
            disabled={isDisabled}
            disableFuture={false}
            minDate={moment().subtract(1,'day')}
            maxDate={moment().add(5,'day')}
          />
        </Grid>

        <Grid item xs={12}>
          <Box mt={1} mb={-0.75} component={Typography} fontSize="20px" variant="subtitle2" color="text.secondary">
            {t("Arriving from")}
          </Box>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="arrivalCityOrTown"
            label="City or Town*"
            component={RenderTextField}
            placeholder="Required"
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="arrivalCountry"
            label="Country*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={Countries}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
              name="dateLeftCanada"
              label="Date left Canada (if returning to Canada)"
              component={RenderDateField}
              disabled={isDisabled}
            />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Field
              name="lengthOfAbsence"
              label="Length of absence from Canada"
              component={RenderTextField}
              disabled={true}
              value={values.arrivalDate && values.dateLeftCanada && moment(values.arrivalDate).diff(moment(values.dateLeftCanada),'days')}
            />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="countryOfResidence"
            label="Which country do you currently reside in?*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={Countries}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="airline"
            label="Airlines flown for flight arriving to Calgary"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="flightNumber"
            label="Flight number(s) for flight arriving to Canada"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="seatNumber"
            label="Seat number(s) for flight arriving to Canada"
            component={RenderTextField}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="reasonForTravel"
            label="Reason for travel"
            component={RenderSelectField}
            disabled={isDisabled}
            options={TravelReason}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FastField
            name="durationOfStay"
            label="Expected duration of stay*"
            component={RenderSelectField}
            disabled={isDisabled}
            options={StayDuration}
          />
        </Grid>
        {values.exemptionType === ExemptionType[0].value && values.durationOfStay === StayDuration[0].value && (
        <Grid item xs={12} sm={6}>
          <FastField
            name="dateLeavingCanada"
            label="Date Leaving Canada*"
            component={RenderDateField}
            disabled={isDisabled}
            disableFuture={false}
            minDate={moment()}
          />
        </Grid>
        )}
        <Grid item xs={12}>
          <Box mt={1}>
            <Typography component="p" style={{ opacity: 0.5 }} variant="caption">
              {t("Countries listed are taken from the ISO 3166 Standard")}
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};