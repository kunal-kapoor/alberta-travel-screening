import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { EnrollmentFormSchema } from '../../../constants';

import { Button, FocusError } from '../../generic';
import { PrimaryContact } from './PrimaryContact';
import { TravelInformation } from './TravelInformation';
import { ArrivalInformation } from './ArrivalInformation';
import { IsolationQuestionnaire } from './IsolationQuestionnaire';

export const EnrollmentForm = ({ onSubmit, initialValues, isFetching, isDisabled, canEdit }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={EnrollmentFormSchema}
      enableReinitialize
      onSubmit={onSubmit}
    >
      {({setFieldValue}) => (
        <FormikForm>
            <FocusError />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <PrimaryContact setFieldValue={setFieldValue} isDisabled={isDisabled} canEdit={canEdit} />
              </Grid>
              <Grid item xs={12}>
                <ArrivalInformation isDisabled={isDisabled} />
              </Grid>
              <Grid item xs={12}>
                <TravelInformation isDisabled={isDisabled} />
              </Grid>
              <Grid item xs={12}>
                <IsolationQuestionnaire isDisabled={isDisabled} />
              </Grid>
              {(!isDisabled) && (
                <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                  <Box ml={[1, 0]} mr={[1, 0]}>
                    <Button
                      text="Submit Form"
                      type="submit"
                      loading={isFetching}
                    />
                  </Box>
                </Grid>
              )}
            </Grid>
          </FormikForm>
        )}
      </Formik>
    );
};