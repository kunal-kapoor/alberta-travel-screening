import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Formik, Form } from 'formik';
import { makeStyles } from '@material-ui/core/styles';

import { FocusError } from '../generic';
import { DailySurvey } from '../../constants';
import { Daily } from './Daily';

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    borderColor: theme.palette.common.lightGrey,
  },
}));

export const DailyForm = ({ onSubmit, initialValues, isFetching, isDisabled, isAuthenticated, confirmationNumber }) => {
  const classes = useStyles();
  
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={DailySurvey}
      onSubmit={onSubmit}
    >
      <Form>
        <FocusError />
        <Grid container spacing={2}>
          <Grid item xs={12}>
          { /** Confirmation number
           * (visible only to monitoring admin) */ }
          { isAuthenticated && (
            <Box my={2}>
              <Container maxWidth="sd">
                <Box my={2}>
                  <Box p={2} border={2} borderRadius={5} className={classes.root}>
                    <Typography variant="subtitle2">Confirmation Number:</Typography>
                    <Typography variant="body1">{confirmationNumber}</Typography>
                  </Box>
                </Box>
              </Container>
            </Box>)
          }
          </Grid>
          <Grid item xs={12}>
            <Daily isDisabled={isDisabled} isFetching={isFetching} />
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};
