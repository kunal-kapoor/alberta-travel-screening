import React, { Fragment } from 'react';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import LogoSmall from '../../assets/images/logo-small.svg';
import LogoFrench from '../../assets/images/logo-french.png';

import { Route, UserType } from '../../constants';
import { useAuth } from '../../hooks';
import { useReportsAccessControl } from '../../hooks/admin';

import { Button, Menu } from '.';

export const Header = () => {
  const history = useHistory();
  const location = useLocation();
  const { t, i18n } = useTranslation();
  const { clearAuthState, state: { userType, isAuthenticated, user } } = useAuth();
  const changeLanguage = async () => {
    const lng = i18n.language === 'en' ? 'fr' : 'en';
    await i18n.changeLanguage(lng);
    history.replace(Route.Root + lng);
    document.title = t("Alberta Isolation Questionnaire");
  }
  const { reportAccess } = useReportsAccessControl();

  return (
    <Fragment>
      <Box py={0.75} px={[1.25, 3]} display="flex" alignItems="center" justifyContent="space-between">
        <img
          style={{ cursor: 'pointer' }}
          height={32}
          src={i18n.language === 'en' ? LogoSmall : LogoFrench}
          alt="Logo"
          onClick={() => history.push(!isAuthenticated ? Route.Root : {
            [UserType.ServiceAlberta]: Route.ServiceAlbertaLookup.dynamicRoute(),
            [UserType.BackOffice]: Route.AdminForm,
          }[userType] || Route.Root)}
        />

        <Box display="flex">
          <Hidden smUp>
            {!isAuthenticated && (location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <Menu
              options={[
                { label: t('Language'), onClick: changeLanguage }
              ]}
              label={( <MoreVertIcon /> )}
            />
            )}

            {isAuthenticated && (location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <Menu
              options={[
                { label: t('Language'), onClick: changeLanguage },
                { label: 'Logout', onClick: clearAuthState }
              ]}
              label={(
                <Fragment>
                  <AccountCircleOutlinedIcon/>&nbsp;{user.idTokenPayload?.name || user.idTokenPayload?.email || 'Unknown'}
                </Fragment>
              )}
            />
            )}
          </Hidden>

          <Hidden xsDown>
            {(location.pathname === Route.Root + 'fr' || location.pathname === Route.Root + 'en') && (
              <div>
                <Box
                  component={Button}
                  mr={3}
                  style={{ minWidth: 175 }}
                  text={t('Language')}
                  variant="outlined"
                  fullWidth={false}
                  size="small"
                  onClick={ changeLanguage }
                />
              </div>
            )}
            {isAuthenticated && (
              <div>
                {{
                  [UserType.BackOffice]: history.location.pathname !== Route.BackOfficeLookup && (
                    <Box
                      component={Button}
                      mr={3}
                      style={{ minWidth: 175 }}
                      text="Submission Lookup"
                      onClick={() => history.push(Route.BackOfficeLookup)}
                      variant="outlined"
                      fullWidth={false}
                      size="small"
                    />
                  ),
                  [UserType.ServiceAlberta]: history.location.pathname !== Route.ServiceAlbertaLookup.dynamicRoute() 
                  ? (
                    <>
                      <Box
                        component={Button}
                        mr={3}
                        style={{ minWidth: 175 }}
                        text="Upload Testing Records"
                        onClick={() => history.push(Route.RtopAdminUploadNightlyReport)}
                        variant="outlined"
                        fullWidth={false}
                        size="small"
                      />
                      <Box
                        component={Button}
                        mr={3}
                        style={{ minWidth: 175 }}
                        text="Submission Lookup"
                        onClick={() => history.push(Route.ServiceAlbertaLookup.dynamicRoute())}
                        variant="outlined"
                        fullWidth={false}
                        size="small"
                      />
                    </>
                  )
                  : null,
                }[userType]}

                {(reportAccess.hasAccess && history.location.pathname !== Route.ReportDashboard) && (
                    <Box
                      component={Button}
                      mr={3}
                      style={{ minWidth: 175 }}
                      text="Reports"
                      onClick={() => history.push(Route.ReportDashboard)}
                      variant="outlined"
                      fullWidth={false}
                      size="small"
                    />
                )} 
                <Menu
                  options={[{ label: 'Logout', onClick: clearAuthState }]}
                  label={(
                    <Fragment>
                      <AccountCircleOutlinedIcon/>&nbsp;{user.idTokenPayload?.name || user.idTokenPayload?.email || 'Unknown'}
                    </Fragment>
                  )}
                />
              </div>
            )}
          </Hidden>
        </Box>
      </Box>
    </Fragment>
  );
};