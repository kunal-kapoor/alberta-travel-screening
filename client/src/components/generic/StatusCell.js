import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import MuiButton from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  statusCellWrapper: {
    display: 'flex',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  cell: {
    height: '36px',
    minHeight: '36px',
    border: '1.5px solid rgba(0, 0, 0, 0.2)',
    borderRadius: '25px',
    width: '180px',
    fontSize: '16px',
    display: 'flex',
    alignItems: 'center',
  },
  iconContainer: {
    display: 'flex',
    borderRight: '1.5px solid rgba(0, 0, 0, 0.2)',
    height: '36px',
    width: '38px',
    paddingLeft: '8px',
    justifyContent: '',
    alignItems: 'center',
  },
  textContainer: {
    display: 'flex',
    flex: '1 1 auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitted: {
    color: '#16c92e',
    fontSize: '1.5em'
  },
  notSubmitted: {
    color: '#ffb835',
    fontSize: '1.5em'
  },
  rejected: {
    color: 'red',
    fontSize: '1.5em'
  }
}));


export const StatusCell = ({
  status,
  submissionStatus
}) => {
  const classes = useStyles();
  let ActiveIcon;
  let activeContent;
  let activeStyle;
  switch(submissionStatus) {

    case 'not_submitted':
      ActiveIcon = ErrorOutlineIcon;
      activeContent = 'Not Submitted';
      activeStyle = classes.notSubmitted;
      break;
    case 'submitted':
      activeContent = 'Submitted';
      if(status === 'red') {
        ActiveIcon = AddCircleOutlineIcon;
        activeStyle = classes.rejected;
      } else if (status === 'green' ) {
        ActiveIcon = CheckCircleOutlineIcon;
        activeStyle = classes.submitted;  
      }

      break;
  }


  return (
    <div className={classes.statusCellWrapper}>
      {
        submissionStatus
          ?
            <div className={classes.cell}>
              <div className={classes.iconContainer}> 
                <ActiveIcon className={activeStyle}/>
              </div>
              <div className={classes.textContainer}>
                <Typography className={classes.textContainer}>
                  {activeContent}
                </Typography>
              </div>
            </div>
          :
            null
      }
    </div>
  );
};
