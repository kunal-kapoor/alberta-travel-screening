const replace = require('replace-in-file');

const devConfig = {
    oauthServerUrl: process.env.OAUTH_SERVER_URL,
    tenantId: process.env.TENANT_ID,
    discoveryEndpoint: process.env.REACT_APP_IBM_DISCOVERY_ENDPOINT,
    clientId: process.env.REACT_APP_IBM_CLIENT_ID,
    borderPilotDomain: process.env.REACT_APP_BORDER_PILOT_DOMAIN,
};

const envConfOrDev = (envVariable, prop) => {
    envVariable = envVariable? JSON.parse(envVariable): {};

    return {
        oauthServerUrl: (devConfig.oauthServerUrl || envVariable.oauthServerUrl || '').trim(),
        tenantId: (devConfig.tenantId || envVariable.tenantId || '').trim(),
        discoveryEndpoint: (devConfig.discoveryEndpoint || envVariable.discoveryEndpoint || '').trim(),
        clientId: (devConfig.clientId || envVariable.clientId || '').trim()
    }[prop];
};

const envs = [
    {
        val: envConfOrDev(process.env.APPID_AHS, 'clientId'),
        name: '__AHS_CLIENT_ID__'
    },
    {
        val: envConfOrDev(process.env.APPID_AHS, 'discoveryEndpoint'),
        name: '__AHS_DISCOVERY_ENDPOINT__'
    },
    {
        val: envConfOrDev(process.env.APPID_CLOUD, 'clientId'),
        name: '__OTHER_CLIENT_ID__'
    },
    {
        val: envConfOrDev(process.env.APPID_CLOUD, 'discoveryEndpoint'),
        name: '__OTHER_DISCOVERY_ENDPOINT__'
    },
    {
        val: envConfOrDev(process.env.APPID_SA, 'clientId'),
        name: '__GOA_CLIENT_ID__'
    },
    {
        val: envConfOrDev(process.env.APPID_SA, 'discoveryEndpoint'),
        name: '__GOA_DISCOVERY_ENDPOINT__'
    },
    {
        val: devConfig.borderPilotDomain,
        name: '__REACT_APP_BORDER_PILOT_DOMAIN__'
    }
];

const from = envs.map(v => new RegExp(v.name, 'g'));
const to = envs.map(v => v.val || '');


replace.sync({
    files: '/client/build/index.html',
    from: from,
    to: to
});

