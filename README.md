# Alberta Traveller Screening

## Summary

The premiere of Alberta has made an announcement that Alberta will strengthen safety measures for travellers. This
application allows to keep track of land border crossings with the US and Alberta. Travellers complete their self-isolation
plan at Calgary and Edmonton international airports to determine whether they require additional support in order to
self isolate.

## Table of Contents

1. [Features](#features)
2. [Prerequisites](#prerequisites)
3. [Technology stack](#technology-stack)
4. [Installation](#installation)
5. [Frontend routes](#frontend-routes)
6. [API routes](#api-routes)
7. [Database](#database)

## Features

1. Travel screening form
2. Admin portal with login
3. Secure lookup of submissions
4. Secure search of submitted forms by confirmation number
5. Generation of confirmation number based on government requirements

## Prerequisites

- [npm](https://www.npmjs.com/)
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Technology stack

- [NestJS](https://nestjs.com/) - Backend framework
- [TypeScript](https://www.typescriptlang.org/) - Backend programming language
- [React](https://reactjs.org/) - Frontend library
- [JavaScript](https://www.w3schools.com/Js/js_es6.asp) - Frontend programming language
- [IBM Cloud AppID](https://www.ibm.com/ca-en/cloud/app-id) - Authentication service
- [DB2](https://www.ibm.com/analytics/db2) - Relational database

## Installation

Follow these steps to get started.

1. Add a `.env` file to `/server`
- `cp server/.env.example server/.env`
- Update `server/.env` with variables provided by existing dev.

2. Run project
- `make run-local`

3. Run DB Migrations
- `make local-db-migrate`

### Running the project with docker


To run the database, server, and client:
```
make run-local
```

To tear down your environment:
```
make close-local
```

To seed the database:
```
make local-db-seed records={NUMBER_OF_RECORDS}
```

To migrate the database:
```
make local-db-migrate
```

Workspaces:
- Database - `make local-db-workspace`
- Server - `make local-server-workspace`
- Client - `make local-client-workspace`

### Running the project with npm

Server:
```
cd server && npm install && npm run watch
```

Client:
```
cd client && npm install && npm run start
```

Database seed:
```
cd server && npm run db:seed
```

Database migrate:
```
cd server && npm run db:migrate
```

## Client Routes

#### Traveller

- `/` - Traveller form page
- `/confirmation` - Successful traveller form submission page

#### Admin

- `/` - Back office / Service Alberta login page
- `/form` - Traveller form page
- `/back-office-login` - Back office login page
- `/service-alberta-login` - Service Alberta login page
- `/back-office-lookup` - Back Office lookup page
- `/back-office-lookup-last-name/:query?` - Back Office last name search page
- `/back-office-lookup-confirmation-number/:confirmationNumber` - Back Office submission details page
- `/service-alberta-lookup/:query?/:filter?/:orderBy?/:order?/:page?` - Service Alberta lookup page
- `/service-alberta-lookup-confirmation-number/:confirmationNumber` - Service Alberta submission details page
- `/admin-lookup` - Access Control for Report Dashboard

## Server Routes

#### Traveller

- `/form` - [POST] Submits a new form

#### Admin

- `/auth/validate` - [GET] Validates a user access token
- `/admin/back-office/lastname/:lname` - [GET] Searches and retrieves a list of forms via traveller last name
- `/admin/back-office/form/:confirmationNumber` - [GET] Searches and retrieves a submitted form by confirmation number
- `/admin/back-office/form/:id` - [PATCH] Updates the address / phone number of a submitted form
- `/admin/back-office/form/:id/determination` - [PATCH] Updates a submitted form by adding a determination
- `/admin/service-alberta/form` - [GET] Searches and retrieves a list of forms by via query/filter/order/orderBy/page
- `/admin/service-alberta/form/:confirmationNumber` - [GET] Searches and retrieves a submitted form by confirmation number
- `/admin/service-alberta/form/:id/assign` - [POST] Assigns the agent of the form
- `/admin/service-alberta/form/:id/assign/undo` - [POST] Un-assigns the agent of the form
- `/admin/service-alberta/form/:id/activity` - [POST] Updates a submitted form by adding a determination
- `/admin/report/accesscontrols` - [POST] Creates access controls for the provided agent
- `/admin/report/accesscontrols` - [PUT] Updates access controls for the provided agent
- `/admin/report/accesscontrols` - [GET] Retrieves the entire list of active report access controls for agents

#### Other

- `/health` - [GET] Runs a health check on the server

## Database

This application uses a relational IBM DB2 database. The data is stored as JSON blob inside one of the fields.

More information about Db2 Database:
- https://www.ibm.com/ca-en/products/db2-database
- https://www.ibm.com/analytics/db2
